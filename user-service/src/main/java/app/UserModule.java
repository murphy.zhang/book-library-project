package app;

import app.api.BOUserWebService;
import app.api.UserNotificationWebService;
import app.api.UserSubscriptionWebService;
import app.api.UserWebService;
import app.user.domain.User;
import app.user.domain.UserNotification;
import app.user.domain.UserSubscription;
import app.user.service.BOUserService;
import app.user.service.UserNotificationService;
import app.user.service.UserService;
import app.user.service.UserSubscriptionService;
import app.user.web.BOUserWebServiceImpl;
import app.user.web.UserNotificationWebServiceImpl;
import app.user.web.UserSubscriptionWebServiceImpl;
import app.user.web.UserWebServiceImpl;
import core.framework.module.Module;
import core.framework.mongo.module.MongoConfig;

/**
 * @author murphy.zhang
 */
public class UserModule extends Module {
    @Override
    protected void initialize() {
        db().repository(User.class);
        MongoConfig config = config(MongoConfig.class);
        config.uri(requiredProperty("sys.mongo.uri"));
        config.collection(UserSubscription.class);
        config.collection(UserNotification.class);

        bind(UserService.class);
        bind(UserSubscriptionService.class);
        bind(UserNotificationService.class);
        bind(BOUserService.class);

        api().service(UserWebService.class, bind(UserWebServiceImpl.class));
        api().service(UserSubscriptionWebService.class, bind(UserSubscriptionWebServiceImpl.class));
        api().service(UserNotificationWebService.class, bind(UserNotificationWebServiceImpl.class));
        api().service(BOUserWebService.class, bind(BOUserWebServiceImpl.class));
    }
}
