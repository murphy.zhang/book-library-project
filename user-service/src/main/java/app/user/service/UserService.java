package app.user.service;

import app.api.user.GetUserResponse;
import app.api.user.SearchUserRequest;
import app.api.user.SearchUserResponse;
import app.api.user.UserStatus;
import app.user.domain.User;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.web.exception.NotFoundException;

/**
 * @author murphy.zhang
 */
public class UserService {
    @Inject
    Repository<User> userRepository;

    public GetUserResponse get(Long id) {
        User user = userRepository.get(id).orElseThrow(() -> new NotFoundException("user is not found, id=" + id));
        return userView(user);
    }

    private GetUserResponse userView(User user) {
        GetUserResponse response = new GetUserResponse();
        response.id = user.id;
        response.password = user.password;
        response.username = user.username;
        response.email = user.email;
        response.status = UserStatus.valueOf(user.status);
        return response;
    }

    public SearchUserResponse search(SearchUserRequest request) {
        User user = userRepository.selectOne("username = ?", request.username).orElseThrow(
            () -> new NotFoundException("user is not found, username=" + request.username)
        );
        return searchUserView(user);
    }

    private SearchUserResponse searchUserView(User user) {
        SearchUserResponse result = new SearchUserResponse();
        result.id = user.id;
        result.password = user.password;
        result.username = user.username;
        result.email = user.email;
        result.status = UserStatus.valueOf(user.status);
        return result;
    }
}
