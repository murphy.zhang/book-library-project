package app.user.service;

import app.api.user.notification.CreateUserNotificationRequest;
import app.api.user.notification.SearchUserNotificationRequest;
import app.api.user.notification.SearchUserNotificationResponse;
import app.api.user.notification.UserNotificationStatus;
import app.api.user.notification.UserNotificationType;
import app.user.domain.UserNotification;
import com.mongodb.ReadPreference;
import core.framework.inject.Inject;
import core.framework.mongo.MongoCollection;
import core.framework.mongo.Query;
import org.bson.conversions.Bson;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;

public class UserNotificationService {
    @Inject
    MongoCollection<UserNotification> notificationMongoCollection;

    public void create(CreateUserNotificationRequest request) {
        UserNotification notification = new UserNotification();
        notification.id = UUID.randomUUID().toString();
        notification.userId = request.userId;
        notification.bookId = request.bookId;
        notification.type = convert(request.type);
        notification.status = UserNotification.UserNotificationStatus.UNREAD;
        notification.content = request.content;
        notification.createdTime = LocalDateTime.now();
        notification.updatedTime = notification.createdTime;
        notificationMongoCollection.insert(notification);
    }

    public SearchUserNotificationResponse search(Long userId, SearchUserNotificationRequest request) {
        SearchUserNotificationResponse response = new SearchUserNotificationResponse();
        Query query = new Query();
        query.skip = request.skip;
        query.limit = request.limit;
        query.readPreference = ReadPreference.secondaryPreferred();
        ArrayList<Bson> bsonArrayList = new ArrayList<>();
        bsonArrayList.add(eq("user_id", userId));
        if (request.status != null) {
            bsonArrayList.add(eq("status", convert(request.status)));
        }
        query.filter = and(bsonArrayList.toArray(new Bson[0]));
        long total = notificationMongoCollection.count(query.filter);
        response.total = total;
        if (total > 0) {
            response.userNotifications = notificationMongoCollection.find(query).stream().map(this::userNotificationView).collect(Collectors.toList());
        }
        return response;
    }

    private SearchUserNotificationResponse.UserNotification userNotificationView(UserNotification userNotification) {
        SearchUserNotificationResponse.UserNotification result = new SearchUserNotificationResponse.UserNotification();
        result.id = userNotification.id;
        result.bookId = userNotification.bookId;
        result.type = convert(userNotification.type);
        result.status = convert(userNotification.status);
        result.content = userNotification.content;
        result.createdTime = userNotification.createdTime;
        return result;
    }

    private UserNotification.UserNotificationType convert(UserNotificationType type) {
        return UserNotification.UserNotificationType.valueOf(type.name());
    }

    private UserNotificationType convert(UserNotification.UserNotificationType type) {
        return UserNotificationType.valueOf(type.name());
    }

    private UserNotification.UserNotificationStatus convert(UserNotificationStatus status) {
        return UserNotification.UserNotificationStatus.valueOf(status.name());
    }

    private UserNotificationStatus convert(UserNotification.UserNotificationStatus status) {
        return UserNotificationStatus.valueOf(status.name());
    }
}
