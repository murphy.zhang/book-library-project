package app.user.service;

import app.api.user.BOCreateUserRequest;
import app.api.user.BOCreateUserResponse;
import app.api.user.BOGetUserResponse;
import app.api.user.BOListUserRequest;
import app.api.user.BOListUserResponse;
import app.api.user.BOSearchUserRequest;
import app.api.user.BOSearchUserResponse;
import app.api.user.BOUpdateUserPasswordRequest;
import app.api.user.BOUpdateUserRequest;
import app.api.user.BOUpdateUserResponse;
import app.api.user.BOUpdateUserStatusRequest;
import app.api.user.UserStatus;
import app.user.domain.User;
import app.user.util.QueryUtil;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.util.Strings;
import core.framework.web.exception.BadRequestException;
import core.framework.web.exception.NotFoundException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class BOUserService {
    @Inject
    Repository<User> userRepository;

    public BOCreateUserResponse create(BOCreateUserRequest request) {
        BOCreateUserResponse result = new BOCreateUserResponse();
        if (userRepository.selectOne("username = ?", request.username).isPresent()) {
            result.success = Boolean.FALSE;
            result.message = "user is already exists. username=" + request.username;
            return result;
        }
        User user = new User();
        user.username = request.username;
        user.password = request.password;
        user.email = request.email;
        user.status = UserStatus.ACTIVE.name();
        user.description = request.description;
        user.createdTime = LocalDateTime.now();
        user.updatedTime = user.createdTime;
        user.createdBy = request.createdBy;
        user.updatedBy = user.createdBy;
        user.id = userRepository.insert(user).orElseThrow(() -> new BadRequestException("create user error."));
        result.id = user.id;
        result.success = Boolean.TRUE;
        return result;
    }

    public BOUpdateUserResponse update(Long id, BOUpdateUserRequest request) {
        BOUpdateUserResponse result = new BOUpdateUserResponse();
        User user = userRepository.get(id).orElseThrow(() -> new NotFoundException("user is not found, id=" + id));
        if (request.username != null
                && !user.username.equals(request.username)
                && userRepository.selectOne("username = ?", request.username).isPresent()) {
            result.success = Boolean.FALSE;
            result.message = "user is already exists. username=" + request.username;
            return result;
        }
        user.username = request.username;
        user.email = request.email;
        if (request.status != null) {
            user.status = request.status.name();
        }
        user.description = request.description;
        user.updatedBy = request.updatedBy;
        userRepository.partialUpdate(user);
        result.id = user.id;
        result.success = Boolean.TRUE;
        return result;
    }

    public BOGetUserResponse get(Long id) {
        User user = userRepository.get(id).orElseThrow(() -> new NotFoundException("user is not found, id=" + id));
        BOGetUserResponse result = new BOGetUserResponse();
        result.id = user.id;
        result.username = user.username;
        result.email = user.email;
        result.description = user.description;
        result.status = UserStatus.valueOf(user.status);
        result.createdTime = user.createdTime;
        result.updatedTime = user.updatedTime;
        return result;
    }

    public BOSearchUserResponse search(BOSearchUserRequest request) {
        BOSearchUserResponse result = new BOSearchUserResponse();
        Query<User> query = userRepository.select();
        query.limit(request.limit);
        query.skip(request.skip);
        if (!Strings.isBlank(request.name)) {
            query.where("INSTR(username, ?)", request.name);
        }
        if (!Strings.isBlank(request.email)) {
            query.where("INSTR(email, ?)", request.email);
        }
        if (request.status != null) {
            query.where("status = ?", request.status.name());
        }
        query.orderBy("id desc");

        result.total = (long) query.count();
        if (result.total > 0) {
            result.users = query.fetch().stream().map(this::userView).collect(Collectors.toList());
        }
        return result;
    }

    private BOSearchUserResponse.User userView(User user) {
        BOSearchUserResponse.User result = new BOSearchUserResponse.User();
        result.id = user.id;
        result.username = user.username;
        result.email = user.email;
        result.status = UserStatus.valueOf(user.status);
        result.description = user.description;
        result.createdTime = user.createdTime;
        return result;
    }

    public BOListUserResponse list(BOListUserRequest request) {
        BOListUserResponse result = new BOListUserResponse();
        QueryUtil.Query query = QueryUtil.in("id", request.ids.toArray());
        List<User> userList = userRepository.select(query.condition, query.params);
        result.users = userList.stream().map(this::listUserView).collect(Collectors.toList());
        return result;
    }

    private BOListUserResponse.User listUserView(User user) {
        BOListUserResponse.User result = new BOListUserResponse.User();
        result.id = user.id;
        result.username = user.username;
        return result;
    }

    public void updatePassword(Long id, BOUpdateUserPasswordRequest request) {
        User user = userRepository.get(id).orElseThrow(() -> new NotFoundException("user is not found, id=" + id));
        user.password = request.password;
        user.updatedBy = request.updatedBy;
        userRepository.partialUpdate(user);
    }

    public void updateStatus(Long id, BOUpdateUserStatusRequest request) {
        User user = userRepository.get(id).orElseThrow(() -> new NotFoundException("user is not found, id=" + id));
        if (!user.status.equals(request.status.name())) {
            user.status = request.status.name();
            user.updatedBy = request.updatedBy;
            userRepository.partialUpdate(user);
        }
    }
}
