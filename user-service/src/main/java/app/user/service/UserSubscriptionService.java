package app.user.service;

import app.api.user.subscription.CheckUserSubscriptionRequest;
import app.api.user.subscription.CheckUserSubscriptionResponse;
import app.api.user.subscription.CreateUserSubscriptionResponse;
import app.api.user.subscription.ListUserSubscriptionRequest;
import app.api.user.subscription.ListUserSubscriptionResponse;
import app.api.user.subscription.SearchUserSubscriptionRequest;
import app.api.user.subscription.SearchUserSubscriptionResponse;
import app.api.user.subscription.TotalUserSubscriptionResponse;
import app.api.user.subscription.UserSubscriptionStatus;
import app.user.domain.UserSubscription;
import com.mongodb.ReadPreference;
import core.framework.inject.Inject;
import core.framework.mongo.FindOne;
import core.framework.mongo.MongoCollection;
import core.framework.mongo.Query;
import core.framework.web.exception.BadRequestException;
import core.framework.web.exception.NotFoundException;
import org.bson.conversions.Bson;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;

public class UserSubscriptionService {
    @Inject
    MongoCollection<UserSubscription> userSubscriptionMongoCollection;

    public CreateUserSubscriptionResponse subscribe(Long userId, Long bookId) {
        FindOne query = new FindOne();
        query.readPreference = ReadPreference.secondaryPreferred();
        query.filter = and(eq("user_id", userId), eq("book_id", bookId), eq("status", UserSubscription.UserSubscriptionStatus.ON));
        CreateUserSubscriptionResponse response = new CreateUserSubscriptionResponse();
        Optional<UserSubscription> optional = userSubscriptionMongoCollection.findOne(query);
        if (optional.isPresent()) {
            response.success = Boolean.FALSE;
            response.message = "duplicate subscription";
        } else {
            UserSubscription subscribe = new UserSubscription();
            subscribe.id = UUID.randomUUID().toString();
            subscribe.userId = userId;
            subscribe.bookId = bookId;
            subscribe.status = UserSubscription.UserSubscriptionStatus.ON;
            subscribe.createdTime = LocalDateTime.now();
            subscribe.updatedTime = subscribe.createdTime;
            userSubscriptionMongoCollection.insert(subscribe);
            response.success = Boolean.TRUE;
        }
        return response;
    }

    public SearchUserSubscriptionResponse search(Long userId, SearchUserSubscriptionRequest request) {
        SearchUserSubscriptionResponse response = new SearchUserSubscriptionResponse();
        Query query = new Query();
        query.skip = request.skip;
        query.limit = request.limit;
        query.readPreference = ReadPreference.secondaryPreferred();
        ArrayList<Bson> bsonArrayList = new ArrayList<>();
        bsonArrayList.add(eq("user_id", userId));
        if (request.status != null) {
            bsonArrayList.add(eq("status", convert(request.status)));
        }
        query.filter = and(bsonArrayList.toArray(new Bson[0]));
        long total = userSubscriptionMongoCollection.count(query.filter);
        response.total = total;
        if (total > 0) {
            response.userSubscriptions = userSubscriptionMongoCollection.find(query).stream().map(this::searchUserSubscriptionView).collect(Collectors.toList());
        }
        return response;
    }

    private SearchUserSubscriptionResponse.UserSubscription searchUserSubscriptionView(UserSubscription userSubscription) {
        SearchUserSubscriptionResponse.UserSubscription result = new SearchUserSubscriptionResponse.UserSubscription();
        result.id = userSubscription.id;
        result.bookId = userSubscription.bookId;
        result.status = convert(userSubscription.status);
        result.createdTime = userSubscription.createdTime;
        return result;
    }

    public TotalUserSubscriptionResponse total(ListUserSubscriptionRequest request) {
        Query query = buildQuery(request);
        TotalUserSubscriptionResponse result = new TotalUserSubscriptionResponse();
        result.total = userSubscriptionMongoCollection.count(query.filter);
        return result;
    }

    public ListUserSubscriptionResponse list(ListUserSubscriptionRequest request) {
        ListUserSubscriptionResponse result = new ListUserSubscriptionResponse();
        Query query = buildQuery(request);
        result.userSubscriptions = userSubscriptionMongoCollection.find(query).stream().map(this::listUserSubscribeView).collect(Collectors.toList());
        return result;
    }

    private Query buildQuery(ListUserSubscriptionRequest request) {
        Query query = new Query();
        query.skip = request.skip;
        query.limit = request.limit;
        query.readPreference = ReadPreference.secondaryPreferred();
        ArrayList<Bson> bsonArrayList = new ArrayList<>();
        bsonArrayList.add(eq("book_id", request.bookId));
        if (request.status != null) {
            bsonArrayList.add(eq("status", convert(request.status)));
        }
        query.filter = and(bsonArrayList.toArray(new Bson[0]));
        return query;
    }

    private ListUserSubscriptionResponse.UserSubscription listUserSubscribeView(UserSubscription userSubscription) {
        ListUserSubscriptionResponse.UserSubscription result = new ListUserSubscriptionResponse.UserSubscription();
        result.id = userSubscription.id;
        result.bookId = userSubscription.bookId;
        result.userId = userSubscription.userId;
        result.status = convert(userSubscription.status);
        result.createdTime = userSubscription.createdTime;
        return result;
    }

    public CheckUserSubscriptionResponse check(Long userId, CheckUserSubscriptionRequest request) {
        FindOne query = new FindOne();
        query.readPreference = ReadPreference.secondaryPreferred();
        query.filter = and(eq("user_id", userId), eq("book_id", request.bookId), eq("status", UserSubscription.UserSubscriptionStatus.ON));
        CheckUserSubscriptionResponse result = new CheckUserSubscriptionResponse();
        result.subscribed = userSubscriptionMongoCollection.findOne(query).isPresent();
        return result;
    }

    public void finish(String id) {
        UserSubscription subscription = userSubscriptionMongoCollection.get(id).orElseThrow(() -> new NotFoundException("subscribe is not found, id=" + id));
        subscription.status = UserSubscription.UserSubscriptionStatus.FINISH;
        subscription.updatedTime = LocalDateTime.now();
        userSubscriptionMongoCollection.replace(subscription);
    }

    public void unsubscribe(Long userId, String id) {
        UserSubscription subscription = userSubscriptionMongoCollection.get(id).orElseThrow(() -> new NotFoundException("subscribe is not found, id=" + id));
        if (!subscription.userId.equals(userId)) {
            throw new BadRequestException("wrong operator");
        }
        if (UserSubscription.UserSubscriptionStatus.ON == subscription.status) {
            subscription.status = UserSubscription.UserSubscriptionStatus.OFF;
            subscription.updatedTime = LocalDateTime.now();
            userSubscriptionMongoCollection.replace(subscription);
        }
    }

    public UserSubscription.UserSubscriptionStatus convert(UserSubscriptionStatus status) {
        return UserSubscription.UserSubscriptionStatus.valueOf(status.name());
    }

    public UserSubscriptionStatus convert(UserSubscription.UserSubscriptionStatus status) {
        return UserSubscriptionStatus.valueOf(status.name());
    }
}
