package app.user.util;

import core.framework.util.Strings;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author murphy.zhang
 */
public class QueryUtil {
    public static Query in(String field, Object... params) {
        Query query = new Query();
        query.condition = Strings.format("{} IN ({})", field, Arrays.stream(params).map(p -> "?").collect(Collectors.joining(",")));
        query.params = params;
        return query;
    }

    public static class Query {
        public String condition;
        public Object[] params;
    }
}
