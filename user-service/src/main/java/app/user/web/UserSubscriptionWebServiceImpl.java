package app.user.web;

import app.api.UserSubscriptionWebService;
import app.api.user.subscription.CheckUserSubscriptionRequest;
import app.api.user.subscription.CheckUserSubscriptionResponse;
import app.api.user.subscription.CreateUserSubscriptionRequest;
import app.api.user.subscription.CreateUserSubscriptionResponse;
import app.api.user.subscription.ListUserSubscriptionRequest;
import app.api.user.subscription.ListUserSubscriptionResponse;
import app.api.user.subscription.SearchUserSubscriptionRequest;
import app.api.user.subscription.SearchUserSubscriptionResponse;
import app.api.user.subscription.TotalUserSubscriptionResponse;
import app.user.service.UserSubscriptionService;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

/**
 * @author murphy.zhang
 */
public class UserSubscriptionWebServiceImpl implements UserSubscriptionWebService {
    @Inject
    UserSubscriptionService userSubscriptionService;

    @Override
    public TotalUserSubscriptionResponse total(ListUserSubscriptionRequest request) {
        return userSubscriptionService.total(request);
    }

    @Override
    public ListUserSubscriptionResponse list(ListUserSubscriptionRequest request) {
        return userSubscriptionService.list(request);
    }

    @Override
    public void finish(String id) {
        ActionLogContext.put("userSubscriptionId", id);
        userSubscriptionService.finish(id);
    }

    @Override
    public CheckUserSubscriptionResponse check(Long userId, CheckUserSubscriptionRequest request) {
        ActionLogContext.put("userId", userId);
        ActionLogContext.put("bookId", request.bookId);
        return userSubscriptionService.check(userId, request);
    }

    @Override
    public void unsubscribe(Long userId, String id) {
        ActionLogContext.put("userId", userId);
        ActionLogContext.put("userSubscriptionId", id);
        userSubscriptionService.unsubscribe(userId, id);
    }

    @Override
    public CreateUserSubscriptionResponse subscribe(Long userId, CreateUserSubscriptionRequest request) {
        ActionLogContext.put("userId", userId);
        ActionLogContext.put("bookId", request.bookId);
        return userSubscriptionService.subscribe(userId, request.bookId);
    }

    @Override
    public SearchUserSubscriptionResponse search(Long userId, SearchUserSubscriptionRequest request) {
        ActionLogContext.put("userId", userId);
        return userSubscriptionService.search(userId, request);
    }
}
