package app.user.web;

import app.api.UserWebService;
import app.api.user.GetUserResponse;
import app.api.user.SearchUserRequest;
import app.api.user.SearchUserResponse;
import app.user.service.UserService;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

/**
 * @author murphy.zhang
 */
public class UserWebServiceImpl implements UserWebService {
    @Inject
    UserService userService;

    @Override
    public GetUserResponse get(Long id) {
        ActionLogContext.put("userId", id);
        return userService.get(id);
    }

    @Override
    public SearchUserResponse search(SearchUserRequest request) {
        ActionLogContext.put("username", request.username);
        return userService.search(request);
    }
}
