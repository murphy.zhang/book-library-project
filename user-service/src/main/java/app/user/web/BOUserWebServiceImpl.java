package app.user.web;

import app.api.BOUserWebService;
import app.api.user.BOCreateUserRequest;
import app.api.user.BOCreateUserResponse;
import app.api.user.BOGetUserResponse;
import app.api.user.BOListUserRequest;
import app.api.user.BOListUserResponse;
import app.api.user.BOSearchUserRequest;
import app.api.user.BOSearchUserResponse;
import app.api.user.BOUpdateUserPasswordRequest;
import app.api.user.BOUpdateUserRequest;
import app.api.user.BOUpdateUserResponse;
import app.api.user.BOUpdateUserStatusRequest;
import app.user.service.BOUserService;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

public class BOUserWebServiceImpl implements BOUserWebService {
    @Inject
    BOUserService boUserService;

    @Override
    public BOCreateUserResponse create(BOCreateUserRequest request) {
        ActionLogContext.put("username", request.username);
        return boUserService.create(request);
    }

    @Override
    public BOUpdateUserResponse update(Long id, BOUpdateUserRequest request) {
        ActionLogContext.put("userId", id);
        return boUserService.update(id, request);
    }

    @Override
    public BOGetUserResponse get(Long id) {
        ActionLogContext.put("userId", id);
        return boUserService.get(id);
    }

    @Override
    public BOSearchUserResponse search(BOSearchUserRequest request) {
        return boUserService.search(request);
    }

    @Override
    public BOListUserResponse list(BOListUserRequest request) {
        return boUserService.list(request);
    }

    @Override
    public void updatePassword(Long id, BOUpdateUserPasswordRequest request) {
        ActionLogContext.put("userId", id);
        boUserService.updatePassword(id, request);
    }

    @Override
    public void updateStatus(Long id, BOUpdateUserStatusRequest request) {
        ActionLogContext.put("userId", id);
        boUserService.updateStatus(id, request);
    }
}
