package app.user.web;

import app.api.UserNotificationWebService;
import app.api.user.notification.CreateUserNotificationRequest;
import app.api.user.notification.SearchUserNotificationRequest;
import app.api.user.notification.SearchUserNotificationResponse;
import app.user.service.UserNotificationService;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

/**
 * @author murphy.zhang
 */
public class UserNotificationWebServiceImpl implements UserNotificationWebService {
    @Inject
    UserNotificationService userNotificationService;

    @Override
    public void create(CreateUserNotificationRequest request) {
        ActionLogContext.put("bookId", request.bookId);
        userNotificationService.create(request);
    }

    @Override
    public SearchUserNotificationResponse search(Long userId, SearchUserNotificationRequest request) {
        ActionLogContext.put("userId", userId);
        return userNotificationService.search(userId, request);
    }
}
