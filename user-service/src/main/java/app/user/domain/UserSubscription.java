package app.user.domain;

import core.framework.api.validate.NotNull;
import core.framework.mongo.Collection;
import core.framework.mongo.Field;
import core.framework.mongo.Id;
import core.framework.mongo.MongoEnumValue;

import java.time.LocalDateTime;

/**
 * @author murphy.zhang
 */
@Collection(name = "user_subscriptions")
public class UserSubscription {
    @Id
    @Field(name = "id")
    public String id;

    @NotNull
    @Field(name = "user_id")
    public Long userId;

    @NotNull
    @Field(name = "book_id")
    public Long bookId;

    @NotNull
    @Field(name = "status")
    public UserSubscriptionStatus status;

    @NotNull
    @Field(name = "created_time")
    public LocalDateTime createdTime;

    @NotNull
    @Field(name = "updated_time")
    public LocalDateTime updatedTime;

    public enum UserSubscriptionStatus {
        @MongoEnumValue("ON")
        ON,
        @MongoEnumValue("FINISH")
        FINISH,
        @MongoEnumValue("OFF")
        OFF
    }
}
