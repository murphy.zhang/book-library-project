package app.user.domain;

import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;
import core.framework.mongo.Collection;
import core.framework.mongo.Field;
import core.framework.mongo.Id;
import core.framework.mongo.MongoEnumValue;

import java.time.LocalDateTime;

/**
 * @author murphy.zhang
 */
@Collection(name = "user_notifications")
public class UserNotification {
    @Id
    @Field(name = "id")
    public String id;

    @NotNull
    @Field(name = "user_id")
    public Long userId;

    @NotNull
    @Field(name = "book_id")
    public Long bookId;

    @NotNull
    @Field(name = "type")
    public UserNotificationType type;

    @NotNull
    @Field(name = "status")
    public UserNotificationStatus status;

    @NotNull
    @NotBlank
    @Field(name = "content")
    public String content;

    @NotNull
    @Field(name = "created_time")
    public LocalDateTime createdTime;

    @NotNull
    @Field(name = "updated_time")
    public LocalDateTime updatedTime;

    public enum UserNotificationStatus {
        @MongoEnumValue("READ")
        READ,
        @MongoEnumValue("UNREAD")
        UNREAD
    }

    public enum UserNotificationType {
        @MongoEnumValue("EXPIRE")
        EXPIRE,
        @MongoEnumValue("SUBSCRIPTION")
        SUBSCRIPTION
    }
}
