import app.UserServiceApp;

/**
 * @author murphy.zhang
 */
public class Main {
    public static void main(String[] args) {
        new UserServiceApp().start();
    }
}
