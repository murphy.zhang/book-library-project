package app.api;

import app.api.book.CreateBookCategoryAJAXRequest;
import app.api.book.CreateBookCategoryAJAXResponse;
import app.api.book.GetBookCategoryAJAXResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author murphy.zhang
 */
public interface BookCategoryAJAXWebService {
    @GET
    @Path("/ajax/book/category/:id")
    GetBookCategoryAJAXResponse get(@PathParam("id") Long parentId);

    @POST
    @Path("/ajax/book/category")
    CreateBookCategoryAJAXResponse create(CreateBookCategoryAJAXRequest request);
}
