package app.api.book;

import core.framework.api.json.Property;

/**
 * @author murphy.zhang
 */
public enum BookBorrowedAJAXStatus {
    @Property(name = "BORROWED")
    BORROWED,
    @Property(name = "RETURNED")
    RETURNED
}
