package app.api.book;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;

/**
 * @author murphy.zhang
 */
public class UpdateBookAJAXResponse {
    @NotNull
    @Property(name = "id")
    public Long id;

    @NotNull
    @Property(name = "updated_time")
    public LocalDateTime updatedTime;
}
