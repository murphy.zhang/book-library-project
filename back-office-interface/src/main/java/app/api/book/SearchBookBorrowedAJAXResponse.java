package app.api.book;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author murphy
 */
public class SearchBookBorrowedAJAXResponse {
    @NotNull
    @Property(name = "total")
    public Long total;

    @Property(name = "book_borrowed_histories")
    public List<BookBorrowedHistory> bookBorrowedHistories;

    public static class BookBorrowedHistory {
        @NotNull
        @NotBlank
        @Property(name = "id")
        public String id;

        @NotNull
        @Property(name = "user_Id")
        public Long userId;

        @NotNull
        @NotBlank
        @Property(name = "user_full_name")
        public String userFullName;

        @NotNull
        @Property(name = "status")
        public BookBorrowedAJAXStatus status;

        @NotNull
        @Property(name = "expect_returned_time")
        public LocalDateTime expectReturnedTime;

        @Property(name = "actual_returned_time")
        public LocalDateTime actualReturnedTime;

        @NotNull
        @Property(name = "created_time")
        public LocalDateTime createdTime;

        @NotNull
        @Property(name = "expired")
        public Boolean expired;
    }
}
