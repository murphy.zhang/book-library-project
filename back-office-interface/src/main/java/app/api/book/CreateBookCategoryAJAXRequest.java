package app.api.book;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
public class CreateBookCategoryAJAXRequest {
    @NotNull
    @Property(name = "name")
    public String name;

    @Property(name = "parent_id")
    public Long parentId;
}
