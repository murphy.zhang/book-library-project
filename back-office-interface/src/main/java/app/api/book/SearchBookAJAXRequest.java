package app.api.book;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.util.List;

/**
 * @author murphy.zhang
 */
public class SearchBookAJAXRequest {
    @NotNull
    @Property(name = "skip")
    public Integer skip;

    @NotNull
    @Property(name = "limit")
    public Integer limit;

    @Property(name = "author_name")
    public String authorName;

    @Property(name = "category_id")
    public Long categoryId;

    @Property(name = "tag_ids")
    public List<Long> tagIds;

    @Property(name = "name")
    public String name;

    @Property(name = "description")
    public String description;
}
