package app.api.book;

import core.framework.api.json.Property;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author murphy.zhang
 */
public class UpdateBookAJAXRequest {
    @Property(name = "name")
    public String name;

    @Property(name = "category_id")
    public Long categoryId;

    @Property(name = "author_ids")
    public List<Long> authorIds;

    @Property(name = "tag_ids")
    public List<Long> tagIds;

    @Property(name = "stock")
    public Integer stock;

    @Property(name = "description")
    public String description;

    @Property(name = "published_time")
    public LocalDateTime publishedTime;
}
