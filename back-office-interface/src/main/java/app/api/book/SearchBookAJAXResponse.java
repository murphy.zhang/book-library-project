package app.api.book;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author murphy.zhang
 */
public class SearchBookAJAXResponse {
    @NotNull
    @Property(name = "total")
    public Long total;

    @Property(name = "books")
    public List<Book> books;

    public static class Book {
        @NotNull
        @Property(name = "id")
        public Long id;

        @NotNull
        @NotBlank
        @Property(name = "name")
        public String name;

        @NotNull
        @Property(name = "authors")
        public List<Author> authors;

        @NotNull
        @Property(name = "category")
        public Category category;

        @NotNull
        @Property(name = "tags")
        public List<Tag> tags;

        @NotNull
        @Property(name = "stock")
        public Integer stock;

        @Property(name = "description")
        public String description;

        @NotNull
        @Property(name = "published_time")
        public LocalDateTime publishedTime;

        @NotNull
        @Property(name = "created_time")
        public LocalDateTime createdTime;
    }

    public static class Author {
        @NotNull
        @Property(name = "id")
        public Long id;

        @NotNull
        @NotBlank
        @Property(name = "name")
        public String name;

        @NotNull
        @NotBlank
        @Property(name = "nationality")
        public String nationality;

        @NotNull
        @NotBlank
        @Property(name = "dynasty")
        public String dynasty;
    }

    public static class Category {
        @NotNull
        @Property(name = "id")
        public Long id;

        @NotNull
        @NotBlank
        @Property(name = "name")
        public String name;
    }

    public static class Tag {
        @NotNull
        @Property(name = "id")
        public Long id;

        @NotNull
        @NotBlank
        @Property(name = "name")
        public String name;
    }
}
