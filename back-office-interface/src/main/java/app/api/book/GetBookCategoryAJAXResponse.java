package app.api.book;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

import java.util.List;

/**
 * @author murphy.zhang
 */
public class GetBookCategoryAJAXResponse {
    @Property(name = "categories")
    public List<Category> categories;

    public static class Category {
        @NotNull
        @Property(name = "id")
        public Long id;

        @NotNull
        @NotBlank
        @Property(name = "name")
        public String name;

        @NotNull
        @Property(name = "parent_id")
        public Long parentId;

        @NotNull
        @NotBlank
        @Property(name = "full_path")
        public String fullPath;
    }
}
