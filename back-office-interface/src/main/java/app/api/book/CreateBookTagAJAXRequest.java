package app.api.book;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
public class CreateBookTagAJAXRequest {
    @NotNull
    @Property(name = "name")
    public String name;
}
