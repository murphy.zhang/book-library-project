package app.api;

import app.api.user.CreateUserAJAXRequest;
import app.api.user.CreateUserAJAXResponse;
import app.api.user.GetUserAJAXResponse;
import app.api.user.SearchUserAJAXRequest;
import app.api.user.SearchUserAJAXResponse;
import app.api.user.UpdateUserAJAXRequest;
import app.api.user.UpdateUserAJAXResponse;
import core.framework.api.http.HTTPStatus;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;
import core.framework.api.web.service.ResponseStatus;

/**
 * @author murphy.zhang
 */
public interface UserAJAXWebService {
    @POST
    @Path("/ajax/user")
    @ResponseStatus(HTTPStatus.CREATED)
    CreateUserAJAXResponse create(CreateUserAJAXRequest request);

    @PUT
    @Path("/ajax/user/:id")
    UpdateUserAJAXResponse update(@PathParam("id") Long id, UpdateUserAJAXRequest request);

    @GET
    @Path("/ajax/user/:id")
    GetUserAJAXResponse get(@PathParam("id") Long id);

    @GET
    @Path("/ajax/user")
    SearchUserAJAXResponse search(SearchUserAJAXRequest request);

    @PUT
    @Path("/ajax/user/:id/password")
    void resetPassword(@PathParam("id") Long id);

    @PUT
    @Path("/ajax/user/:id/status-active")
    void setActive(@PathParam("id") Long id);

    @PUT
    @Path("/ajax/user/:id/status-inactive")
    void setInactive(@PathParam("id") Long id);
}
