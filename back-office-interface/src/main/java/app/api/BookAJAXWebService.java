package app.api;

import app.api.book.CreateBookAJAXRequest;
import app.api.book.CreateBookAJAXResponse;
import app.api.book.GetBookAJAXResponse;
import app.api.book.SearchBookAJAXRequest;
import app.api.book.SearchBookAJAXResponse;
import app.api.book.SearchBookBorrowedAJAXRequest;
import app.api.book.SearchBookBorrowedAJAXResponse;
import app.api.book.UpdateBookAJAXRequest;
import app.api.book.UpdateBookAJAXResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author murphy.zhang
 */
public interface BookAJAXWebService {
    @POST
    @Path("/ajax/book")
    CreateBookAJAXResponse create(CreateBookAJAXRequest request);

    @PUT
    @Path("/ajax/book/:id")
    UpdateBookAJAXResponse update(@PathParam("id") Long id, UpdateBookAJAXRequest request);

    @GET
    @Path("/ajax/book/:id")
    GetBookAJAXResponse get(@PathParam("id") Long id);

    @PUT
    @Path("/ajax/book")
    SearchBookAJAXResponse search(SearchBookAJAXRequest request);

    //TODO rename to /ajax/book/:id/borrowed-history
    //TODO move to BorrowedHistory interface
    @GET
    @Path("/ajax/book/borrowed/history")
    SearchBookBorrowedAJAXResponse searchBookBorrowed(SearchBookBorrowedAJAXRequest request);
}
