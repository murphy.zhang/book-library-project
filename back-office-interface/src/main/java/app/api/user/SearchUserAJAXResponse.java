package app.api.user;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author murphy.zhang
 */
public class SearchUserAJAXResponse {
    @Property(name = "total")
    public Long total;

    @Property(name = "users")
    public List<User> users;

    public static class User {
        @NotNull
        @Property(name = "id")
        public Long id;

        @NotNull
        @NotBlank
        @Property(name = "username")
        public String username;

        @NotNull
        @NotBlank
        @Property(name = "email")
        public String email;

        @NotNull
        @Property(name = "status")
        public UserAJAXStatus status;

        @Property(name = "description")
        public String description;

        @NotNull
        @Property(name = "created_time")
        public LocalDateTime createdTime;
    }
}
