package app.api.user;

import core.framework.api.json.Property;

/**
 * @author murphy.zhang
 */
public enum UserAJAXStatus {
    @Property(name = "ACTIVE")
    ACTIVE,
    @Property(name = "INACTIVE")
    INACTIVE
}
