package app.api.user;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.Pattern;

/**
 * @author murphy.zhang
 */
public class UpdateUserAJAXRequest {
    @NotBlank
    @Property(name = "username")
    public String username;

    @NotBlank
    @Pattern(value = "^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$", message = "email must match pattern")
    @Property(name = "email")
    public String email;

    @Property(name = "status")
    public UserAJAXStatus status;

    @Property(name = "description")
    public String description;
}
