package app.api.user;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

public class UpdateUserAJAXResponse {
    @NotNull
    @Property(name = "id")
    public Long id;

    @NotNull
    @Property(name = "success")
    public Boolean success;

    @Property(name = "message")
    public String message;
}
