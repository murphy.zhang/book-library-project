package app.api;

import app.api.book.CreateBookTagAJAXRequest;
import app.api.book.CreateBookTagAJAXResponse;
import app.api.book.SearchBookTagAJAXResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.Path;

/**
 * @author murphy.zhang
 */
public interface BookTagAJAXWebService {
    @GET
    @Path("/ajax/book/tag")
    SearchBookTagAJAXResponse search();

    @POST
    @Path("/ajax/book/tag")
    CreateBookTagAJAXResponse create(CreateBookTagAJAXRequest request);
}
