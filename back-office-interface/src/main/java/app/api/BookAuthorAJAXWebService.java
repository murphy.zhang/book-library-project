package app.api;

import app.api.book.CreateBookAuthorAJAXRequest;
import app.api.book.CreateBookAuthorAJAXResponse;
import app.api.book.SearchBookAuthorAJAXRequest;
import app.api.book.SearchBookAuthorAJAXResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.Path;

/**
 * @author murphy.zhang
 */
public interface BookAuthorAJAXWebService {
    @GET
    @Path("/ajax/book/author")
    SearchBookAuthorAJAXResponse search(SearchBookAuthorAJAXRequest request);

    @POST
    @Path("/ajax/book/author")
    CreateBookAuthorAJAXResponse create(CreateBookAuthorAJAXRequest request);
}
