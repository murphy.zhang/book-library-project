package app.api;

import app.api.user.GetUserResponse;
import app.api.user.SearchUserRequest;
import app.api.user.SearchUserResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author murphy.zhang
 */
public interface UserWebService {
    //web site use, get user detail
    @GET
    @Path("/user/:id")
    GetUserResponse get(@PathParam("id") Long id);

    //web site use, valid username and password
    @PUT
    @Path("/user")
    SearchUserResponse search(SearchUserRequest request);
}
