package app.api.user.notification;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
public class CreateUserNotificationRequest {
    @NotNull
    @Property(name = "user_id")
    public Long userId;

    @NotNull
    @Property(name = "book_id")
    public Long bookId;

    @NotNull
    @Property(name = "type")
    public UserNotificationType type;

    @NotNull
    @NotBlank
    @Property(name = "content")
    public String content;
}
