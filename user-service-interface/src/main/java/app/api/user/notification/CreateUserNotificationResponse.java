package app.api.user.notification;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
public class CreateUserNotificationResponse {
    @NotNull
    @Property(name = "id")
    public String id;
}
