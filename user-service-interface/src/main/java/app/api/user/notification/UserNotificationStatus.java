package app.api.user.notification;

import core.framework.api.json.Property;

/**
 * @author murphy.zhang
 */
public enum UserNotificationStatus {
    @Property(name = "READ")
    READ,
    @Property(name = "UNREAD")
    UNREAD
}
