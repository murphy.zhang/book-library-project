package app.api.user.notification;

import core.framework.api.json.Property;

/**
 * @author murphy.zhang
 */
public enum UserNotificationType {
    @Property(name = "EXPIRE")
    EXPIRE,
    @Property(name = "SUBSCRIPTION")
    SUBSCRIPTION
}
