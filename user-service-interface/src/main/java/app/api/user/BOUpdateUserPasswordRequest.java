package app.api.user;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
public class BOUpdateUserPasswordRequest {
    @NotNull
    @NotBlank
    @Property(name = "password")
    public String password;

    @NotNull
    @NotBlank
    @Property(name = "updated_by")
    public String updatedBy;
}
