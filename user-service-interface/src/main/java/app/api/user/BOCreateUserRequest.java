package app.api.user;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;
import core.framework.api.validate.Pattern;

/**
 * @author murphy.zhang
 */
public class BOCreateUserRequest {
    @NotNull
    @NotBlank
    @Property(name = "username")
    public String username;

    @NotNull
    @NotBlank
    @Property(name = "password")
    public String password;

    @NotNull
    @NotBlank
    @Pattern(value = "^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$", message = "email must match pattern")
    @Property(name = "email")
    public String email;

    @Property(name = "description")
    public String description;

    @NotNull
    @NotBlank
    @Property(name = "created_by")
    public String createdBy;
}
