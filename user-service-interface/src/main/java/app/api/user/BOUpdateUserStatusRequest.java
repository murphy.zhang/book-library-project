package app.api.user;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
public class BOUpdateUserStatusRequest {
    @NotNull
    @Property(name = "status")
    public UserStatus status;

    @NotNull
    @NotBlank
    @Property(name = "updated_by")
    public String updatedBy;
}
