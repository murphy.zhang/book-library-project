package app.api.user;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
public class SearchUserRequest {
    @NotNull
    @NotBlank
    @Property(name = "username")
    public String username;
}
