package app.api.user.subscription;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
public class CheckUserSubscriptionResponse {
    @NotNull
    @Property(name = "subscribed")
    public Boolean subscribed;
}
