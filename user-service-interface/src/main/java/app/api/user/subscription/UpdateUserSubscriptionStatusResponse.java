package app.api.user.subscription;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;

/**
 * @author murphy.zhang
 */
public class UpdateUserSubscriptionStatusResponse {
    @NotNull
    @Property(name = "id")
    public String id;

    @NotNull
    @Property(name = "updated_time")
    public LocalDateTime updatedTime;
}
