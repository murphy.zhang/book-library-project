package app.api.user.subscription;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
public class CheckUserSubscriptionRequest {
    @NotNull
    @Property(name = "book_id")
    public Long bookId;
}
