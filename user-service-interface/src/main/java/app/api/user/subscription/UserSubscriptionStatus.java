package app.api.user.subscription;

import core.framework.api.json.Property;

/**
 * @author murphy.zhang
 */
public enum UserSubscriptionStatus {
    @Property(name = "ON")
    ON,
    @Property(name = "FINISH")
    FINISH,
    @Property(name = "OFF")
    OFF
}
