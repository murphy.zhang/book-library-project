package app.api.user.subscription;

import core.framework.api.json.Property;

/**
 * @author murphy.zhang
 */
public class TotalUserSubscriptionResponse {
    @Property(name = "total")
    public Long total;
}
