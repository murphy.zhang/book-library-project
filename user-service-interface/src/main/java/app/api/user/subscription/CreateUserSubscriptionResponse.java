package app.api.user.subscription;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
public class CreateUserSubscriptionResponse {
    @NotNull
    @Property(name = "success")
    public Boolean success;

    @Property(name = "message")
    public String message;
}
