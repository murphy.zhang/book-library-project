package app.api.user.subscription;

import core.framework.api.validate.NotNull;
import core.framework.api.web.service.QueryParam;

/**
 * @author murphy.zhang
 */
public class ListUserSubscriptionRequest {
    @NotNull
    @QueryParam(name = "skip")
    public Integer skip;

    @NotNull
    @QueryParam(name = "limit")
    public Integer limit;

    @NotNull
    @QueryParam(name = "book_id")
    public Long bookId;

    @QueryParam(name = "status")
    public UserSubscriptionStatus status;
}
