package app.api.user;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;

public class BOGetUserResponse {
    @NotNull
    @Property(name = "id")
    public Long id;

    @NotNull
    @NotBlank
    @Property(name = "username")
    public String username;

    @NotNull
    @NotBlank
    @Property(name = "email")
    public String email;

    @NotNull
    @Property(name = "status")
    public UserStatus status;

    @Property(name = "description")
    public String description;

    @NotNull
    @Property(name = "created_time")
    public LocalDateTime createdTime;

    @NotNull
    @Property(name = "updated_time")
    public LocalDateTime updatedTime;
}
