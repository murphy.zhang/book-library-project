package app.api.user;

import core.framework.api.validate.NotNull;
import core.framework.api.validate.Pattern;
import core.framework.api.web.service.QueryParam;

/**
 * @author murphy.zhang
 */
public class BOSearchUserRequest {
    @NotNull
    @QueryParam(name = "skip")
    public Integer skip;

    @NotNull
    @QueryParam(name = "limit")
    public Integer limit;

    @QueryParam(name = "name")
    public String name;

    @Pattern(value = "^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$", message = "email must match pattern")
    @QueryParam(name = "email")
    public String email;

    @QueryParam(name = "status")
    public UserStatus status;
}
