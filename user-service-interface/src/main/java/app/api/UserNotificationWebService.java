package app.api;

import app.api.user.notification.CreateUserNotificationRequest;
import app.api.user.notification.SearchUserNotificationRequest;
import app.api.user.notification.SearchUserNotificationResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author murphy.zhang
 */
public interface UserNotificationWebService {
    //notification service use, sending notifications to users
    @POST
    @Path("/user-notification")
    void create(CreateUserNotificationRequest request);

    //web site use, search user notifications
    @GET
    @Path("/user/:user-id/user-notification")
    SearchUserNotificationResponse search(@PathParam("user-id") Long userId, SearchUserNotificationRequest request);
}
