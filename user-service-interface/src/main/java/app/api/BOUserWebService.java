package app.api;

import app.api.user.BOCreateUserRequest;
import app.api.user.BOCreateUserResponse;
import app.api.user.BOListUserRequest;
import app.api.user.BOListUserResponse;
import app.api.user.BOGetUserResponse;
import app.api.user.BOSearchUserRequest;
import app.api.user.BOSearchUserResponse;
import app.api.user.BOUpdateUserPasswordRequest;
import app.api.user.BOUpdateUserRequest;
import app.api.user.BOUpdateUserResponse;
import app.api.user.BOUpdateUserStatusRequest;
import core.framework.api.http.HTTPStatus;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;
import core.framework.api.web.service.ResponseStatus;

/**
 * @author murphy.zhang
 */
public interface BOUserWebService {
    @POST
    @Path("/bo/user")
    @ResponseStatus(HTTPStatus.CREATED)
    BOCreateUserResponse create(BOCreateUserRequest request);

    @PUT
    @Path("/bo/user/:id")
    BOUpdateUserResponse update(@PathParam("id") Long id, BOUpdateUserRequest request);

    @GET
    @Path("/bo/user/:id")
    BOGetUserResponse get(@PathParam("id") Long id);

    @GET
    @Path("/bo/user")
    BOSearchUserResponse search(BOSearchUserRequest request);

    //back office use, back office search borrowed history need return user name
    @PUT
    @Path("/bo/user")
    BOListUserResponse list(BOListUserRequest request);

    @PUT
    @Path("/bo/user/:id/password")
    void updatePassword(@PathParam("id") Long id, BOUpdateUserPasswordRequest request);

    @PUT
    @Path("/bo/user/:id/status")
    void updateStatus(@PathParam("id") Long id, BOUpdateUserStatusRequest request);
}
