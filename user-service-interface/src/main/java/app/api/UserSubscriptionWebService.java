package app.api;

import app.api.user.subscription.CheckUserSubscriptionRequest;
import app.api.user.subscription.CheckUserSubscriptionResponse;
import app.api.user.subscription.CreateUserSubscriptionRequest;
import app.api.user.subscription.CreateUserSubscriptionResponse;
import app.api.user.subscription.ListUserSubscriptionRequest;
import app.api.user.subscription.ListUserSubscriptionResponse;
import app.api.user.subscription.SearchUserSubscriptionRequest;
import app.api.user.subscription.SearchUserSubscriptionResponse;
import app.api.user.subscription.TotalUserSubscriptionResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author murphy.zhang
 */
public interface UserSubscriptionWebService {
    //notification service use, a total of users subscribe to the book
    @GET
    @Path("/user-subscription/total")
    TotalUserSubscriptionResponse total(ListUserSubscriptionRequest request);

    //notification service use, search users who subscribe to the book
    @GET
    @Path("/user-subscription")
    ListUserSubscriptionResponse list(ListUserSubscriptionRequest request);

    //notification service use, when notify user and update user subscriptions status
    @PUT
    @Path("/user-subscription/:id/finish")
    void finish(@PathParam("id") String id);

    //web site use, user selects a book need check subscription
    @GET
    @Path("/user/:user-id/user-subscription/check")
    CheckUserSubscriptionResponse check(@PathParam("user-id") Long userId, CheckUserSubscriptionRequest request);

    //web site use, user unsubscribe
    @PUT
    @Path("/user/:user-id/user-subscription/:id")
    void unsubscribe(@PathParam("user-id") Long userId, @PathParam("id") String id);

    //web site use, user subscribe book
    @POST
    @Path("/user/:user-id/user-subscription")
    CreateUserSubscriptionResponse subscribe(@PathParam("user-id") Long userId, CreateUserSubscriptionRequest request);

    //web site use, user subscriptions search, the params which include skip&limit&user_id&status
    @GET
    @Path("/user/:user-id/user-subscription")
    SearchUserSubscriptionResponse search(@PathParam("user-id") Long userId, SearchUserSubscriptionRequest request);
}
