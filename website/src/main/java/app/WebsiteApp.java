package app;

import core.framework.module.App;
import core.framework.module.SystemModule;

/**
 * @author murphy
 */
public class WebsiteApp extends App {
    @Override
    protected void initialize() {
        load(new SystemModule("sys.properties"));
        http().httpPort(8084);
        load(new WebModule());

        site().session().local();
    }
}
