package app.web;

import app.api.UserWebService;
import app.api.user.SearchUserRequest;
import app.api.user.SearchUserResponse;
import app.api.user.UserStatus;
import app.web.security.Passwords;
import app.web.security.SecureUser;
import app.web.security.SecureUserHolder;
import core.framework.cache.Cache;
import core.framework.inject.Inject;
import core.framework.web.Request;
import core.framework.web.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.UUID;

/**
 * @author murphy.zhang
 */
public class LoginController {
    private final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Inject
    UserWebService userWebService;
    @Inject
    Cache<SecureUser> cache;

    public Response login(Request request) {
        Map<String, String> params = request.formParams();
        String username = params.get("username");
        String password = params.get("password");
        SearchUserRequest searchUserRequest = new SearchUserRequest();
        searchUserRequest.username = username;
        SearchUserResponse searchUserResponse = userWebService.search(searchUserRequest);
        if (!searchUserResponse.password.equals(Passwords.hash(password))) {
            return Response.text("password incorrect");
        }
        if (searchUserResponse.status == UserStatus.INACTIVE) {
            return Response.text("user status is inactive");
        }
        String token = UUID.randomUUID().toString();
        request.session().set(SecureUserHolder.KEY_TOKEN, token);
        SecureUser secureUser = new SecureUser();
        secureUser.id = searchUserResponse.id;
        secureUser.username = searchUserResponse.username;
        cache.put(token, secureUser);
        logger.info("login success, username={}, password={}, token={}", username, password, token);
        return Response.text("login");
    }

    public Response logout(Request request) {
        request.session().get(SecureUserHolder.KEY_TOKEN).ifPresent(token -> {
            cache.evict(token);
        });
        request.session().invalidate();
        return Response.text("logout");
    }
}
