package app.web;

import api.UserNotificationAJAXWebService;
import api.user.SearchUserNotificationAJAXRequest;
import api.user.SearchUserNotificationAJAXResponse;
import app.web.interceptor.LoginRequired;
import app.web.security.SecureUserHolder;
import app.web.user.UserNotificationService;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

/**
 * @author murphy.zhang
 */
@LoginRequired
public class UserNotificationAJAXWebServiceImpl implements UserNotificationAJAXWebService {
    @Inject
    SecureUserHolder secureUserHolder;
    @Inject
    UserNotificationService userNotificationService;

    @Override
    public SearchUserNotificationAJAXResponse search(SearchUserNotificationAJAXRequest request) {
        Long userId = secureUserHolder.getUserId();
        ActionLogContext.put("userId", userId);
        return userNotificationService.search(userId, request);
    }
}
