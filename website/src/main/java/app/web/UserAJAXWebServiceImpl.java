package app.web;

import api.UserAJAXWebService;
import api.user.GetUserAJAXResponse;
import app.web.interceptor.LoginRequired;
import app.web.user.UserService;
import core.framework.inject.Inject;

/**
 * @author murphy.zhang
 */
public class UserAJAXWebServiceImpl implements UserAJAXWebService {
    @Inject
    UserService userService;

    @LoginRequired
    @Override
    public GetUserAJAXResponse get() {
        return userService.get();
    }
}
