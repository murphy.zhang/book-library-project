package app.web;

import api.BookAJAXWebService;
import api.book.CreateBookBorrowAJAXResponse;
import api.book.CreateBookBorrowedAJAXRequest;
import api.book.GetBookAJAXResponse;
import api.book.GetBookBorrowedStatusResponse;
import api.book.SearchBookAJAXRequest;
import api.book.SearchBookAJAXResponse;
import api.book.SearchBookBorrowedAJAXRequest;
import api.book.SearchBookBorrowedAJAXResponse;
import api.book.GetBookCategoryAJAXResponse;
import api.book.SearchBookTagAJAXResponse;
import api.book.UpdateBookBorrowAJAXResponse;
import app.web.book.BookService;
import app.web.interceptor.LoginRequired;
import app.web.security.SecureUserHolder;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

/**
 * @author murphy.zhang
 */
public class BookAJAXWebServiceImpl implements BookAJAXWebService {
    @Inject
    BookService bookService;
    @Inject
    SecureUserHolder userHolder;

    @Override
    public GetBookAJAXResponse get(Long id) {
        ActionLogContext.put("bookId", id);
        return bookService.get(id);
    }

    @Override
    public SearchBookAJAXResponse search(SearchBookAJAXRequest request) {
        return bookService.search(request);
    }

    @Override
    public GetBookCategoryAJAXResponse getCategory(Long parentId) {
        return bookService.getCategory(parentId);
    }

    @Override
    public SearchBookTagAJAXResponse searchTag() {
        return bookService.searchTag();
    }

    @LoginRequired
    @Override
    public SearchBookBorrowedAJAXResponse searchBookBorrowed(SearchBookBorrowedAJAXRequest request) {
        Long userId = userHolder.getUserId();
        ActionLogContext.put("userId", userId);
        return bookService.searchBookBorrowed(userId, request);
    }

    @LoginRequired
    @Override
    public GetBookBorrowedStatusResponse borrowedStatus(Long bookId) {
        Long userId = userHolder.getUserId();
        ActionLogContext.put("bookId", bookId);
        ActionLogContext.put("userId", userId);
        return bookService.bookBorrowedStatus(bookId, userId);
    }

    @LoginRequired
    @Override
    public CreateBookBorrowAJAXResponse borrowed(Long bookId, CreateBookBorrowedAJAXRequest request) {
        Long userId = userHolder.getUserId();
        ActionLogContext.put("userId", userId);
        return bookService.borrowed(bookId, userId, request);
    }

    @LoginRequired
    @Override
    public UpdateBookBorrowAJAXResponse returned(String id) {
        Long userId = userHolder.getUserId();
        ActionLogContext.put("userId", userId);
        return bookService.returned(id, userId);
    }
}
