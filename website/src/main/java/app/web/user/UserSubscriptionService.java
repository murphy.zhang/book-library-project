package app.web.user;

import api.user.CreateUserSubscriptionAJAXRequest;
import api.user.CreateUserSubscriptionAJAXResponse;
import api.user.SearchUserSubscriptionAJAXRequest;
import api.user.SearchUserSubscriptionAJAXResponse;
import api.user.UserSubscribeAJAXStatus;
import app.api.UserSubscriptionWebService;
import app.api.user.subscription.CreateUserSubscriptionRequest;
import app.api.user.subscription.CreateUserSubscriptionResponse;
import app.api.user.subscription.SearchUserSubscriptionRequest;
import app.api.user.subscription.SearchUserSubscriptionResponse;
import app.api.user.subscription.UserSubscriptionStatus;
import core.framework.inject.Inject;

import java.util.stream.Collectors;

/**
 * @author murphy.zhang
 */
public class UserSubscriptionService {
    @Inject
    UserSubscriptionWebService userSubscriptionWebService;

    public SearchUserSubscriptionAJAXResponse search(Long userId, SearchUserSubscriptionAJAXRequest request) {
        SearchUserSubscriptionRequest searchUserSubscriptionRequest = new SearchUserSubscriptionRequest();
        searchUserSubscriptionRequest.limit = request.limit;
        searchUserSubscriptionRequest.skip = request.skip;
        if (request.status != null) {
            searchUserSubscriptionRequest.status = convert(request.status);
        }
        SearchUserSubscriptionResponse searchUserSubscriptionResponse = userSubscriptionWebService.search(userId, searchUserSubscriptionRequest);
        SearchUserSubscriptionAJAXResponse result = new SearchUserSubscriptionAJAXResponse();
        result.total = searchUserSubscriptionResponse.total;
        if (result.total > 0) {
            result.userSubscriptions = searchUserSubscriptionResponse.userSubscriptions.stream().map(this::searchSubscriptionView).collect(Collectors.toList());
        }
        return result;
    }

    private SearchUserSubscriptionAJAXResponse.UserSubscription searchSubscriptionView(SearchUserSubscriptionResponse.UserSubscription subscription) {
        SearchUserSubscriptionAJAXResponse.UserSubscription result = new SearchUserSubscriptionAJAXResponse.UserSubscription();
        result.id = subscription.id;
        result.bookId = subscription.bookId;
        result.status = convert(subscription.status);
        result.createdTime = subscription.createdTime;
        return result;
    }

    public CreateUserSubscriptionAJAXResponse subscribe(Long userId, CreateUserSubscriptionAJAXRequest request) {
        CreateUserSubscriptionRequest createUserSubscriptionRequest = new CreateUserSubscriptionRequest();
        createUserSubscriptionRequest.bookId = request.bookId;
        CreateUserSubscriptionResponse createUserSubscriptionResponse = userSubscriptionWebService.subscribe(userId, createUserSubscriptionRequest);
        CreateUserSubscriptionAJAXResponse result = new CreateUserSubscriptionAJAXResponse();
        result.success = createUserSubscriptionResponse.success;
        result.message = createUserSubscriptionResponse.message;
        return result;
    }

    public void unsubscribe(Long userId, String id) {
        userSubscriptionWebService.unsubscribe(userId, id);
    }

    private UserSubscriptionStatus convert(UserSubscribeAJAXStatus status) {
        return UserSubscriptionStatus.valueOf(status.name());
    }

    private UserSubscribeAJAXStatus convert(UserSubscriptionStatus status) {
        return UserSubscribeAJAXStatus.valueOf(status.name());
    }
}
