package app.web.user;

import api.user.GetUserAJAXResponse;
import api.user.UserAJAXStatus;
import app.api.UserWebService;
import app.api.user.GetUserResponse;
import app.api.user.UserStatus;
import app.web.security.SecureUserHolder;
import core.framework.inject.Inject;

/**
 * @author murphy.zhang
 */
public class UserService {
    @Inject
    UserWebService userWebService;
    @Inject
    SecureUserHolder userHolder;

    public GetUserAJAXResponse get() {
        GetUserResponse getUserResponse = userWebService.get(userHolder.getUserId());
        GetUserAJAXResponse result = new GetUserAJAXResponse();
        result.id = getUserResponse.id;
        result.username = getUserResponse.username;
        result.email = getUserResponse.email;
        result.status = convert(getUserResponse.status);
        return result;
    }

    private UserAJAXStatus convert(UserStatus status) {
        return UserAJAXStatus.valueOf(status.name());
    }
}
