package app.web.user;

import api.user.SearchUserNotificationAJAXRequest;
import api.user.SearchUserNotificationAJAXResponse;
import api.user.UserNotificationAJAXStatus;
import api.user.UserNotificationAJAXType;
import app.api.UserNotificationWebService;
import app.api.user.notification.SearchUserNotificationRequest;
import app.api.user.notification.SearchUserNotificationResponse;
import app.api.user.notification.UserNotificationStatus;
import core.framework.inject.Inject;

import java.util.stream.Collectors;

/**
 * @author murphy.zhang
 */
public class UserNotificationService {
    @Inject
    UserNotificationWebService userNotificationWebService;

    public SearchUserNotificationAJAXResponse search(Long userId, SearchUserNotificationAJAXRequest request) {
        SearchUserNotificationRequest searchUserNotificationRequest = new SearchUserNotificationRequest();
        searchUserNotificationRequest.limit = request.limit;
        searchUserNotificationRequest.skip = request.skip;
        if (request.status != null) {
            searchUserNotificationRequest.status = convert(request.status);
        }
        SearchUserNotificationResponse searchUserNotificationResponse = userNotificationWebService.search(userId, searchUserNotificationRequest);
        SearchUserNotificationAJAXResponse result = new SearchUserNotificationAJAXResponse();
        result.total = searchUserNotificationResponse.total;
        if (result.total > 0) {
            result.userNotifications = searchUserNotificationResponse.userNotifications.stream().map(this::searchNotificationView).collect(Collectors.toList());
        }
        return result;
    }

    private SearchUserNotificationAJAXResponse.UserNotification searchNotificationView(SearchUserNotificationResponse.UserNotification notification) {
        SearchUserNotificationAJAXResponse.UserNotification result = new SearchUserNotificationAJAXResponse.UserNotification();
        result.id = notification.id;
        result.bookId = notification.bookId;
        result.status = convert(notification.status);
        result.type = UserNotificationAJAXType.valueOf(notification.type.name());
        result.content = notification.content;
        result.createdTime = notification.createdTime;
        return result;
    }

    private UserNotificationStatus convert(UserNotificationAJAXStatus status) {
        return UserNotificationStatus.valueOf(status.name());
    }

    private UserNotificationAJAXStatus convert(UserNotificationStatus status) {
        return UserNotificationAJAXStatus.valueOf(status.name());
    }
}
