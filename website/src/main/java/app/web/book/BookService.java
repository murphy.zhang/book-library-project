package app.web.book;

import api.book.BookBorrowedAJAXStatus;
import api.book.CreateBookBorrowAJAXResponse;
import api.book.CreateBookBorrowedAJAXRequest;
import api.book.GetBookAJAXResponse;
import api.book.GetBookBorrowedStatusResponse;
import api.book.GetBookCategoryAJAXResponse;
import api.book.SearchBookAJAXRequest;
import api.book.SearchBookAJAXResponse;
import api.book.SearchBookBorrowedAJAXRequest;
import api.book.SearchBookBorrowedAJAXResponse;
import api.book.SearchBookTagAJAXResponse;
import api.book.UpdateBookBorrowAJAXResponse;
import app.api.BookWebService;
import app.api.UserSubscriptionWebService;
import app.api.UserWebService;
import app.api.book.ListBookRequest;
import app.api.book.GetBookResponse;
import app.api.book.SearchBookRequest;
import app.api.book.SearchBookResponse;
import app.api.book.borrowed.BookBorrowedStatus;
import app.api.book.borrowed.CreateBookBorrowResponse;
import app.api.book.borrowed.CreateBookBorrowedRequest;
import app.api.book.borrowed.SearchBookBorrowedRequest;
import app.api.book.borrowed.SearchBookBorrowedResponse;
import app.api.book.borrowed.UpdateBookBorrowResponse;
import app.api.book.borrowed.UpdateBookBorrowedRequest;
import app.api.book.category.GetBookCategoryResponse;
import app.api.book.stock.GetBookStockResponse;
import app.api.book.tag.SearchBookTagResponse;
import app.api.user.subscription.CheckUserSubscriptionRequest;
import core.framework.inject.Inject;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author murphy.zhang
 */
public class BookService {
    @Inject
    BookWebService bookWebService;
    @Inject
    UserWebService userWebService;
    @Inject
    UserSubscriptionWebService userSubscriptionWebService;

    public GetBookAJAXResponse get(Long id) {
        GetBookResponse getBookResponse = bookWebService.get(id);
        GetBookStockResponse getBookStockResponse = bookWebService.getStock(id);
        GetBookAJAXResponse result = new GetBookAJAXResponse();
        result.id = getBookResponse.id;
        result.name = getBookResponse.name;
        result.description = getBookResponse.description;
        result.publishedTime = getBookResponse.publishedTime;
        result.createdTime = getBookResponse.createdTime;
        result.stock = getBookStockResponse.stock;
        result.authors = getBookResponse.authors.stream().map(this::getBookAuthorView).collect(Collectors.toList());
        result.category = getBookCategoryView(getBookResponse.category);
        result.tags = getBookResponse.tags.stream().map(this::getBookTagView).collect(Collectors.toList());
        return result;
    }

    private GetBookAJAXResponse.Author getBookAuthorView(GetBookResponse.Author author) {
        GetBookAJAXResponse.Author result = new GetBookAJAXResponse.Author();
        result.id = author.id;
        result.name = author.name;
        result.dynasty = author.dynasty;
        result.nationality = author.nationality;
        return result;
    }

    private GetBookAJAXResponse.Category getBookCategoryView(GetBookResponse.Category category) {
        GetBookAJAXResponse.Category result = new GetBookAJAXResponse.Category();
        result.id = category.id;
        result.name = category.name;
        return result;
    }

    private GetBookAJAXResponse.Tag getBookTagView(GetBookResponse.Tag tag) {
        GetBookAJAXResponse.Tag result = new GetBookAJAXResponse.Tag();
        result.id = tag.id;
        result.name = tag.name;
        return result;
    }

    public GetBookBorrowedStatusResponse bookBorrowedStatus(Long bookId, Long userId) {
        GetBookBorrowedStatusResponse result = new GetBookBorrowedStatusResponse();
        CheckUserSubscriptionRequest checkUserSubscriptionRequest = new CheckUserSubscriptionRequest();
        checkUserSubscriptionRequest.bookId = bookId;
        result.subscribed = userSubscriptionWebService.check(userId, checkUserSubscriptionRequest).subscribed;
        SearchBookBorrowedRequest searchBookBorrowedRequest = new SearchBookBorrowedRequest();
        searchBookBorrowedRequest.limit = 1;
        searchBookBorrowedRequest.bookId = bookId;
        searchBookBorrowedRequest.userId = userId;
        searchBookBorrowedRequest.status = BookBorrowedStatus.BORROWED;
        result.borrowed = bookWebService.searchBookBorrowed(searchBookBorrowedRequest).total > 0;
        return result;
    }

    public SearchBookAJAXResponse search(SearchBookAJAXRequest request) {
        SearchBookRequest searchBookRequest = new SearchBookRequest();
        searchBookRequest.limit = request.limit;
        searchBookRequest.skip = request.skip;
        searchBookRequest.name = request.name;
        searchBookRequest.authorName = request.authorName;
        searchBookRequest.categoryId = request.categoryId;
        searchBookRequest.tagIds = request.tagIds;
        SearchBookResponse searchBookResponse = bookWebService.search(searchBookRequest);
        SearchBookAJAXResponse result = new SearchBookAJAXResponse();
        result.total = searchBookResponse.total;
        if (result.total > 0) {
            result.books = searchBookResponse.books.stream().map(this::bookView).collect(Collectors.toList());
        }
        return result;
    }

    private SearchBookAJAXResponse.Book bookView(SearchBookResponse.Book book) {
        SearchBookAJAXResponse.Book result = new SearchBookAJAXResponse.Book();
        result.id = book.id;
        result.name = book.name;
        result.description = book.description;
        result.publishedTime = book.publishedTime;
        result.createdTime = book.createdTime;
        SearchBookAJAXResponse.Category category = new SearchBookAJAXResponse.Category();
        category.id = book.category.id;
        category.name = book.category.name;
        result.category = category;
        result.authors = book.authors.stream().map(this::bookSearchAuthorView).collect(Collectors.toList());
        result.tags = book.tags.stream().map(this::tagSearchView).collect(Collectors.toList());
        return result;
    }

    private SearchBookAJAXResponse.Author bookSearchAuthorView(SearchBookResponse.Author author) {
        SearchBookAJAXResponse.Author result = new SearchBookAJAXResponse.Author();
        result.id = author.id;
        result.name = author.name;
        result.dynasty = author.dynasty;
        result.nationality = author.nationality;
        return result;
    }

    private SearchBookAJAXResponse.Tag tagSearchView(SearchBookResponse.Tag tag) {
        SearchBookAJAXResponse.Tag result = new SearchBookAJAXResponse.Tag();
        result.id = tag.id;
        result.name = tag.name;
        return result;
    }

    public SearchBookBorrowedAJAXResponse searchBookBorrowed(Long userId, SearchBookBorrowedAJAXRequest request) {
        SearchBookBorrowedRequest searchBookBorrowedRequest = new SearchBookBorrowedRequest();
        searchBookBorrowedRequest.limit = request.limit;
        searchBookBorrowedRequest.skip = request.skip;
        searchBookBorrowedRequest.userId = userId;
        if (request.status != null) {
            searchBookBorrowedRequest.status = convert(request.status);
        }
        SearchBookBorrowedResponse searchBookBorrowedResponse = bookWebService.searchBookBorrowed(searchBookBorrowedRequest);
        SearchBookBorrowedAJAXResponse result = new SearchBookBorrowedAJAXResponse();
        result.total = searchBookBorrowedResponse.total;
        if (result.total > 0) {
            ListBookRequest listBookRequest = new ListBookRequest();
            listBookRequest.ids = searchBookBorrowedResponse.bookBorrowedHistories.stream().map(h -> h.bookId).collect(Collectors.toList());
            Map<Long, String> bookNameMap = bookWebService.list(listBookRequest).books.stream().collect(Collectors.toMap(b -> b.id, b -> b.name));
            result.bookBorrowedHistories = searchBookBorrowedResponse.bookBorrowedHistories.stream().map(h -> borrowedHistoryView(h, bookNameMap)).collect(Collectors.toList());
        }
        return result;
    }

    private SearchBookBorrowedAJAXResponse.BookBorrowedHistory borrowedHistoryView(SearchBookBorrowedResponse.BookBorrowedHistory history, Map<Long, String> bookNameMap) {
        SearchBookBorrowedAJAXResponse.BookBorrowedHistory result = new SearchBookBorrowedAJAXResponse.BookBorrowedHistory();
        result.id = history.id;
        result.bookId = history.bookId;
        result.bookName = bookNameMap.get(history.bookId);
        result.expectReturnedTime = history.expectReturnedTime;
        result.actualReturnedTime = history.actualReturnedTime;
        result.createdTime = history.createdTime;
        result.status = convert(history.status);
        return result;
    }

    public CreateBookBorrowAJAXResponse borrowed(Long bookId, Long userId, CreateBookBorrowedAJAXRequest request) {
        CreateBookBorrowedRequest createBookBorrowedRequest = new CreateBookBorrowedRequest();
        createBookBorrowedRequest.userId = userId;
        createBookBorrowedRequest.expectReturnedTime = request.expectReturnedTime;
        CreateBookBorrowResponse createBookBorrowResponse = bookWebService.borrowed(bookId, createBookBorrowedRequest);
        CreateBookBorrowAJAXResponse result = new CreateBookBorrowAJAXResponse();
        result.id = createBookBorrowResponse.id;
        result.createdTime = createBookBorrowResponse.createdTime;
        return result;
    }

    public UpdateBookBorrowAJAXResponse returned(String id, Long userId) {
        UpdateBookBorrowedRequest updateBookBorrowedRequest = new UpdateBookBorrowedRequest();
        updateBookBorrowedRequest.userId = userId;
        UpdateBookBorrowResponse updateBookBorrowResponse = bookWebService.returned(id, updateBookBorrowedRequest);
        UpdateBookBorrowAJAXResponse result = new UpdateBookBorrowAJAXResponse();
        result.id = updateBookBorrowResponse.id;
        result.actualReturnedTime = updateBookBorrowResponse.actualReturnedTime;
        return result;
    }

    private BookBorrowedStatus convert(BookBorrowedAJAXStatus status) {
        return BookBorrowedStatus.valueOf(status.name());
    }

    private BookBorrowedAJAXStatus convert(BookBorrowedStatus status) {
        return BookBorrowedAJAXStatus.valueOf(status.name());
    }

    public GetBookCategoryAJAXResponse getCategory(Long parentId) {
        GetBookCategoryResponse getBookCategoryResponse = bookWebService.getCategory(parentId);
        GetBookCategoryAJAXResponse result = new GetBookCategoryAJAXResponse();
        result.categories = getBookCategoryResponse.categories.stream().map(this::categoryView).collect(Collectors.toList());
        return result;
    }

    private GetBookCategoryAJAXResponse.Category categoryView(GetBookCategoryResponse.Category category) {
        GetBookCategoryAJAXResponse.Category result = new GetBookCategoryAJAXResponse.Category();
        result.id = category.id;
        result.name = category.name;
        result.parentId = category.parentId;
        result.fullPath = category.fullPath;
        return result;
    }

    public SearchBookTagAJAXResponse searchTag() {
        SearchBookTagResponse searchBookTagResponse = bookWebService.searchTag();
        SearchBookTagAJAXResponse result = new SearchBookTagAJAXResponse();
        result.tags = searchBookTagResponse.tags.stream().map(this::tagView).collect(Collectors.toList());
        return result;
    }

    private SearchBookTagAJAXResponse.Tag tagView(SearchBookTagResponse.Tag tag) {
        SearchBookTagAJAXResponse.Tag result = new SearchBookTagAJAXResponse.Tag();
        result.id = tag.id;
        result.name = tag.name;
        return result;
    }
}
