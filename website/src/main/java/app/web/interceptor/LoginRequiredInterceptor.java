package app.web.interceptor;

import app.web.security.SecureUser;
import app.web.security.SecureUserHolder;
import core.framework.cache.Cache;
import core.framework.inject.Inject;
import core.framework.json.JSON;
import core.framework.web.Interceptor;
import core.framework.web.Invocation;
import core.framework.web.Request;
import core.framework.web.Response;
import core.framework.web.exception.UnauthorizedException;

/**
 * @author murphy.zhang
 */
public class LoginRequiredInterceptor implements Interceptor {
    @Inject
    Cache<SecureUser> cache;

    @Override
    public Response intercept(Invocation invocation) throws Exception {
        LoginRequired annotation = invocation.annotation(LoginRequired.class);
        if (annotation != null) {
            Request request = invocation.context().request();
            String token = request.session().get(SecureUserHolder.KEY_TOKEN).orElseThrow(() -> new UnauthorizedException("token must not be null."));
            SecureUser secureUser = cache.get(token, s -> JSON.fromJSON(SecureUser.class, s));
            if (secureUser == null) throw new UnauthorizedException("token is expired.");
            invocation.context().put(SecureUserHolder.KEY_USER, secureUser);
        }
        return invocation.proceed();
    }
}
