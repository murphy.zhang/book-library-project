package app.web;

import api.UserSubscriptionAJAXWebService;
import api.user.CreateUserSubscriptionAJAXRequest;
import api.user.CreateUserSubscriptionAJAXResponse;
import api.user.SearchUserSubscriptionAJAXRequest;
import api.user.SearchUserSubscriptionAJAXResponse;
import app.web.interceptor.LoginRequired;
import app.web.security.SecureUserHolder;
import app.web.user.UserSubscriptionService;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

/**
 * @author murphy.zhang
 */
@LoginRequired
public class UserSubscriptionAJAXWebServiceImpl implements UserSubscriptionAJAXWebService {
    @Inject
    SecureUserHolder secureUserHolder;
    @Inject
    UserSubscriptionService userSubscriptionService;

    @Override
    public void unsubscribe(String id) {
        Long userId = secureUserHolder.getUserId();
        ActionLogContext.put("userId", userId);
        ActionLogContext.put("userSubscriptionId", id);
        userSubscriptionService.unsubscribe(userId, id);
    }

    @Override
    public CreateUserSubscriptionAJAXResponse subscribe(CreateUserSubscriptionAJAXRequest request) {
        Long userId = secureUserHolder.getUserId();
        ActionLogContext.put("userId", userId);
        return userSubscriptionService.subscribe(userId, request);
    }

    @Override
    public SearchUserSubscriptionAJAXResponse search(SearchUserSubscriptionAJAXRequest request) {
        Long userId = secureUserHolder.getUserId();
        ActionLogContext.put("userId", userId);
        return userSubscriptionService.search(userId, request);
    }
}
