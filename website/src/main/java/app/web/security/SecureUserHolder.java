package app.web.security;

import core.framework.inject.Inject;
import core.framework.web.WebContext;
import core.framework.web.exception.UnauthorizedException;

/**
 * @author murphy.zhang
 */
public class SecureUserHolder {
    public static final String KEY_TOKEN = "token";
    public static final String KEY_USER = "user";

    @Inject
    WebContext webContext;

    public SecureUser getUser() {
        Object o = webContext.get(KEY_USER);
        if (o == null) {
            throw new UnauthorizedException("user login is expired.");
        }
        return (SecureUser) o;
    }

    public Long getUserId() {
        return getUser().id;
    }
}