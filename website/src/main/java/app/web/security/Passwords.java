package app.web.security;

import core.framework.crypto.Hash;

/**
 * @author murphy.zhang
 */
public class Passwords {
    public static String hash(String password) {
        return hash(password, "murphy-library", 1);
    }

    private static String hash(String password, String salt, int iteration) {
        String hashedPassword = password;
        for (int i = 0; i < iteration; i++) {
            hashedPassword = Hash.sha256Hex(salt + hashedPassword);
        }
        return hashedPassword;
    }
}
