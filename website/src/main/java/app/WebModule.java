package app;

import api.BookAJAXWebService;
import api.UserAJAXWebService;
import api.UserNotificationAJAXWebService;
import api.UserSubscriptionAJAXWebService;
import app.api.BookWebService;
import app.api.UserWebService;
import app.web.BookAJAXWebServiceImpl;
import app.web.LoginController;
import app.web.UserAJAXWebServiceImpl;
import app.web.UserNotificationAJAXWebServiceImpl;
import app.web.UserSubscriptionAJAXWebServiceImpl;
import app.web.book.BookService;
import app.web.interceptor.LoginRequiredInterceptor;
import app.web.security.SecureUser;
import app.web.security.SecureUserHolder;
import app.web.user.UserNotificationService;
import app.web.user.UserService;
import app.web.user.UserSubscriptionService;
import core.framework.http.HTTPMethod;
import core.framework.module.Module;

import java.time.Duration;

/**
 * @author murphy
 */
public class WebModule extends Module {
    @Override
    protected void initialize() {
        cache().redis(requiredProperty("sys.redis.host"));
        cache().add(SecureUser.class, Duration.ofMinutes(30));

        bind(SecureUserHolder.class);
        bind(BookService.class);
        bind(UserService.class);
        bind(UserSubscriptionService.class);
        bind(UserNotificationService.class);

        http().intercept(bind(LoginRequiredInterceptor.class));

        LoginController loginController = bind(LoginController.class);
        http().route(HTTPMethod.PUT, "/ajax/login", loginController::login);
        http().route(HTTPMethod.PUT, "/ajax/logout", loginController::logout);

        api().client(BookWebService.class, requiredProperty("sys.book.uri"));
        api().client(UserWebService.class, requiredProperty("sys.user.uri"));

        api().service(BookAJAXWebService.class, bind(BookAJAXWebServiceImpl.class));
        api().service(UserAJAXWebService.class, bind(UserAJAXWebServiceImpl.class));
        api().service(UserSubscriptionAJAXWebService.class, bind(UserSubscriptionAJAXWebServiceImpl.class));
        api().service(UserNotificationAJAXWebService.class, bind(UserNotificationAJAXWebServiceImpl.class));
    }
}
