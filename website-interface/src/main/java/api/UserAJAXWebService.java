package api;

import api.user.GetUserAJAXResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;

/**
 * @author murphy
 */
public interface UserAJAXWebService {
    @GET
    @Path("/ajax/user")
    GetUserAJAXResponse get();
}
