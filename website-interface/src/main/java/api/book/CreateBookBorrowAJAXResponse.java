package api.book;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;

/**
 * @author murphy.zhang
 */
public class CreateBookBorrowAJAXResponse {
    @NotNull
    @Property(name = "id")
    public String id;

    @NotNull
    @Property(name = "created_time")
    public LocalDateTime createdTime;
    //TODO return response status and reasons for failure
    public Boolean success;

    public String reason;
}
