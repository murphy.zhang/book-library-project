package api.book;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
public class GetBookBorrowedStatusResponse {
    @NotNull
    @Property(name = "borrowed")
    public Boolean borrowed = Boolean.FALSE;

    @NotNull
    @Property(name = "subscribed")
    public Boolean subscribed = Boolean.FALSE;
}
