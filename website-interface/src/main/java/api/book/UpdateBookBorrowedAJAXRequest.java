package api.book;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
public class UpdateBookBorrowedAJAXRequest {
    @NotNull
    @Property(name = "user_id")
    public Long userId;
}
