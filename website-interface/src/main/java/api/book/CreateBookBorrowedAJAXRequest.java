package api.book;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;

/**
 * @author murphy.zhang
 */
public class CreateBookBorrowedAJAXRequest {
    @NotNull
    @Property(name = "expect_returned_time")
    public LocalDateTime expectReturnedTime;
}
