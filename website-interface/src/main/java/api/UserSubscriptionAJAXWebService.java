package api;

import api.user.CreateUserSubscriptionAJAXRequest;
import api.user.CreateUserSubscriptionAJAXResponse;
import api.user.SearchUserSubscriptionAJAXRequest;
import api.user.SearchUserSubscriptionAJAXResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author murphy.zhang
 */
public interface UserSubscriptionAJAXWebService {
    @PUT
    @Path("/user/user-subscription/:id")
    void unsubscribe(@PathParam("id") String id);

    @POST
    @Path("/user/user-subscription")
    CreateUserSubscriptionAJAXResponse subscribe(CreateUserSubscriptionAJAXRequest request);

    @GET
    @Path("/user/user-subscription")
    SearchUserSubscriptionAJAXResponse search(SearchUserSubscriptionAJAXRequest request);
}
