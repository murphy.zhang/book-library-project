package api;

import api.book.CreateBookBorrowAJAXResponse;
import api.book.CreateBookBorrowedAJAXRequest;
import api.book.GetBookAJAXResponse;
import api.book.GetBookBorrowedStatusResponse;
import api.book.SearchBookAJAXRequest;
import api.book.SearchBookAJAXResponse;
import api.book.SearchBookBorrowedAJAXRequest;
import api.book.SearchBookBorrowedAJAXResponse;
import api.book.GetBookCategoryAJAXResponse;
import api.book.SearchBookTagAJAXResponse;
import api.book.UpdateBookBorrowAJAXResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

public interface BookAJAXWebService {
    @GET
    @Path("/ajax/book/:id")
    GetBookAJAXResponse get(@PathParam("id") Long id);

    @PUT
    @Path("/ajax/book")
    SearchBookAJAXResponse search(SearchBookAJAXRequest request);

    //TODO move category interface, and rename path, and rename method to getChildrenCategory
    @GET
    @Path("/ajax/book-category/:id")
    GetBookCategoryAJAXResponse getCategory(@PathParam("id") Long parentId);

    //TODO move tag interface
    @GET
    @Path("/ajax/book/tag")
    SearchBookTagAJAXResponse searchTag();

    //TODO rename and move
    @GET
    @Path("/ajax/user/book-borrowed-history")
    SearchBookBorrowedAJAXResponse searchBookBorrowed(SearchBookBorrowedAJAXRequest request);

    //TODO rename path to /ajax/book/:id/borrowed-status
    //TODO rename method to getBorrowedStatus, and add method getSubscribedStatus
    //TODO move
    @GET
    @Path("/ajax/book/:id/borrowed-status")
    GetBookBorrowedStatusResponse borrowedStatus(@PathParam("id") Long bookId);

    //TODO rename path to /ajax/book/:id/borrowed-history
    //TODO rename method to borrow
    //TODO move
    @POST
    @Path("/ajax/book/:id/borrow")
    CreateBookBorrowAJAXResponse borrowed(@PathParam("id") Long bookId, CreateBookBorrowedAJAXRequest request);

    //TODO rename path to /ajax/book/:id/borrowed-history
    //TODO rename method to return
    //TODO move
    @PUT
    @Path("/ajax/book/:id/return")
    UpdateBookBorrowAJAXResponse returned(@PathParam("id") String id);
}
