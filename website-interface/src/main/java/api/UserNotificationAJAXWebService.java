package api;

import api.user.SearchUserNotificationAJAXRequest;
import api.user.SearchUserNotificationAJAXResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.Path;

/**
 * @author murphy.zhang
 */
public interface UserNotificationAJAXWebService {
    @GET
    @Path("/ajax/user/user-notification")
    SearchUserNotificationAJAXResponse search(SearchUserNotificationAJAXRequest request);
}
