package api.user;

import core.framework.api.validate.NotNull;
import core.framework.api.web.service.QueryParam;

/**
 * @author murphy.zhang
 */
public class SearchUserSubscriptionAJAXRequest {
    @NotNull
    @QueryParam(name = "skip")
    public Integer skip;

    @NotNull
    @QueryParam(name = "limit")
    public Integer limit;

    @QueryParam(name = "book_id")
    public Long bookId;

    @QueryParam(name = "status")
    public UserSubscribeAJAXStatus status;
}
