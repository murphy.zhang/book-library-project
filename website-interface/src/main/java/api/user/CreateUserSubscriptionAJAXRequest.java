package api.user;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
public class CreateUserSubscriptionAJAXRequest {
    @NotNull
    @Property(name = "book_id")
    public Long bookId;
}
