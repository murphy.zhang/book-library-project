package api.user;

import core.framework.api.json.Property;

/**
 * @author murphy.zhang
 */
//TODO rename to open/close
public enum UserSubscribeAJAXStatus {
    @Property(name = "NOTIFIED")
    NOTIFIED,
    @Property(name = "UN_NOTIFIED")
    UN_NOTIFIED
}
