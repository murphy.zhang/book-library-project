package api.user;

import core.framework.api.validate.NotNull;
import core.framework.api.web.service.QueryParam;

/**
 * @author murphy.zhang
 */
public class SearchUserNotificationAJAXRequest {
    @NotNull
    @QueryParam(name = "skip")
    public Integer skip;

    @NotNull
    @QueryParam(name = "limit")
    public Integer limit;

    @QueryParam(name = "status")
    public UserNotificationAJAXStatus status;
}
