package api.user;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author murphy.zhang
 */
public class SearchUserNotificationAJAXResponse {
    @Property(name = "total")
    public Long total;

    @Property(name = "user_notifications")
    public List<UserNotification> userNotifications;

    public static class UserNotification {
        @NotNull
        @NotBlank
        @Property(name = "id")
        public String id;

        @NotNull
        @Property(name = "book_id")
        public Long bookId;

        @NotNull
        @Property(name = "type")
        public UserNotificationAJAXType type;

        @NotNull
        @Property(name = "status")
        public UserNotificationAJAXStatus status;

        @NotNull
        @NotBlank
        @Property(name = "content")
        public String content;

        @NotNull
        @Property(name = "created_time")
        public LocalDateTime createdTime;
    }
}
