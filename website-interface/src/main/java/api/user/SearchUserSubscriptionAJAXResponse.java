package api.user;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author murphy.zhang
 */
public class SearchUserSubscriptionAJAXResponse {
    @Property(name = "total")
    public Long total;

    @Property(name = "user_subscriptions")
    public List<UserSubscription> userSubscriptions;

    public static class UserSubscription {
        @NotNull
        @Property(name = "id")
        public String id;

        @NotNull
        @Property(name = "book_id")
        public Long bookId;

        @NotNull
        @Property(name = "status")
        public UserSubscribeAJAXStatus status;

        @NotNull
        @Property(name = "created_time")
        public LocalDateTime createdTime;
    }
}
