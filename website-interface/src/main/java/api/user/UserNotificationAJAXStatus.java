package api.user;

import core.framework.api.json.Property;

/**
 * @author murphy.zhang
 */
public enum UserNotificationAJAXStatus {
    @Property(name = "READ")
    READ,
    @Property(name = "UNREAD")
    UNREAD
}
