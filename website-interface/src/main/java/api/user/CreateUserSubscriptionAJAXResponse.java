package api.user;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
public class CreateUserSubscriptionAJAXResponse {
    @NotNull
    @Property(name = "success")
    public Boolean success;

    @Property(name = "message")
    public String message;
}
