package api.user;

import core.framework.api.json.Property;

/**
 * @author murphy.zhang
 */
public enum UserNotificationAJAXType {
    @Property(name = "EXPIRE")
    EXPIRE,
    @Property(name = "SUBSCRIPTION")
    SUBSCRIPTION
}
