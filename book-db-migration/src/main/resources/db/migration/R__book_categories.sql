CREATE TABLE IF NOT EXISTS `book_categories` (
  `id`                  INT(11)             NOT NULL AUTO_INCREMENT,
  `name`                VARCHAR(100)        NOT NULL,
  `parent_id`           INT(11)             NOT NULL,
  `created_by`          VARCHAR (200)       NOT NULL,
  `updated_by`          VARCHAR (200)       NOT NULL,
  `created_time`        DATETIME            NOT NULL,
  `updated_time`        DATETIME            NOT NULL,
  PRIMARY KEY (`id`),
  KEY `k_name` (`name`),
  KEY `k_parent_id` (`parent_id`)
)
ENGINE=InnoDB
AUTO_INCREMENT=1
DEFAULT CHARSET=utf8;

SELECT COUNT(*) INTO @index FROM information_schema.`COLUMNS` WHERE table_schema=DATABASE() AND column_name='full_path' AND table_name='book_categories';
SET @SQL=IF(@index<1,'ALTER TABLE book_categories ADD full_path VARCHAR(100) NOT NULL AFTER parent_id','select \'Exist Column\';');
PREPARE statement FROM @SQL;
EXECUTE statement;