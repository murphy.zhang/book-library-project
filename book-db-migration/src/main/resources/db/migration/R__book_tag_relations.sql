CREATE TABLE IF NOT EXISTS `book_tag_relations` (
  `id`                  INT(11)         NOT NULL AUTO_INCREMENT,
  `book_id`             INT(11)         NOT NULL,
  `tag_id`              INT(11)         NOT NULL,
  `created_time`        DATETIME        NOT NULL,
  PRIMARY KEY (`id`),
  KEY `k_book_id` (`book_id`),
  KEY `k_tag_id` (`tag_id`)
)
ENGINE=InnoDB
AUTO_INCREMENT=1
DEFAULT CHARSET=utf8;