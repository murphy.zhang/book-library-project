CREATE TABLE IF NOT EXISTS `book_tags` (
  `id`                  INT(11)             NOT NULL AUTO_INCREMENT,
  `name`                VARCHAR(100)        NOT NULL,
  `created_by`          VARCHAR (200)       NOT NULL,
  `updated_by`          VARCHAR (200)       NOT NULL,
  `created_time`        DATETIME            NOT NULL,
  `updated_time`        DATETIME            NOT NULL,
  PRIMARY KEY (`id`),
  KEY `k_name` (`name`)
)
ENGINE=InnoDB
AUTO_INCREMENT=1
DEFAULT CHARSET=utf8;