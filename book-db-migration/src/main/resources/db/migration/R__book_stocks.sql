CREATE TABLE IF NOT EXISTS `book_stocks` (
  `book_id`             INT(11)         NOT NULL,
  `stock`               INT(11)         NOT NULL DEFAULT 0,
  `created_by`          VARCHAR (200)   NOT NULL,
  `updated_by`          VARCHAR (200)   NOT NULL,
  `created_time`        DATETIME        NOT NULL,
  `updated_time`        DATETIME        NOT NULL,
  PRIMARY KEY (`book_id`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8;