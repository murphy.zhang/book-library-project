CREATE TABLE IF NOT EXISTS `book_author_relations` (
  `id`                  INT(11)         NOT NULL AUTO_INCREMENT,
  `book_id`             INT(11)         NOT NULL,
  `author_id`           INT(11)         NOT NULL,
  `created_time`        DATETIME        NOT NULL,
  PRIMARY KEY (`id`),
  KEY `k_book_id` (`book_id`),
  KEY `k_author_id` (`author_id`)
)
ENGINE=InnoDB
AUTO_INCREMENT=1
DEFAULT CHARSET=utf8;