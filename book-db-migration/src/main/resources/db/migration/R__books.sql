CREATE TABLE IF NOT EXISTS `books` (
  `id`                  INT(11)             NOT NULL AUTO_INCREMENT,
  `name`                VARCHAR(100)        NOT NULL,
  `category_id`         INT(11)             NOT NULL,
  `published_time`      DATETIME            NOT NULL,
  `description`         VARCHAR(100)        DEFAULT NULL,
  `created_by`          VARCHAR (200)       NOT NULL,
  `updated_by`          VARCHAR (200)       NOT NULL,
  `created_time`        DATETIME            NOT NULL,
  `updated_time`        DATETIME            NOT NULL,
  PRIMARY KEY (`id`),
  KEY `k_name` (`name`),
  KEY `k_category_id` (`category_id`)
)
ENGINE=InnoDB
AUTO_INCREMENT=1
DEFAULT CHARSET=utf8;