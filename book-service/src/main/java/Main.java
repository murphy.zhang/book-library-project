import app.BookServiceApp;

/**
 * @author murphy.zhang
 */
public class Main {
    public static void main(String[] args) {
        new BookServiceApp().start();
    }
}
