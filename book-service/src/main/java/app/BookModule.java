package app;

import app.api.BOBookAuthorWebService;
import app.api.BOBookCategoryWebService;
import app.api.BOBookTagWebService;
import app.api.BOBookWebService;
import app.api.BookWebService;
import app.api.book.kafka.BookIncreaseStocksMessage;
import app.book.domain.Book;
import app.book.domain.BookAuthor;
import app.book.domain.BookAuthorRelation;
import app.book.domain.BookBorrowedHistory;
import app.book.domain.BookCategory;
import app.book.domain.BookStock;
import app.book.domain.BookTag;
import app.book.domain.BookTagRelation;
import app.book.service.BOBookAuthorService;
import app.book.service.BOBookBorrowedService;
import app.book.service.BOBookCategoryService;
import app.book.service.BOBookService;
import app.book.service.BOBookTagService;
import app.book.service.BookBorrowedService;
import app.book.service.BookService;
import app.book.service.BookView;
import app.book.web.BOBookAuthorWebServiceImpl;
import app.book.web.BOBookCategoryWebServiceImpl;
import app.book.web.BOBookTagWebServiceImpl;
import app.book.web.BOBookWebServiceImpl;
import app.book.web.BookWebServiceImpl;
import core.framework.module.Module;
import core.framework.mongo.module.MongoConfig;

/**
 * @author murphy.zhang
 */
public class BookModule extends Module {
    @Override
    protected void initialize() {
        db().repository(BookAuthor.class);
        db().repository(BookAuthorRelation.class);
        db().repository(BookCategory.class);
        db().repository(BookStock.class);
        db().repository(BookTag.class);
        db().repository(BookTagRelation.class);
        db().repository(Book.class);
        db().view(BookView.class);

        MongoConfig config = config(MongoConfig.class);
        config.uri(requiredProperty("sys.mongo.uri"));
        config.collection(BookBorrowedHistory.class);

        kafka().publish("book-increase-stocks", BookIncreaseStocksMessage.class);

        bind(BOBookAuthorService.class);
        bind(BOBookCategoryService.class);
        bind(BOBookTagService.class);
        bind(BOBookService.class);
        bind(BOBookBorrowedService.class);
        bind(BookService.class);
        bind(BookBorrowedService.class);

        api().service(BOBookAuthorWebService.class, bind(BOBookAuthorWebServiceImpl.class));
        api().service(BOBookCategoryWebService.class, bind(BOBookCategoryWebServiceImpl.class));
        api().service(BOBookTagWebService.class, bind(BOBookTagWebServiceImpl.class));
        api().service(BOBookWebService.class, bind(BOBookWebServiceImpl.class));

        api().service(BookWebService.class, bind(BookWebServiceImpl.class));
    }
}
