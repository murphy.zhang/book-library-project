package app;

import app.api.book.kafka.BookBorrowedWillExpireMessage;
import app.job.BookBorrowedExpireJob;
import core.framework.module.Module;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

/**
 * @author murphy.zhang
 */
public class JobModule extends Module {
    @Override
    protected void initialize() {
        kafka().publish("book-borrowed-will-expire", BookBorrowedWillExpireMessage.class);

        BookBorrowedExpireJob borrowedExpireJob = bind(BookBorrowedExpireJob.class);
        schedule().dailyAt("borrowedExpireJob", borrowedExpireJob, LocalTime.now().plus(5, ChronoUnit.SECONDS));
    }
}
