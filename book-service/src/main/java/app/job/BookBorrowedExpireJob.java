package app.job;

import app.api.book.kafka.BookBorrowedWillExpireMessage;
import app.book.domain.BookBorrowedHistory;
import com.mongodb.ReadPreference;
import core.framework.inject.Inject;
import core.framework.kafka.MessagePublisher;
import core.framework.mongo.MongoCollection;
import core.framework.mongo.Query;
import core.framework.scheduler.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.lte;
import static com.mongodb.client.model.Sorts.ascending;

public class BookBorrowedExpireJob implements Job {
    private final Logger logger = LoggerFactory.getLogger(BookBorrowedExpireJob.class);

    @Inject
    MongoCollection<BookBorrowedHistory> mongoCollection;
    @Inject
    MessagePublisher<BookBorrowedWillExpireMessage> messagePublisher;

    @Override
    public void execute() throws Exception {
        Query query = new Query();
        query.filter = and(eq("status", BookBorrowedHistory.BookBorrowedStatus.BORROWED), lte("expect_returned_time", LocalDate.now().plusDays(1).atStartOfDay()));
        long total = mongoCollection.count(query.filter);
        logger.info("a total of {} book borrowed will expire", total);
        if (total > 0) {
            query.limit = 1000;
            query.readPreference = ReadPreference.secondaryPreferred();
            query.sort = ascending("created_time");
            long number = total / query.limit + (total % query.limit == 0 ? 0 : 1);
            for (int i = 0; i < number; i++) {
                query.skip = i * query.limit;
                mongoCollection.find(query).forEach(h -> {
                    BookBorrowedWillExpireMessage message = new BookBorrowedWillExpireMessage();
                    message.id = h.id;
                    message.bookId = h.bookId;
                    message.userId = h.userId;
                    message.expectReturnedTime = h.expectReturnedTime;
                    messagePublisher.publish(message);
                });
            }
        }
    }
}
