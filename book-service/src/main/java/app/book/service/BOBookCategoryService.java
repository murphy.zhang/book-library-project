package app.book.service;

import app.api.book.category.BOCreateBookCategoryRequest;
import app.api.book.category.BOCreateBookCategoryResponse;
import app.api.book.category.BOGetBookCategoryResponse;
import app.book.domain.BookCategory;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.web.exception.BadRequestException;
import core.framework.web.exception.ConflictException;
import core.framework.web.exception.NotFoundException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author murphy.zhang
 */
public class BOBookCategoryService {
    @Inject
    Repository<BookCategory> categoryRepository;

    public BOGetBookCategoryResponse get(Long parentId) {
        List<BookCategory> categoryList = categoryRepository.select("parent_id = ?", parentId);
        BOGetBookCategoryResponse result = new BOGetBookCategoryResponse();
        result.categories = categoryList.stream().map(this::categoryView).collect(Collectors.toList());
        return result;
    }

    private BOGetBookCategoryResponse.Category categoryView(BookCategory c) {
        BOGetBookCategoryResponse.Category result = new BOGetBookCategoryResponse.Category();
        result.id = c.id;
        result.name = c.name;
        result.parentId = c.parentId;
        result.fullPath = c.fullPath;
        return result;
    }

    public BOCreateBookCategoryResponse create(BOCreateBookCategoryRequest request) {
        categoryRepository.selectOne("name = ? and parent_id = ?", request.name, request.parentId).ifPresent(c -> {
            throw new ConflictException("category already exists, name=" + request.name);
        });
        BookCategory category = new BookCategory();
        category.name = request.name;
        category.parentId = request.parentId;
        category.fullPath = "";
        category.createdTime = LocalDateTime.now();
        category.updatedTime = category.createdTime;
        category.createdBy = request.createdBy;
        category.updatedBy = category.createdBy;
        category.id = categoryRepository.insert(category).orElseThrow(() -> new BadRequestException("category create error."));
        if (request.parentId > 0) {
            BookCategory parentCategory = categoryRepository.get(request.parentId).orElseThrow(() -> new NotFoundException("category is not found, id=" + request.parentId));
            category.fullPath = parentCategory.fullPath + "," + category.id;
        } else {
            category.fullPath = category.id.toString();
        }
        categoryRepository.update(category);
        BOCreateBookCategoryResponse result = new BOCreateBookCategoryResponse();
        result.id = category.id;
        result.name = category.name;
        return result;
    }
}
