package app.book.service;

import app.api.book.tag.BOCreateBookTagRequest;
import app.api.book.tag.BOCreateBookTagResponse;
import app.api.book.tag.BOSearchBookTagResponse;
import app.book.domain.BookTag;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.web.exception.BadRequestException;
import core.framework.web.exception.ConflictException;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

/**
 * @author murphy.zhang
 */
public class BOBookTagService {
    @Inject
    Repository<BookTag> tagRepository;

    public BOSearchBookTagResponse search() {
        BOSearchBookTagResponse result = new BOSearchBookTagResponse();
        result.tags = tagRepository.select().fetch().stream().map(this::tagView).collect(Collectors.toList());
        return result;
    }

    private BOSearchBookTagResponse.Tag tagView(BookTag tag) {
        BOSearchBookTagResponse.Tag result = new BOSearchBookTagResponse.Tag();
        result.id = tag.id;
        result.name = tag.name;
        return result;
    }

    public BOCreateBookTagResponse create(BOCreateBookTagRequest request) {
        tagRepository.selectOne("name = ?", request.name).ifPresent(author -> {
            throw new ConflictException("tag already exists, name=" + request.name);
        });
        BookTag tag = new BookTag();
        tag.name = request.name;
        tag.createdTime = LocalDateTime.now();
        tag.updatedTime = tag.createdTime;
        tag.createdBy = request.createdBy;
        tag.updatedBy = tag.createdBy;
        tag.id = tagRepository.insert(tag).orElseThrow(() -> new BadRequestException("tag create error."));
        BOCreateBookTagResponse result = new BOCreateBookTagResponse();
        result.id = tag.id;
        result.name = tag.name;
        return result;
    }
}
