package app.book.service;

import app.api.book.ListBookRequest;
import app.api.book.ListBookResponse;
import app.api.book.GetBookResponse;
import app.api.book.SearchBookRequest;
import app.api.book.SearchBookResponse;
import app.api.book.category.GetBookCategoryResponse;
import app.api.book.stock.GetBookStockResponse;
import app.api.book.tag.SearchBookTagResponse;
import app.book.domain.Book;
import app.book.domain.BookAuthor;
import app.book.domain.BookAuthorRelation;
import app.book.domain.BookCategory;
import app.book.domain.BookStock;
import app.book.domain.BookTag;
import app.book.domain.BookTagRelation;
import app.book.util.QueryUtil;
import core.framework.db.Database;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.util.Lists;
import core.framework.util.Strings;
import core.framework.web.exception.NotFoundException;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author murphy.zhang
 */
public class BookService {
    @Inject
    Repository<Book> bookRepository;
    @Inject
    Repository<BookCategory> categoryRepository;
    @Inject
    Repository<BookTag> tagRepository;
    @Inject
    Repository<BookAuthor> authorRepository;
    @Inject
    Repository<BookStock> stockRepository;
    @Inject
    Repository<BookTagRelation> tagRelationsRepository;
    @Inject
    Repository<BookAuthorRelation> bookAuthorRelationsRepository;
    @Inject
    Database database;

    public GetBookResponse get(Long id) {
        Book book = bookRepository.get(id).orElseThrow(() -> new NotFoundException("book is not found, id=" + id));
        GetBookResponse result = new GetBookResponse();
        result.id = book.id;
        result.name = book.name;
        result.description = book.description;
        result.publishedTime = book.publishedTime;
        result.createdTime = book.createdTime;
        result.stock = stockRepository.selectOne("book_id = ?", id).orElseThrow(
            () -> new NotFoundException("book stock is not found. id=" + id)
        ).stock;
        result.category = getBookCategory(book.categoryId);
        result.authors = getBookAuthor(id);
        result.tags = getBookTag(id);
        return result;
    }

    private GetBookResponse.Category getBookCategory(Long bookCategoryId) {
        BookCategory c = categoryRepository.get(bookCategoryId).orElseThrow(() -> new NotFoundException("category is not found. id=" + bookCategoryId));
        GetBookResponse.Category category = new GetBookResponse.Category();
        category.id = c.id;
        category.name = c.name;
        return category;
    }

    private List<GetBookResponse.Author> getBookAuthor(Long id) {
        List<Long> authorIds = bookAuthorRelationsRepository.select("book_id = ?", id).stream().map(t -> t.authorId).collect(Collectors.toList());
        QueryUtil.Query query = QueryUtil.in("id", authorIds.toArray());
        return authorRepository.select(query.condition, query.params).stream().map(a -> {
            GetBookResponse.Author author = new GetBookResponse.Author();
            author.id = a.id;
            author.name = a.name;
            author.dynasty = a.dynasty;
            author.nationality = a.nationality;
            return author;
        }).collect(Collectors.toList());
    }

    private List<GetBookResponse.Tag> getBookTag(Long id) {
        List<Long> tagIds = tagRelationsRepository.select("book_id = ?", id).stream().map(t -> t.tagId).collect(Collectors.toList());
        QueryUtil.Query query = QueryUtil.in("id", tagIds.toArray());
        return tagRepository.select(query.condition, query.params).stream().map(t -> {
            GetBookResponse.Tag tag = new GetBookResponse.Tag();
            tag.id = t.id;
            tag.name = t.name;
            return tag;
        }).collect(Collectors.toList());
    }

    public SearchBookResponse search(SearchBookRequest request) {
        SearchBookResponse result = new SearchBookResponse();
        String projection = "count(DISTINCT b.id)";
        String columns = "DISTINCT b.id, b.name, b.description, b.published_time, b.created_time, b.updated_time, s.stock, a.id as author_id, a.name as author_name, a.dynasty, a.nationality, c.id as category_id, c.name as category_name";
        StringBuilder sql = new StringBuilder(512);
        sql.append("SELECT {} FROM books b INNER JOIN book_stocks s ON b.id = s.book_id INNER JOIN book_author_relations ba ON b.id = ba.book_id INNER JOIN book_categories c ON b.category_id = c.id INNER JOIN book_tag_relations m ON b.id = m.book_id INNER JOIN book_authors a ON a.id = ba.author_id");
        List<Object> params = Lists.newArrayList();
        StringBuilder whereClause = new StringBuilder();
        if (!Strings.isBlank(request.name)) {
            append(params, whereClause, "INSTR(b.name, ?)", request.name);
        }
        if (!Strings.isBlank(request.authorName)) {
            append(params, whereClause, "INSTR(a.name, ?)", request.authorName);
        }
        if (request.categoryId != null) {
            append(params, whereClause, "b.category_id = ?", request.categoryId);
        }
        if (request.tagIds != null && !request.tagIds.isEmpty()) {
            QueryUtil.Query query = QueryUtil.in("m.tag_id", request.tagIds.toArray());
            append(params, whereClause, query.condition, query.params);
        }
        if (whereClause.length() > 0) {
            sql.append(" where ").append(whereClause);
        }
        result.total = (long) database.selectOne(Strings.format(sql.toString(), projection), Integer.class, params.toArray()).orElse(0);
        if (result.total <= 0) {
            return result;
        }
        sql.append(" order by b.id desc limit ?, ?");
        params.add(request.skip);
        params.add(request.limit);
        Object[] results = params.toArray();
        List<BookView> bookViews = database.select(Strings.format(sql.toString(), columns), BookView.class, results);
        if (!bookViews.isEmpty()) {
            result.books = bookViews.stream().map(this::bookSearchView).collect(Collectors.toList());
            Object[] bookIdArray = bookViews.stream().map(b -> b.id).toArray();
            bookSearchAuthorView(result, bookIdArray);
            bookSearchTagView(result, bookIdArray);
        } else {
            result.books = Collections.emptyList();
        }
        return result;
    }

    private void bookSearchTagView(SearchBookResponse result, Object[] bookIds) {
        QueryUtil.Query query = QueryUtil.in("book_id", bookIds);
        List<BookTagRelation> bookTagRelations = tagRelationsRepository.select(query.condition, query.params);
        query = QueryUtil.in("id", bookTagRelations.stream().map(m -> m.tagId).toArray());
        Map<Long, SearchBookResponse.Tag> tagMap = tagRepository.select(query.condition, query.params)
                .stream().map(this::bookSearchTagView).collect(Collectors.toMap(t -> t.id, t -> t));
        Map<Long, List<SearchBookResponse.Tag>> bookTagMap = bookTagRelations.stream().collect(Collectors.groupingBy(m -> m.bookId, Collectors.mapping(m -> tagMap.get(m.tagId), Collectors.toList())));
        result.books.parallelStream().forEach(b -> b.tags = bookTagMap.get(b.id));
    }

    private SearchBookResponse.Tag bookSearchTagView(BookTag t) {
        SearchBookResponse.Tag tag = new SearchBookResponse.Tag();
        tag.id = t.id;
        tag.name = t.name;
        return tag;
    }

    private SearchBookResponse.Book bookSearchView(BookView b) {
        SearchBookResponse.Book book = new SearchBookResponse.Book();
        SearchBookResponse.Category category = new SearchBookResponse.Category();
        category.id = b.categoryId;
        category.name = b.categoryName;
        book.category = category;
        book.id = b.id;
        book.name = b.name;
        book.description = b.description;
        book.publishedTime = b.publishedTime;
        book.createdTime = b.createdTime;
        book.updatedTime = b.updatedTime;
        book.stock = b.stock;
        return book;
    }

    private void bookSearchAuthorView(SearchBookResponse result, Object[] bookIds) {
        QueryUtil.Query query = QueryUtil.in("book_id", bookIds);
        List<BookAuthorRelation> bookAuthorRelations = bookAuthorRelationsRepository.select(query.condition, query.params);
        query = QueryUtil.in("id", bookAuthorRelations.stream().map(r -> r.authorId).toArray());
        Map<Long, SearchBookResponse.Author> authorMap = authorRepository.select(query.condition, query.params)
                .stream().map(this::bookSearchAuthorView).collect(Collectors.toMap(a -> a.id, a -> a));
        Map<Long, List<SearchBookResponse.Author>> bookAuthorMap = bookAuthorRelations.stream().collect(Collectors.groupingBy(m -> m.bookId, Collectors.mapping(r -> authorMap.get(r.authorId), Collectors.toList())));
        result.books.parallelStream().forEach(b -> b.authors = bookAuthorMap.get(b.id));
    }

    private SearchBookResponse.Author bookSearchAuthorView(BookAuthor a) {
        SearchBookResponse.Author result = new SearchBookResponse.Author();
        result.id = a.id;
        result.name = a.name;
        result.dynasty = a.dynasty;
        result.nationality = a.nationality;
        return result;
    }

    private void append(List<Object> params, StringBuilder whereClause, String condition, Object... conditionParams) {
        if (Strings.isBlank(condition)) throw new Error("condition must not be blank");
        if (whereClause.length() > 0) {
            whereClause.append(" AND ");
        }
        whereClause.append(condition);
        Collections.addAll(params, conditionParams);
    }

    public ListBookResponse list(ListBookRequest request) {
        QueryUtil.Query query = QueryUtil.in("id", request.ids.toArray());
        ListBookResponse result = new ListBookResponse();
        result.books = bookRepository.select(query.condition, query.params).stream().map(this::listBookView).collect(Collectors.toList());
        return result;
    }

    private ListBookResponse.Book listBookView(Book book) {
        ListBookResponse.Book result = new ListBookResponse.Book();
        result.id = book.id;
        result.name = book.name;
        return result;
    }

    public GetBookCategoryResponse getCategory(Long parentId) {
        List<BookCategory> categoryList = categoryRepository.select("parent_id = ?", parentId);
        GetBookCategoryResponse result = new GetBookCategoryResponse();
        result.categories = categoryList.stream().map(this::categoryView).collect(Collectors.toList());
        return result;
    }

    private GetBookCategoryResponse.Category categoryView(BookCategory c) {
        GetBookCategoryResponse.Category result = new GetBookCategoryResponse.Category();
        result.id = c.id;
        result.name = c.name;
        result.parentId = c.parentId;
        result.fullPath = c.fullPath;
        return result;
    }

    public GetBookStockResponse getStock(Long bookId) {
        BookStock bookStock = stockRepository.get(bookId).orElseThrow(() -> new NotFoundException("book stock is not found, bookId=" + bookId));
        GetBookStockResponse result = new GetBookStockResponse();
        result.bookId = bookId;
        result.stock = bookStock.stock;
        return result;
    }

    public SearchBookTagResponse searchTag() {
        Query<BookTag> query = tagRepository.select();
        SearchBookTagResponse result = new SearchBookTagResponse();
        result.tags = query.fetch().stream().map(this::tagView).collect(Collectors.toList());
        return result;
    }

    private SearchBookTagResponse.Tag tagView(BookTag t) {
        SearchBookTagResponse.Tag result = new SearchBookTagResponse.Tag();
        result.id = t.id;
        result.name = t.name;
        return result;
    }
}
