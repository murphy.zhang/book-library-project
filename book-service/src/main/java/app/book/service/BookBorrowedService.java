package app.book.service;

import app.api.book.borrowed.BookBorrowedStatus;
import app.api.book.borrowed.CreateBookBorrowResponse;
import app.api.book.borrowed.CreateBookBorrowedRequest;
import app.api.book.borrowed.SearchBookBorrowedRequest;
import app.api.book.borrowed.SearchBookBorrowedResponse;
import app.api.book.borrowed.UpdateBookBorrowResponse;
import app.api.book.borrowed.UpdateBookBorrowedRequest;
import app.api.book.kafka.BookIncreaseStocksMessage;
import app.book.domain.Book;
import app.book.domain.BookBorrowedHistory;
import com.mongodb.ReadPreference;
import core.framework.db.Database;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.kafka.MessagePublisher;
import core.framework.mongo.MongoCollection;
import core.framework.mongo.Query;
import core.framework.web.exception.BadRequestException;
import core.framework.web.exception.ConflictException;
import core.framework.web.exception.NotFoundException;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;
import static com.mongodb.client.model.Sorts.descending;

public class BookBorrowedService {
    private final Logger logger = LoggerFactory.getLogger(BookBorrowedService.class);

    @Inject
    MongoCollection<BookBorrowedHistory> historyMongoCollection;
    @Inject
    Repository<Book> bookRepository;
    @Inject
    Database database;
    @Inject
    MessagePublisher<BookIncreaseStocksMessage> messagePublisher;

    public CreateBookBorrowResponse borrowed(Long bookId, CreateBookBorrowedRequest request) {
        bookRepository.get(bookId).orElseThrow(() -> new NotFoundException("book is not found, id=" + bookId));
        historyMongoCollection.findOne(and(eq("book_id", bookId), eq("user_id", request.userId), eq("status", BookBorrowedHistory.BookBorrowedStatus.BORROWED))).ifPresent(h -> {
            logger.warn("book is borrowed, booId = {}, userId = {}", bookId, request.userId);
            throw new ConflictException("book is borrowed, bookId=" + bookId);
        });
        LocalDateTime now = LocalDateTime.now();
        int execute = database.execute("UPDATE book_stocks SET stock = stock - 1, updated_time = ? WHERE book_id = ? AND stock > 0", now, bookId);
        if (execute == 0) {
            logger.info("book is no stock, bookId={}", bookId);
            throw new NotFoundException("book is no stock, bookId=" + bookId);
        }
        BookBorrowedHistory history = new BookBorrowedHistory();
        history.id = UUID.randomUUID().toString();
        history.bookId = bookId;
        history.userId = request.userId;
        history.status = BookBorrowedHistory.BookBorrowedStatus.BORROWED;
        history.expectReturnedTime = request.expectReturnedTime;
        history.createdTime = now;
        history.updatedTime = now;
        historyMongoCollection.insert(history);
        CreateBookBorrowResponse result = new CreateBookBorrowResponse();
        result.id = history.id;
        result.createdTime = history.createdTime;
        return result;
    }

    public UpdateBookBorrowResponse returned(String id, UpdateBookBorrowedRequest request) {
        BookBorrowedHistory history = historyMongoCollection.get(id).orElseThrow(() -> new NotFoundException("book borrowed history is not found, id=" + id));
        if (!history.userId.equals(request.userId)) {
            logger.error("error user, expected user is {}, actual user is {}", history.userId, request.userId);
            throw new BadRequestException("error user, userId=" + request.userId);
        }
        LocalDateTime now = LocalDateTime.now();
        int execute = database.execute("UPDATE book_stocks SET stock = stock + 1, updated_time = ? WHERE book_id = ?", now, history.bookId);
        if (execute == 0) {
            logger.info("failure to return books, bookId={}, userId={}", history.bookId, history.userId);
            throw new BadRequestException("failure to return books, bookId=" + history.bookId);
        }
        history.actualReturnedTime = now;
        history.updatedTime = now;
        history.status = BookBorrowedHistory.BookBorrowedStatus.RETURNED;
        historyMongoCollection.replace(history);
        BookIncreaseStocksMessage message = new BookIncreaseStocksMessage();
        message.bookId = history.bookId;
        messagePublisher.publish(message);
        UpdateBookBorrowResponse result = new UpdateBookBorrowResponse();
        result.id = id;
        result.actualReturnedTime = history.actualReturnedTime;
        return result;
    }

    public SearchBookBorrowedResponse search(SearchBookBorrowedRequest request) {
        SearchBookBorrowedResponse result = new SearchBookBorrowedResponse();
        Query query = new Query();
        query.limit = request.limit;
        query.skip = request.skip;
        query.readPreference = ReadPreference.secondaryPreferred();
        List<Bson> filters = new ArrayList<>();
        filters.add(eq("user_id", request.userId));
        if (request.status != null) {
            filters.add(eq("status", BookBorrowedHistory.BookBorrowedStatus.valueOf(request.status.name())));
        }
        if (request.bookId != null) {
            filters.add(eq("book_id", request.bookId));
        }
        if (request.bookIds != null && !request.bookIds.isEmpty()) {
            filters.add(in("book_id", request.bookIds));
        }
        query.filter = and(filters.toArray(new Bson[0]));
        result.total = historyMongoCollection.count(query.filter);
        if (result.total > 0) {
            query.sort = descending("created_time");
            result.bookBorrowedHistories = historyMongoCollection.find(query).stream().map(this::historyView).collect(Collectors.toList());
        }
        return result;
    }

    private SearchBookBorrowedResponse.BookBorrowedHistory historyView(BookBorrowedHistory history) {
        SearchBookBorrowedResponse.BookBorrowedHistory result = new SearchBookBorrowedResponse.BookBorrowedHistory();
        result.id = history.id;
        result.bookId = history.bookId;
        result.userId = history.userId;
        result.status = BookBorrowedStatus.valueOf(history.status.name());
        result.expectReturnedTime = history.expectReturnedTime;
        result.actualReturnedTime = history.actualReturnedTime;
        result.createdTime = history.createdTime;
        return result;
    }
}
