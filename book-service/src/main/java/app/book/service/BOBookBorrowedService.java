package app.book.service;

import app.api.book.borrowed.BOSearchBookBorrowedRequest;
import app.api.book.borrowed.BOSearchBookBorrowedResponse;
import app.api.book.borrowed.BookBorrowedStatus;
import app.book.domain.BookBorrowedHistory;
import com.mongodb.ReadPreference;
import core.framework.inject.Inject;
import core.framework.mongo.MongoCollection;
import core.framework.mongo.Query;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.descending;

public class BOBookBorrowedService {
    @Inject
    MongoCollection<BookBorrowedHistory> historyMongoCollection;

    public BOSearchBookBorrowedResponse search(BOSearchBookBorrowedRequest request) {
        BOSearchBookBorrowedResponse result = new BOSearchBookBorrowedResponse();
        Query query = new Query();
        query.limit = request.limit;
        query.skip = request.skip;
        query.readPreference = ReadPreference.secondaryPreferred();
        List<Bson> filters = new ArrayList<>();
        filters.add(eq("book_id", request.bookId));
        if (request.userId != null) {
            filters.add(eq("user_id", request.userId));
        }
        if (request.status != null) {
            filters.add(eq("status", BookBorrowedHistory.BookBorrowedStatus.valueOf(request.status.name())));
        }
        query.filter = and(filters.toArray(new Bson[0]));
        result.total = historyMongoCollection.count(query.filter);
        if (result.total > 0) {
            query.sort = descending("created_time");
            result.bookBorrowedHistories = historyMongoCollection.find(query).stream().map(this::historyView).collect(Collectors.toList());
        }
        return result;
    }

    private BOSearchBookBorrowedResponse.BookBorrowedHistory historyView(BookBorrowedHistory history) {
        BOSearchBookBorrowedResponse.BookBorrowedHistory result = new BOSearchBookBorrowedResponse.BookBorrowedHistory();
        result.id = history.id;
        result.bookId = history.bookId;
        result.userId = history.userId;
        result.status = BookBorrowedStatus.valueOf(history.status.name());
        result.expectReturnedTime = history.expectReturnedTime;
        result.actualReturnedTime = history.actualReturnedTime;
        result.createdTime = history.createdTime;
        return result;
    }
}
