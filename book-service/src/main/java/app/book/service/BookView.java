package app.book.service;

import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;
import core.framework.db.Column;

import java.time.LocalDateTime;

public class BookView {
    @NotNull
    @Column(name = "id")
    public Long id;

    @NotNull
    @NotBlank
    @Column(name = "name")
    public String name;

    @Column(name = "description")
    public String description;

    @NotNull
    @Column(name = "published_time")
    public LocalDateTime publishedTime;

    @NotNull
    @Column(name = "created_time")
    public LocalDateTime createdTime;

    @NotNull
    @Column(name = "updated_time")
    public LocalDateTime updatedTime;

    @NotNull
    @Column(name = "stock")
    public Integer stock;

    @NotNull
    @Column(name = "category_id")
    public Long categoryId;

    @NotNull
    @NotBlank
    @Column(name = "category_name")
    public String categoryName;
}
