package app.book.service;

import app.api.book.BOCreateBookRequest;
import app.api.book.BOCreateBookResponse;
import app.api.book.BOGetBookResponse;
import app.api.book.BOSearchBookRequest;
import app.api.book.BOSearchBookResponse;
import app.api.book.BOUpdateBookRequest;
import app.api.book.BOUpdateBookResponse;
import app.api.book.kafka.BookIncreaseStocksMessage;
import app.api.book.stock.BOGetBookStockResponse;
import app.book.domain.Book;
import app.book.domain.BookAuthor;
import app.book.domain.BookAuthorRelation;
import app.book.domain.BookCategory;
import app.book.domain.BookStock;
import app.book.domain.BookTag;
import app.book.domain.BookTagRelation;
import app.book.util.QueryUtil;
import core.framework.db.Database;
import core.framework.db.Repository;
import core.framework.db.Transaction;
import core.framework.inject.Inject;
import core.framework.kafka.MessagePublisher;
import core.framework.util.Lists;
import core.framework.util.Strings;
import core.framework.web.exception.ConflictException;
import core.framework.web.exception.NotFoundException;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author murphy.zhang
 */
public class BOBookService {
    @Inject
    Repository<Book> bookRepository;
    @Inject
    Repository<BookCategory> categoryRepository;
    @Inject
    Repository<BookTag> tagRepository;
    @Inject
    Repository<BookAuthor> authorRepository;
    @Inject
    Repository<BookStock> stockRepository;
    @Inject
    Repository<BookTagRelation> tagRelationsRepository;
    @Inject
    Repository<BookAuthorRelation> bookAuthorRelationsRepository;
    @Inject
    Database database;
    @Inject
    MessagePublisher<BookIncreaseStocksMessage> messagePublisher;

    public BOGetBookResponse get(Long id) {
        Book book = bookRepository.get(id).orElseThrow(() -> new NotFoundException("book is not found, id=" + id));
        BOGetBookResponse result = new BOGetBookResponse();
        result.id = book.id;
        result.name = book.name;
        result.description = book.description;
        result.publishedTime = book.publishedTime;
        result.createdTime = book.createdTime;
        result.category = getBookCategory(book.categoryId);
        result.authors = getBookAuthor(id);
        result.tags = getBookTag(id);
        return result;
    }

    private BOGetBookResponse.Category getBookCategory(Long bookCategoryId) {
        BookCategory c = categoryRepository.get(bookCategoryId).orElseThrow(() -> new NotFoundException("category is not found. id=" + bookCategoryId));
        BOGetBookResponse.Category category = new BOGetBookResponse.Category();
        category.id = c.id;
        category.name = c.name;
        return category;
    }

    private List<BOGetBookResponse.Author> getBookAuthor(Long id) {
        List<Long> authorIds = bookAuthorRelationsRepository.select("book_id = ?", id).stream().map(t -> t.authorId).collect(Collectors.toList());
        QueryUtil.Query query = QueryUtil.in("id", authorIds.toArray());
        return authorRepository.select(query.condition, query.params).stream().map(a -> {
            BOGetBookResponse.Author author = new BOGetBookResponse.Author();
            author.id = a.id;
            author.name = a.name;
            author.dynasty = a.dynasty;
            author.nationality = a.nationality;
            return author;
        }).collect(Collectors.toList());
    }

    private List<BOGetBookResponse.Tag> getBookTag(Long id) {
        List<Long> tagIds = tagRelationsRepository.select("book_id = ?", id).stream().map(t -> t.tagId).collect(Collectors.toList());
        QueryUtil.Query query = QueryUtil.in("id", tagIds.toArray());
        return tagRepository.select(query.condition, query.params).stream().map(t -> {
            BOGetBookResponse.Tag tag = new BOGetBookResponse.Tag();
            tag.id = t.id;
            tag.name = t.name;
            return tag;
        }).collect(Collectors.toList());
    }

    public BOGetBookStockResponse getStock(Long bookId) {
        BookStock bookStock = stockRepository.get(bookId).orElseThrow(() -> new NotFoundException("book stock is not found, bookId=" + bookId));
        BOGetBookStockResponse result = new BOGetBookStockResponse();
        result.bookId = bookId;
        result.stock = bookStock.stock;
        return result;
    }

    //TODO is that okay support one million data for association queries?
    //TODO is it necessary to modify the table structure?
    public BOSearchBookResponse search(BOSearchBookRequest request) {
        BOSearchBookResponse result = new BOSearchBookResponse();
        String projection = "count(DISTINCT b.id)";
        String columns = "DISTINCT b.id, b.name, b.description, b.published_time, b.created_time, b.updated_time, s.stock, c.id as category_id, c.name as category_name";
        StringBuilder sql = new StringBuilder(512);
        sql.append("SELECT {} FROM books b INNER JOIN book_stocks s ON b.id = s.book_id INNER JOIN book_author_relations ba ON b.id = ba.book_id INNER JOIN book_categories c ON b.category_id = c.id INNER JOIN book_tag_relations m ON b.id = m.book_id INNER JOIN book_authors a ON a.id = ba.author_id");
        List<Object> params = Lists.newArrayList();
        StringBuilder whereClause = new StringBuilder();
        if (!Strings.isBlank(request.name)) {
            append(params, whereClause, "INSTR(b.name, ?)", request.name);
        }
        if (!Strings.isBlank(request.description)) {
            append(params, whereClause, "INSTR(b.description, ?)", request.description);
        }
        if (!Strings.isBlank(request.authorName)) {
            append(params, whereClause, "INSTR(a.name, ?)", request.authorName);
        }
        if (request.categoryId != null) {
            append(params, whereClause, "b.category_id = ?", request.categoryId);
        }
        if (request.tagIds != null && !request.tagIds.isEmpty()) {
            QueryUtil.Query query = QueryUtil.in("m.tag_id", request.tagIds.toArray());
            append(params, whereClause, query.condition, query.params);
        }
        if (whereClause.length() > 0) {
            sql.append(" where ").append(whereClause);
        }
        result.total = (long) database.selectOne(Strings.format(sql.toString(), projection), Integer.class, params.toArray()).orElse(0);
        if (result.total <= 0) {
            return result;
        }
        sql.append(" order by b.id desc limit ?, ?");
        params.add(request.skip);
        params.add(request.limit);
        Object[] results = params.toArray();
        List<BookView> bookViews = database.select(Strings.format(sql.toString(), columns), BookView.class, results);
        if (!bookViews.isEmpty()) {
            result.books = bookViews.stream().map(this::bookSearchView).collect(Collectors.toList());
            Object[] bookIdArray = bookViews.stream().map(b -> b.id).toArray();
            bookSearchAuthorView(result, bookIdArray);
            bookSearchTagView(result, bookIdArray);
        } else {
            result.books = Collections.emptyList();
        }
        return result;
    }

    private void bookSearchTagView(BOSearchBookResponse result, Object[] bookIds) {
        QueryUtil.Query query = QueryUtil.in("book_id", bookIds);
        List<BookTagRelation> bookTagRelations = tagRelationsRepository.select(query.condition, query.params);
        query = QueryUtil.in("id", bookTagRelations.stream().map(m -> m.tagId).toArray());
        Map<Long, BOSearchBookResponse.Tag> tagMap = tagRepository.select(query.condition, query.params)
                .stream().map(this::bookSearchTagView).collect(Collectors.toMap(t -> t.id, t -> t));
        Map<Long, List<BOSearchBookResponse.Tag>> bookTagMap = bookTagRelations.stream().collect(Collectors.groupingBy(m -> m.bookId, Collectors.mapping(m -> tagMap.get(m.tagId), Collectors.toList())));
        result.books.parallelStream().forEach(b -> b.tags = bookTagMap.get(b.id));
    }

    private BOSearchBookResponse.Tag bookSearchTagView(BookTag t) {
        BOSearchBookResponse.Tag tag = new BOSearchBookResponse.Tag();
        tag.id = t.id;
        tag.name = t.name;
        return tag;
    }

    private BOSearchBookResponse.Book bookSearchView(BookView b) {
        BOSearchBookResponse.Book book = new BOSearchBookResponse.Book();
        BOSearchBookResponse.Category category = new BOSearchBookResponse.Category();
        category.id = b.categoryId;
        category.name = b.categoryName;
        book.category = category;
        book.id = b.id;
        book.name = b.name;
        book.description = b.description;
        book.publishedTime = b.publishedTime;
        book.createdTime = b.createdTime;
        book.updatedTime = b.updatedTime;
        book.stock = b.stock;
        return book;
    }

    private void bookSearchAuthorView(BOSearchBookResponse result, Object[] bookIds) {
        QueryUtil.Query query = QueryUtil.in("book_id", bookIds);
        List<BookAuthorRelation> bookAuthorRelations = bookAuthorRelationsRepository.select(query.condition, query.params);
        query = QueryUtil.in("id", bookAuthorRelations.stream().map(r -> r.authorId).toArray());
        Map<Long, BOSearchBookResponse.Author> authorMap = authorRepository.select(query.condition, query.params)
                .stream().map(this::bookSearchAuthorView).collect(Collectors.toMap(a -> a.id, a -> a));
        Map<Long, List<BOSearchBookResponse.Author>> bookAuthorMap = bookAuthorRelations.stream().collect(Collectors.groupingBy(m -> m.bookId, Collectors.mapping(r -> authorMap.get(r.authorId), Collectors.toList())));
        result.books.parallelStream().forEach(b -> b.authors = bookAuthorMap.get(b.id));
    }

    private BOSearchBookResponse.Author bookSearchAuthorView(BookAuthor a) {
        BOSearchBookResponse.Author result = new BOSearchBookResponse.Author();
        result.id = a.id;
        result.name = a.name;
        result.dynasty = a.dynasty;
        result.nationality = a.nationality;
        return result;
    }

    private void append(List<Object> params, StringBuilder whereClause, String condition, Object... conditionParams) {
        if (Strings.isBlank(condition)) throw new Error("condition must not be blank");
        if (whereClause.length() > 0) {
            whereClause.append(" AND ");
        }
        whereClause.append(condition);
        Collections.addAll(params, conditionParams);
    }

    public BOCreateBookResponse create(BOCreateBookRequest request) {
        Book book = new Book();
        synchronized (BOBookService.class) {
            bookRepository.selectOne("name = ? AND category_id = ?", request.name, request.categoryId).ifPresent(b -> {
                throw new ConflictException("book is exists, name=" + request.name);
            });
            try (Transaction transaction = database.beginTransaction()) {
                LocalDateTime now = LocalDateTime.now();
                book.name = request.name;
                book.description = request.description;
                book.categoryId = request.categoryId;
                book.publishedTime = request.publishedTime;
                book.createdTime = now;
                book.updatedTime = now;
                book.createdBy = request.createdBy;
                book.updatedBy = request.createdBy;
                book.id = bookRepository.insert(book).orElseThrow(() -> {
                    throw new Error("book persistence error");
                });
                insertBookStock(book.id, request, now);
                batchInsertBookAuthorRelation(book.id, request.authorIds, now);
                batchInsertBookTagRelation(book.id, request.tagIds, now);
                transaction.commit();
            }
        }
        BOCreateBookResponse result = new BOCreateBookResponse();
        result.id = book.id;
        result.name = book.name;
        result.createdTime = book.createdTime;
        return result;
    }

    private void insertBookStock(Long bookId, BOCreateBookRequest request, LocalDateTime now) {
        BookStock bookStock = new BookStock();
        bookStock.bookId = bookId;
        bookStock.stock = request.stock;
        bookStock.createdTime = now;
        bookStock.updatedTime = now;
        bookStock.createdBy = request.createdBy;
        bookStock.updatedBy = request.createdBy;
        stockRepository.insert(bookStock);
    }

    private void batchInsertBookAuthorRelation(Long bookId, List<Long> authorIds, LocalDateTime now) {
        List<BookAuthorRelation> bookAuthorRelations = authorIds.stream().map(authorId -> {
            BookAuthorRelation r = new BookAuthorRelation();
            r.bookId = bookId;
            r.authorId = authorId;
            r.createdTime = now;
            return r;
        }).collect(Collectors.toList());
        bookAuthorRelationsRepository.batchInsert(bookAuthorRelations);
    }

    private void batchInsertBookTagRelation(Long bookId, List<Long> tagIds, LocalDateTime now) {
        List<BookTagRelation> mappings = tagIds.stream().map(tagId -> {
            BookTagRelation mapping = new BookTagRelation();
            mapping.bookId = bookId;
            mapping.tagId = tagId;
            mapping.createdTime = now;
            return mapping;
        }).collect(Collectors.toList());
        tagRelationsRepository.batchInsert(mappings);
    }

    public BOUpdateBookResponse update(Long id, BOUpdateBookRequest request) {
        Book book = bookRepository.get(id).orElseThrow(() -> new NotFoundException("book is not found, id=" + id));
        boolean needPushIncreaseStocksMessage = false;
        try (Transaction transaction = database.beginTransaction()) {
            LocalDateTime now = LocalDateTime.now();
            book.name = request.name;
            book.categoryId = request.categoryId;
            book.description = request.description;
            book.publishedTime = request.publishedTime;
            book.updatedTime = now;
            book.updatedBy = request.updatedBy;
            bookRepository.partialUpdate(book);
            if (request.stock != null) {
                BookStock bookStock = stockRepository.get(id).orElseThrow(() -> new NotFoundException("book stock is not found, bookId=" + id));
                if (request.stock > bookStock.stock) {
                    needPushIncreaseStocksMessage = true;
                }
                if (!bookStock.stock.equals(request.stock)) {
                    bookStock.stock = request.stock;
                    bookStock.updatedTime = now;
                    stockRepository.update(bookStock);
                }
            }
            if (request.authorIds != null && !request.authorIds.isEmpty()) {
                updateBookAuthorRelation(id, request, now);
            }
            if (request.tagIds != null && !request.tagIds.isEmpty()) {
                updateBookTagRelation(id, request, now);
            }
            transaction.commit();
        }
        pushIncreaseStocksMessage(book, needPushIncreaseStocksMessage);
        BOUpdateBookResponse result = new BOUpdateBookResponse();
        result.id = book.id;
        result.updatedTime = book.updatedTime;
        return result;
    }

    private void updateBookAuthorRelation(Long id, BOUpdateBookRequest request, LocalDateTime now) {
        List<BookAuthorRelation> mappingList = bookAuthorRelationsRepository.select("book_id = ?", id);
        Map<Long, Long> authorRelationMap = mappingList.stream().collect(Collectors.toMap(r -> r.authorId, m -> m.id));
        List<Long> existedTagIds = mappingList.stream().map(r -> r.authorId).collect(Collectors.toList());
        List<Long> repeatedTagIds = mappingList.stream().filter(r -> request.authorIds.contains(r.authorId)).map(r -> r.authorId).collect(Collectors.toList());
        List<BookAuthorRelation> newlyIncreasedAuthors = request.authorIds.stream().filter(i -> !repeatedTagIds.contains(i)).map(i -> {
            BookAuthorRelation bookAuthorRelation = new BookAuthorRelation();
            bookAuthorRelation.bookId = id;
            bookAuthorRelation.authorId = i;
            bookAuthorRelation.createdTime = now;
            return bookAuthorRelation;
        }).collect(Collectors.toList());
        List<Long> deletedAuthorRelations = existedTagIds.stream().filter(i -> !repeatedTagIds.contains(i) && authorRelationMap.containsKey(i)).map(i -> authorRelationMap.get(i)).collect(Collectors.toList());
        bookAuthorRelationsRepository.batchDelete(deletedAuthorRelations);
        bookAuthorRelationsRepository.batchInsert(newlyIncreasedAuthors);
    }

    private void updateBookTagRelation(Long id, BOUpdateBookRequest request, LocalDateTime now) {
        List<BookTagRelation> mappingList = tagRelationsRepository.select("book_id = ?", id);
        Map<Long, Long> tagRelationMap = mappingList.stream().collect(Collectors.toMap(m -> m.tagId, m -> m.id));
        List<Long> existedTagIds = mappingList.stream().map(m -> m.tagId).collect(Collectors.toList());
        List<Long> repeatedTagIds = mappingList.stream().filter(m -> request.tagIds.contains(m.tagId)).map(m -> m.tagId).collect(Collectors.toList());
        List<BookTagRelation> newlyIncreasedTags = request.tagIds.stream().filter(i -> !repeatedTagIds.contains(i)).map(i -> {
            BookTagRelation tagRelations = new BookTagRelation();
            tagRelations.bookId = id;
            tagRelations.tagId = i;
            tagRelations.createdTime = now;
            return tagRelations;
        }).collect(Collectors.toList());
        List<Long> deletedTagRelations = existedTagIds.stream().filter(i -> !repeatedTagIds.contains(i) && tagRelationMap.containsKey(i)).map(i -> tagRelationMap.get(i)).collect(Collectors.toList());
        tagRelationsRepository.batchDelete(deletedTagRelations);
        tagRelationsRepository.batchInsert(newlyIncreasedTags);
    }

    private void pushIncreaseStocksMessage(Book book, boolean needPushIncreaseStocksMessage) {
        if (needPushIncreaseStocksMessage) {
            BookIncreaseStocksMessage message = new BookIncreaseStocksMessage();
            message.bookId = book.id;
            messagePublisher.publish(message);
        }
    }
}
