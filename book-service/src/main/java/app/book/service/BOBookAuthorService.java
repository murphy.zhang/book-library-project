package app.book.service;

import app.api.book.author.BOCreateBookAuthorRequest;
import app.api.book.author.BOCreateBookAuthorResponse;
import app.api.book.author.BOGetBookAuthorResponse;
import app.api.book.author.BOSearchBookAuthorRequest;
import app.api.book.author.BOSearchBookAuthorResponse;
import app.book.domain.BookAuthor;
import app.book.util.QueryUtil;
import core.framework.db.Query;
import core.framework.db.Repository;
import core.framework.inject.Inject;
import core.framework.util.Strings;
import core.framework.web.exception.BadRequestException;
import core.framework.web.exception.ConflictException;
import core.framework.web.exception.NotFoundException;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

public class BOBookAuthorService {
    @Inject
    Repository<BookAuthor> authorRepository;

    public BOGetBookAuthorResponse get(Long id) {
        BookAuthor author = authorRepository.get(id).orElseThrow(() -> new NotFoundException("author is not found, id=" + id));
        BOGetBookAuthorResponse result = new BOGetBookAuthorResponse();
        result.id = author.id;
        result.name = author.name;
        result.nationality = author.nationality;
        result.dynasty = author.dynasty;
        result.description = author.description;
        result.createdTime = author.createdTime;
        return result;
    }

    public BOSearchBookAuthorResponse search(BOSearchBookAuthorRequest request) {
        BOSearchBookAuthorResponse result = new BOSearchBookAuthorResponse();
        Query<BookAuthor> query = authorRepository.select();
        query.limit(request.limit);
        query.skip(request.skip);
        query.orderBy("name");
        if (request.ids != null && !request.ids.isEmpty()) {
            QueryUtil.Query in = QueryUtil.in("id", request.ids.toArray());
            query.where(in.condition, in.params);
        }
        if (!Strings.isBlank(request.name)) {
            query.where("INSTR(name, ?)", request.name);
        }
        if (!Strings.isBlank(request.nationality)) {
            query.where("nationality LIKE ?", Strings.format("{}%", request.nationality));
        }
        if (!Strings.isBlank(request.dynasty)) {
            query.where("INSTR(dynasty, ?)", request.dynasty);
        }
        result.total = (long) query.count();
        if (result.total > 0) {
            result.authors = query.fetch().stream().map(this::authorView).collect(Collectors.toList());
        }
        return result;
    }

    private BOSearchBookAuthorResponse.Author authorView(BookAuthor author) {
        BOSearchBookAuthorResponse.Author result = new BOSearchBookAuthorResponse.Author();
        result.id = author.id;
        result.name = author.name;
        result.nationality = author.nationality;
        result.dynasty = author.dynasty;
        result.description = author.description;
        result.createdTime = author.createdTime;
        return result;
    }

    public BOCreateBookAuthorResponse create(BOCreateBookAuthorRequest request) {
        BookAuthor author = new BookAuthor();
        authorRepository.selectOne("name = ? AND nationality = ? AND dynasty = ?", request.name, request.nationality, request.dynasty).ifPresent(a -> {
            throw new ConflictException("author already exists, name=" + request.name);
        });
        author.name = request.name;
        author.dynasty = request.dynasty;
        author.nationality = request.nationality;
        author.description = request.description;
        author.createdTime = LocalDateTime.now();
        author.updatedTime = author.createdTime;
        author.createdBy = request.createdBy;
        author.updatedBy = author.createdBy;
        author.id = authorRepository.insert(author).orElseThrow(() -> new BadRequestException("create author error."));
        BOCreateBookAuthorResponse result = new BOCreateBookAuthorResponse();
        result.id = author.id;
        result.name = author.name;
        return result;
    }
}
