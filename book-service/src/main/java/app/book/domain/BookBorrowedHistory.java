package app.book.domain;

import core.framework.api.validate.NotNull;
import core.framework.mongo.Collection;
import core.framework.mongo.Field;
import core.framework.mongo.Id;
import core.framework.mongo.MongoEnumValue;

import java.time.LocalDateTime;

/**
 * @author murphy
 */
@Collection(name = "book_borrowed_histories")
public class BookBorrowedHistory {
    @Id
    @Field(name = "id")
    public String id;

    @NotNull
    @Field(name = "user_id")
    public Long userId;

    @NotNull
    @Field(name = "book_id")
    public Long bookId;

    @NotNull
    @Field(name = "status")
    public BookBorrowedStatus status;

    @NotNull
    @Field(name = "expect_returned_time")
    public LocalDateTime expectReturnedTime;

    @Field(name = "actual_returned_time")
    public LocalDateTime actualReturnedTime;

    @NotNull
    @Field(name = "created_time")
    public LocalDateTime createdTime;

    @NotNull
    @Field(name = "updated_time")
    public LocalDateTime updatedTime;

    public enum BookBorrowedStatus {
        @MongoEnumValue("BORROWED")
        BORROWED,
        @MongoEnumValue("RETURNED")
        RETURNED
    }
}
