package app.book.domain;

import core.framework.api.validate.NotNull;
import core.framework.db.Column;
import core.framework.db.PrimaryKey;
import core.framework.db.Table;

import java.time.LocalDateTime;

/**
 * @author murphy.zhang
 */
@Table(name = "book_author_relations")
public class BookAuthorRelation {
    @PrimaryKey(autoIncrement = true)
    @Column(name = "id")
    public Long id;

    @NotNull
    @Column(name = "book_id")
    public Long bookId;

    @NotNull
    @Column(name = "author_id")
    public Long authorId;

    @NotNull
    @Column(name = "created_time")
    public LocalDateTime createdTime;
}
