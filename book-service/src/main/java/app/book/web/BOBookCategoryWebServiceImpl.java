package app.book.web;

import app.api.BOBookCategoryWebService;
import app.api.book.category.BOCreateBookCategoryRequest;
import app.api.book.category.BOCreateBookCategoryResponse;
import app.api.book.category.BOGetBookCategoryResponse;
import app.book.service.BOBookCategoryService;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

/**
 * @author murphy.zhang
 */
public class BOBookCategoryWebServiceImpl implements BOBookCategoryWebService {
    @Inject
    BOBookCategoryService boBookCategoryService;

    @Override
    public BOGetBookCategoryResponse get(Long parentId) {
        return boBookCategoryService.get(parentId);
    }

    @Override
    public BOCreateBookCategoryResponse create(BOCreateBookCategoryRequest request) {
        ActionLogContext.put("categoryName", request.name);
        return boBookCategoryService.create(request);
    }
}
