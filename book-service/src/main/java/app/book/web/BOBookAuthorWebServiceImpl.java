package app.book.web;

import app.api.BOBookAuthorWebService;
import app.api.book.author.BOCreateBookAuthorRequest;
import app.api.book.author.BOCreateBookAuthorResponse;
import app.api.book.author.BOGetBookAuthorResponse;
import app.api.book.author.BOSearchBookAuthorRequest;
import app.api.book.author.BOSearchBookAuthorResponse;
import app.book.service.BOBookAuthorService;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

public class BOBookAuthorWebServiceImpl implements BOBookAuthorWebService {
    @Inject
    BOBookAuthorService boBookAuthorService;

    @Override
    public BOGetBookAuthorResponse get(Long id) {
        ActionLogContext.put("authorId", id);
        return boBookAuthorService.get(id);
    }

    @Override
    public BOSearchBookAuthorResponse search(BOSearchBookAuthorRequest request) {
        return boBookAuthorService.search(request);
    }

    @Override
    public BOCreateBookAuthorResponse create(BOCreateBookAuthorRequest request) {
        ActionLogContext.put("authorName", request.name);
        return boBookAuthorService.create(request);
    }
}
