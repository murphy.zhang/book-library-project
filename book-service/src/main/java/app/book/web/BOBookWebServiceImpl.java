package app.book.web;

import app.api.BOBookWebService;
import app.api.book.BOCreateBookRequest;
import app.api.book.BOCreateBookResponse;
import app.api.book.BOGetBookResponse;
import app.api.book.stock.BOGetBookStockResponse;
import app.api.book.borrowed.BOSearchBookBorrowedRequest;
import app.api.book.borrowed.BOSearchBookBorrowedResponse;
import app.api.book.BOSearchBookRequest;
import app.api.book.BOSearchBookResponse;
import app.api.book.BOUpdateBookRequest;
import app.api.book.BOUpdateBookResponse;
import app.book.service.BOBookBorrowedService;
import app.book.service.BOBookService;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

/**
 * @author murphy.zhang
 */
public class BOBookWebServiceImpl implements BOBookWebService {
    @Inject
    BOBookService bookService;
    @Inject
    BOBookBorrowedService borrowedService;

    @Override
    public BOCreateBookResponse create(BOCreateBookRequest request) {
        ActionLogContext.put("bookName", request.name);
        return bookService.create(request);
    }

    @Override
    public BOUpdateBookResponse update(Long id, BOUpdateBookRequest request) {
        ActionLogContext.put("bookId", id);
        return bookService.update(id, request);
    }

    @Override
    public BOGetBookResponse get(Long id) {
        ActionLogContext.put("bookId", id);
        return bookService.get(id);
    }

    @Override
    public BOGetBookStockResponse getStock(Long id) {
        ActionLogContext.put("bookId", id);
        return bookService.getStock(id);
    }

    @Override
    public BOSearchBookResponse search(BOSearchBookRequest request) {
        return bookService.search(request);
    }

    @Override
    public BOSearchBookBorrowedResponse searchBookBorrowed(BOSearchBookBorrowedRequest request) {
        ActionLogContext.put("bookId", request.bookId);
        return borrowedService.search(request);
    }
}
