package app.book.web;

import app.api.BookWebService;
import app.api.book.borrowed.CreateBookBorrowResponse;
import app.api.book.borrowed.CreateBookBorrowedRequest;
import app.api.book.ListBookRequest;
import app.api.book.ListBookResponse;
import app.api.book.GetBookResponse;
import app.api.book.stock.GetBookStockResponse;
import app.api.book.borrowed.SearchBookBorrowedRequest;
import app.api.book.borrowed.SearchBookBorrowedResponse;
import app.api.book.category.GetBookCategoryResponse;
import app.api.book.SearchBookRequest;
import app.api.book.SearchBookResponse;
import app.api.book.tag.SearchBookTagResponse;
import app.api.book.borrowed.UpdateBookBorrowResponse;
import app.api.book.borrowed.UpdateBookBorrowedRequest;
import app.book.service.BookBorrowedService;
import app.book.service.BookService;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

/**
 * @author murphy.zhang
 */
public class BookWebServiceImpl implements BookWebService {

    @Inject
    BookBorrowedService bookBorrowedService;
    @Inject
    BookService bookService;

    @Override
    public GetBookResponse get(Long id) {
        ActionLogContext.put("bookId", id);
        return bookService.get(id);
    }

    @Override
    public GetBookStockResponse getStock(Long id) {
        ActionLogContext.put("bookId", id);
        return bookService.getStock(id);
    }

    @Override
    public SearchBookResponse search(SearchBookRequest request) {
        return bookService.search(request);
    }

    @Override
    public ListBookResponse list(ListBookRequest request) {
        return bookService.list(request);
    }

    @Override
    public GetBookCategoryResponse getCategory(Long parentId) {
        return bookService.getCategory(parentId);
    }

    @Override
    public SearchBookTagResponse searchTag() {
        return bookService.searchTag();
    }

    @Override
    public SearchBookBorrowedResponse searchBookBorrowed(SearchBookBorrowedRequest request) {
        ActionLogContext.put("userId", request.userId);
        return bookBorrowedService.search(request);
    }

    @Override
    public CreateBookBorrowResponse borrowed(Long bookId, CreateBookBorrowedRequest request) {
        ActionLogContext.put("bookId", bookId);
        ActionLogContext.put("userId", request.userId);
        return bookBorrowedService.borrowed(bookId, request);
    }

    @Override
    public UpdateBookBorrowResponse returned(String id, UpdateBookBorrowedRequest request) {
        ActionLogContext.put("bookBorrowedHistoryId", id);
        ActionLogContext.put("userId", request.userId);
        return bookBorrowedService.returned(id, request);
    }
}
