package app.book.web;

import app.api.BOBookTagWebService;
import app.api.book.tag.BOCreateBookTagRequest;
import app.api.book.tag.BOCreateBookTagResponse;
import app.api.book.tag.BOSearchBookTagResponse;
import app.book.service.BOBookTagService;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

/**
 * @author murphy.zhang
 */
public class BOBookTagWebServiceImpl implements BOBookTagWebService {
    @Inject
    BOBookTagService boBookTagService;

    @Override
    public BOSearchBookTagResponse search() {
        return boBookTagService.search();
    }

    @Override
    public BOCreateBookTagResponse create(BOCreateBookTagRequest request) {
        ActionLogContext.put("tagName", request.name);
        return boBookTagService.create(request);
    }
}
