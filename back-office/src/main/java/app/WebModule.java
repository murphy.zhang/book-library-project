package app;

import app.api.BOBookAuthorWebService;
import app.api.BOBookCategoryWebService;
import app.api.BOBookTagWebService;
import app.api.BOBookWebService;
import app.api.BOUserWebService;
import app.api.BookAJAXWebService;
import app.api.BookAuthorAJAXWebService;
import app.api.BookCategoryAJAXWebService;
import app.api.BookTagAJAXWebService;
import app.api.UserAJAXWebService;
import app.web.BookAJAXWebServiceImpl;
import app.web.BookAuthorAJAXWebServiceImpl;
import app.web.BookCategoryAJAXWebServiceImpl;
import app.web.BookTagAJAXWebServiceImpl;
import app.web.LoginController;
import app.web.UserAJAXWebServiceImpl;
import app.web.book.BookService;
import app.web.book.BookAuthorService;
import app.web.book.BookCategoryService;
import app.web.book.BookTagService;
import app.web.interceptor.LoginRequiredInterceptor;
import app.web.security.SecureUser;
import app.web.security.SecureUserHolder;
import app.web.user.UserService;
import core.framework.http.HTTPMethod;
import core.framework.module.Module;

import java.time.Duration;

/**
 * @author murphy
 */
public class WebModule extends Module {
    @Override
    protected void initialize() {
        cache().redis(requiredProperty("sys.redis.host"));
        cache().add(SecureUser.class, Duration.ofMinutes(30));

        bind(SecureUserHolder.class);

        http().intercept(bind(LoginRequiredInterceptor.class));

        api().client(BOUserWebService.class, requiredProperty("sys.user.uri"));
        api().client(BOBookWebService.class, requiredProperty("sys.book.uri"));
        api().client(BOBookCategoryWebService.class, requiredProperty("sys.book.uri"));
        api().client(BOBookAuthorWebService.class, requiredProperty("sys.book.uri"));
        api().client(BOBookTagWebService.class, requiredProperty("sys.book.uri"));

        bind(UserService.class);
        bind(BookTagService.class);
        bind(BookCategoryService.class);
        bind(BookAuthorService.class);
        bind(BookService.class);

        api().service(BookTagAJAXWebService.class, bind(BookTagAJAXWebServiceImpl.class));
        api().service(BookCategoryAJAXWebService.class, bind(BookCategoryAJAXWebServiceImpl.class));
        api().service(BookAuthorAJAXWebService.class, bind(BookAuthorAJAXWebServiceImpl.class));
        api().service(BookAJAXWebService.class, bind(BookAJAXWebServiceImpl.class));
        api().service(UserAJAXWebService.class, bind(UserAJAXWebServiceImpl.class));

        LoginController loginController = bind(LoginController.class);
        http().route(HTTPMethod.POST, "/ajax/login", loginController::login);
        http().route(HTTPMethod.POST, "/ajax/logout", loginController::logout);
    }
}
