package app.web;

import app.api.BookCategoryAJAXWebService;
import app.api.book.CreateBookCategoryAJAXRequest;
import app.api.book.CreateBookCategoryAJAXResponse;
import app.api.book.GetBookCategoryAJAXResponse;
import app.web.book.BookCategoryService;
import app.web.interceptor.LoginRequired;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

/**
 * @author murphy.zhang
 */
@LoginRequired
public class BookCategoryAJAXWebServiceImpl implements BookCategoryAJAXWebService {
    @Inject
    BookCategoryService bookCategoryService;

    @Override
    public GetBookCategoryAJAXResponse get(Long parentId) {
        return bookCategoryService.get(parentId);
    }

    @Override
    public CreateBookCategoryAJAXResponse create(CreateBookCategoryAJAXRequest request) {
        ActionLogContext.put("categoryName", request.name);
        return bookCategoryService.create(request);
    }
}
