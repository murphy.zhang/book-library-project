package app.web;

import app.api.BookAJAXWebService;
import app.api.book.CreateBookAJAXRequest;
import app.api.book.CreateBookAJAXResponse;
import app.api.book.GetBookAJAXResponse;
import app.api.book.SearchBookAJAXRequest;
import app.api.book.SearchBookAJAXResponse;
import app.api.book.SearchBookBorrowedAJAXRequest;
import app.api.book.SearchBookBorrowedAJAXResponse;
import app.api.book.UpdateBookAJAXRequest;
import app.api.book.UpdateBookAJAXResponse;
import app.web.book.BookService;
import app.web.interceptor.LoginRequired;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

/**
 * @author murphy.zhang
 */
@LoginRequired
public class BookAJAXWebServiceImpl implements BookAJAXWebService {
    @Inject
    BookService bookService;

    @Override
    public CreateBookAJAXResponse create(CreateBookAJAXRequest request) {
        ActionLogContext.put("bookName", request.name);
        return bookService.create(request);
    }

    @Override
    public UpdateBookAJAXResponse update(Long id, UpdateBookAJAXRequest request) {
        ActionLogContext.put("bookId", id);
        ActionLogContext.put("bookName", request.name);
        return bookService.update(id, request);
    }

    @Override
    public GetBookAJAXResponse get(Long id) {
        ActionLogContext.put("bookId", id);
        return bookService.get(id);
    }

    @Override
    public SearchBookAJAXResponse search(SearchBookAJAXRequest request) {
        return bookService.search(request);
    }

    @Override
    public SearchBookBorrowedAJAXResponse searchBookBorrowed(SearchBookBorrowedAJAXRequest request) {
        ActionLogContext.put("bookId", request.bookId);
        return bookService.searchBookBorrowed(request);
    }
}
