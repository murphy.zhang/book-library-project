package app.web.book;

import app.api.BOBookAuthorWebService;
import app.api.book.author.BOCreateBookAuthorRequest;
import app.api.book.author.BOCreateBookAuthorResponse;
import app.api.book.author.BOSearchBookAuthorRequest;
import app.api.book.author.BOSearchBookAuthorResponse;
import app.api.book.CreateBookAuthorAJAXRequest;
import app.api.book.CreateBookAuthorAJAXResponse;
import app.api.book.SearchBookAuthorAJAXRequest;
import app.api.book.SearchBookAuthorAJAXResponse;
import app.web.security.SecureUserHolder;
import core.framework.inject.Inject;

import java.util.stream.Collectors;

/**
 * @author murphy.zhang
 */
public class BookAuthorService {
    @Inject
    BOBookAuthorWebService bookAuthorWebService;
    @Inject
    SecureUserHolder userHolder;

    public SearchBookAuthorAJAXResponse search(SearchBookAuthorAJAXRequest request) {
        BOSearchBookAuthorRequest searchBookAuthorRequest = new BOSearchBookAuthorRequest();
        searchBookAuthorRequest.limit = request.limit;
        searchBookAuthorRequest.skip = request.skip;
        searchBookAuthorRequest.name = request.name;
        searchBookAuthorRequest.dynasty = request.dynasty;
        searchBookAuthorRequest.nationality = request.nationality;
        BOSearchBookAuthorResponse searchBookAuthorResponse = bookAuthorWebService.search(searchBookAuthorRequest);
        SearchBookAuthorAJAXResponse result = new SearchBookAuthorAJAXResponse();
        result.total = searchBookAuthorResponse.total;
        if (result.total > 0) {
            result.authors = searchBookAuthorResponse.authors.stream().map(this::authorView).collect(Collectors.toList());
        }
        return result;
    }

    private SearchBookAuthorAJAXResponse.Author authorView(BOSearchBookAuthorResponse.Author author) {
        SearchBookAuthorAJAXResponse.Author result = new SearchBookAuthorAJAXResponse.Author();
        result.id = author.id;
        result.name = author.name;
        result.dynasty = author.dynasty;
        result.nationality = author.nationality;
        result.description = author.description;
        result.createdTime = author.createdTime;
        return result;
    }

    public CreateBookAuthorAJAXResponse create(CreateBookAuthorAJAXRequest request) {
        BOCreateBookAuthorRequest createBookAuthorRequest = new BOCreateBookAuthorRequest();
        createBookAuthorRequest.name = request.name;
        createBookAuthorRequest.dynasty = request.dynasty;
        createBookAuthorRequest.nationality = request.nationality;
        createBookAuthorRequest.description = request.description;
        createBookAuthorRequest.createdBy = userHolder.getUserName();
        BOCreateBookAuthorResponse createBookAuthorResponse = bookAuthorWebService.create(createBookAuthorRequest);
        CreateBookAuthorAJAXResponse result = new CreateBookAuthorAJAXResponse();
        result.id = createBookAuthorResponse.id;
        result.name = createBookAuthorResponse.name;
        return result;
    }
}
