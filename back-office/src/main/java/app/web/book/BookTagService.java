package app.web.book;

import app.api.BOBookTagWebService;
import app.api.book.tag.BOCreateBookTagRequest;
import app.api.book.tag.BOCreateBookTagResponse;
import app.api.book.tag.BOSearchBookTagResponse;
import app.api.book.CreateBookTagAJAXRequest;
import app.api.book.CreateBookTagAJAXResponse;
import app.api.book.SearchBookTagAJAXResponse;
import app.web.security.SecureUserHolder;
import core.framework.inject.Inject;

import java.util.stream.Collectors;

/**
 * @author murphy.zhang
 */
public class BookTagService {
    @Inject
    BOBookTagWebService bookTagWebService;
    @Inject
    SecureUserHolder userHolder;

    public SearchBookTagAJAXResponse search() {
        BOSearchBookTagResponse searchBookTagResponse = bookTagWebService.search();
        SearchBookTagAJAXResponse result = new SearchBookTagAJAXResponse();
        result.tags = searchBookTagResponse.tags.stream().map(this::tagView).collect(Collectors.toList());
        return result;
    }

    private SearchBookTagAJAXResponse.Tag tagView(BOSearchBookTagResponse.Tag tag) {
        SearchBookTagAJAXResponse.Tag result = new SearchBookTagAJAXResponse.Tag();
        result.id = tag.id;
        result.name = tag.name;
        return result;
    }

    public CreateBookTagAJAXResponse create(CreateBookTagAJAXRequest request) {
        BOCreateBookTagRequest createBookTagRequest = new BOCreateBookTagRequest();
        createBookTagRequest.name = request.name;
        createBookTagRequest.createdBy = userHolder.getUserName();
        BOCreateBookTagResponse createBookTagResponse = bookTagWebService.create(createBookTagRequest);
        CreateBookTagAJAXResponse result = new CreateBookTagAJAXResponse();
        result.id = createBookTagResponse.id;
        result.name = createBookTagResponse.name;
        return result;
    }
}
