package app.web.book;

import app.api.BOBookCategoryWebService;
import app.api.book.category.BOCreateBookCategoryRequest;
import app.api.book.category.BOCreateBookCategoryResponse;
import app.api.book.category.BOGetBookCategoryResponse;
import app.api.book.CreateBookCategoryAJAXRequest;
import app.api.book.CreateBookCategoryAJAXResponse;
import app.api.book.GetBookCategoryAJAXResponse;
import app.web.security.SecureUserHolder;
import core.framework.inject.Inject;

import java.util.stream.Collectors;

/**
 * @author murphy.zhang
 */
public class BookCategoryService {
    @Inject
    BOBookCategoryWebService bookCategoryWebService;
    @Inject
    SecureUserHolder userHolder;

    public GetBookCategoryAJAXResponse get(Long parentId) {
        BOGetBookCategoryResponse getBookCategoryResponse = bookCategoryWebService.get(parentId);
        GetBookCategoryAJAXResponse result = new GetBookCategoryAJAXResponse();
        result.categories = getBookCategoryResponse.categories.stream().map(this::categoryView).collect(Collectors.toList());
        return result;
    }

    private GetBookCategoryAJAXResponse.Category categoryView(BOGetBookCategoryResponse.Category category) {
        GetBookCategoryAJAXResponse.Category result = new GetBookCategoryAJAXResponse.Category();
        result.id = category.id;
        result.name = category.name;
        result.parentId = category.parentId;
        result.fullPath = category.fullPath;
        return result;
    }

    public CreateBookCategoryAJAXResponse create(CreateBookCategoryAJAXRequest request) {
        BOCreateBookCategoryRequest createBookCategoryRequest = new BOCreateBookCategoryRequest();
        createBookCategoryRequest.name = request.name;
        createBookCategoryRequest.parentId = request.parentId;
        createBookCategoryRequest.createdBy = userHolder.getUserName();
        BOCreateBookCategoryResponse createBookCategoryResponse = bookCategoryWebService.create(createBookCategoryRequest);
        CreateBookCategoryAJAXResponse result = new CreateBookCategoryAJAXResponse();
        result.id = createBookCategoryResponse.id;
        result.name = createBookCategoryResponse.name;
        return result;
    }
}
