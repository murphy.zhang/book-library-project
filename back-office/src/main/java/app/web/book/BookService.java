package app.web.book;

import app.api.BOBookWebService;
import app.api.BOUserWebService;
import app.api.book.BOCreateBookRequest;
import app.api.book.BOCreateBookResponse;
import app.api.book.BOGetBookResponse;
import app.api.book.BOSearchBookRequest;
import app.api.book.BOSearchBookResponse;
import app.api.book.BOUpdateBookRequest;
import app.api.book.BOUpdateBookResponse;
import app.api.book.BookBorrowedAJAXStatus;
import app.api.book.CreateBookAJAXRequest;
import app.api.book.CreateBookAJAXResponse;
import app.api.book.GetBookAJAXResponse;
import app.api.book.SearchBookAJAXRequest;
import app.api.book.SearchBookAJAXResponse;
import app.api.book.SearchBookBorrowedAJAXRequest;
import app.api.book.SearchBookBorrowedAJAXResponse;
import app.api.book.UpdateBookAJAXRequest;
import app.api.book.UpdateBookAJAXResponse;
import app.api.book.borrowed.BOSearchBookBorrowedRequest;
import app.api.book.borrowed.BOSearchBookBorrowedResponse;
import app.api.book.borrowed.BookBorrowedStatus;
import app.api.book.stock.BOGetBookStockResponse;
import app.api.user.BOListUserRequest;
import app.web.security.SecureUserHolder;
import core.framework.inject.Inject;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author murphy.zhang
 */
public class BookService {
    @Inject
    BOBookWebService bookWebService;
    @Inject
    BOUserWebService userWebService;
    @Inject
    SecureUserHolder userHolder;

    public CreateBookAJAXResponse create(CreateBookAJAXRequest request) {
        BOCreateBookRequest createBookRequest = new BOCreateBookRequest();
        createBookRequest.name = request.name;
        createBookRequest.authorIds = request.authorIds;
        createBookRequest.categoryId = request.categoryId;
        createBookRequest.description = request.description;
        createBookRequest.tagIds = request.tagIds;
        createBookRequest.stock = request.stock;
        createBookRequest.publishedTime = request.publishedTime;
        createBookRequest.createdBy = userHolder.getUserName();
        BOCreateBookResponse createBookResponse = bookWebService.create(createBookRequest);
        CreateBookAJAXResponse result = new CreateBookAJAXResponse();
        result.id = createBookResponse.id;
        result.name = createBookResponse.name;
        result.createdTime = createBookResponse.createdTime;
        return result;
    }

    public UpdateBookAJAXResponse update(Long id, UpdateBookAJAXRequest request) {
        BOUpdateBookRequest updateBookRequest = new BOUpdateBookRequest();
        updateBookRequest.name = request.name;
        updateBookRequest.authorIds = request.authorIds;
        updateBookRequest.categoryId = request.categoryId;
        updateBookRequest.publishedTime = request.publishedTime;
        updateBookRequest.description = request.description;
        updateBookRequest.tagIds = request.tagIds;
        updateBookRequest.stock = request.stock;
        updateBookRequest.updatedBy = userHolder.getUserName();
        BOUpdateBookResponse updateBookResponse = bookWebService.update(id, updateBookRequest);
        UpdateBookAJAXResponse result = new UpdateBookAJAXResponse();
        result.id = updateBookResponse.id;
        result.updatedTime = updateBookResponse.updatedTime;
        return result;
    }

    public GetBookAJAXResponse get(Long id) {
        BOGetBookResponse getBookResponse = bookWebService.get(id);
        BOGetBookStockResponse getBookStockResponse = bookWebService.getStock(id);
        GetBookAJAXResponse result = new GetBookAJAXResponse();
        result.id = getBookResponse.id;
        result.name = getBookResponse.name;
        result.description = getBookResponse.description;
        result.publishedTime = getBookResponse.publishedTime;
        result.createdTime = getBookResponse.createdTime;
        result.stock = getBookStockResponse.stock;
        result.authors = getBookResponse.authors.stream().map(this::bookGetAuthorView).collect(Collectors.toList());
        result.category = bookGetCategoryView(getBookResponse.category);
        result.tags = getBookResponse.tags.stream().map(this::tagView).collect(Collectors.toList());
        return result;
    }

    private GetBookAJAXResponse.Category bookGetCategoryView(BOGetBookResponse.Category category) {
        GetBookAJAXResponse.Category result = new GetBookAJAXResponse.Category();
        result.id = category.id;
        result.name = category.name;
        return result;
    }

    private GetBookAJAXResponse.Author bookGetAuthorView(BOGetBookResponse.Author author) {
        GetBookAJAXResponse.Author result = new GetBookAJAXResponse.Author();
        result.id = author.id;
        result.name = author.name;
        result.dynasty = author.dynasty;
        result.nationality = author.nationality;
        return result;
    }

    private GetBookAJAXResponse.Tag tagView(BOGetBookResponse.Tag tag) {
        GetBookAJAXResponse.Tag result = new GetBookAJAXResponse.Tag();
        result.id = tag.id;
        result.name = tag.name;
        return result;
    }

    public SearchBookAJAXResponse search(SearchBookAJAXRequest request) {
        BOSearchBookRequest searchBookRequest = new BOSearchBookRequest();
        searchBookRequest.limit = request.limit;
        searchBookRequest.skip = request.skip;
        searchBookRequest.name = request.name;
        searchBookRequest.authorName = request.authorName;
        searchBookRequest.categoryId = request.categoryId;
        searchBookRequest.tagIds = request.tagIds;
        searchBookRequest.description = request.description;
        BOSearchBookResponse searchBookResponse = bookWebService.search(searchBookRequest);
        SearchBookAJAXResponse result = new SearchBookAJAXResponse();
        result.total = searchBookResponse.total;
        if (result.total > 0) {
            result.books = searchBookResponse.books.stream().map(this::bookView).collect(Collectors.toList());
        }
        return result;
    }

    private SearchBookAJAXResponse.Book bookView(BOSearchBookResponse.Book book) {
        SearchBookAJAXResponse.Book result = new SearchBookAJAXResponse.Book();
        result.id = book.id;
        result.name = book.name;
        result.description = book.description;
        result.publishedTime = book.publishedTime;
        result.createdTime = book.createdTime;
        result.stock = book.stock;
        result.authors = book.authors.stream().map(this::bookSearchAuthorView).collect(Collectors.toList());
        result.category = bookSearchCategoryView(book.category);
        result.tags = book.tags.stream().map(this::tagSearchView).collect(Collectors.toList());
        return result;
    }

    private SearchBookAJAXResponse.Category bookSearchCategoryView(BOSearchBookResponse.Category category) {
        SearchBookAJAXResponse.Category result = new SearchBookAJAXResponse.Category();
        result.id = category.id;
        result.name = category.name;
        return result;
    }

    private SearchBookAJAXResponse.Author bookSearchAuthorView(BOSearchBookResponse.Author author) {
        SearchBookAJAXResponse.Author result = new SearchBookAJAXResponse.Author();
        result.id = author.id;
        result.name = author.name;
        result.dynasty = author.dynasty;
        result.nationality = author.nationality;
        return result;
    }

    private SearchBookAJAXResponse.Tag tagSearchView(BOSearchBookResponse.Tag tag) {
        SearchBookAJAXResponse.Tag result = new SearchBookAJAXResponse.Tag();
        result.id = tag.id;
        result.name = tag.name;
        return result;
    }

    public SearchBookBorrowedAJAXResponse searchBookBorrowed(SearchBookBorrowedAJAXRequest request) {
        BOSearchBookBorrowedRequest searchBookBorrowedRequest = new BOSearchBookBorrowedRequest();
        searchBookBorrowedRequest.limit = request.limit;
        searchBookBorrowedRequest.skip = request.skip;
        searchBookBorrowedRequest.bookId = request.bookId;
        if (request.status != null) {
            searchBookBorrowedRequest.status = convert(request.status);
        }
        BOSearchBookBorrowedResponse searchBookBorrowedResponse = bookWebService.searchBookBorrowed(searchBookBorrowedRequest);
        SearchBookBorrowedAJAXResponse result = new SearchBookBorrowedAJAXResponse();
        result.total = searchBookBorrowedResponse.total;
        if (result.total > 0) {
            List<Long> userIdList = searchBookBorrowedResponse.bookBorrowedHistories.stream().map(h -> h.userId).collect(Collectors.toList());
            BOListUserRequest listUserRequest = new BOListUserRequest();
            listUserRequest.ids = userIdList;
            Map<Long, String> userNameMap = userWebService.list(listUserRequest).users.stream().collect(Collectors.toMap(u -> u.id, u -> u.username));
            result.bookBorrowedHistories = searchBookBorrowedResponse.bookBorrowedHistories.stream().map(h -> borrowedHistoryView(h, userNameMap)).collect(Collectors.toList());
        }
        return result;
    }

    private SearchBookBorrowedAJAXResponse.BookBorrowedHistory borrowedHistoryView(BOSearchBookBorrowedResponse.BookBorrowedHistory history, Map<Long, String> userNameMap) {
        SearchBookBorrowedAJAXResponse.BookBorrowedHistory result = new SearchBookBorrowedAJAXResponse.BookBorrowedHistory();
        result.id = history.id;
        result.userId = history.userId;
        result.userFullName = userNameMap.get(history.userId);
        result.expectReturnedTime = history.expectReturnedTime;
        result.actualReturnedTime = history.actualReturnedTime;
        result.createdTime = history.createdTime;
        result.status = convert(history.status);
        result.expired = result.status == BookBorrowedAJAXStatus.BORROWED && LocalDate.now().isAfter(result.expectReturnedTime.toLocalDate());
        return result;
    }

    private BookBorrowedStatus convert(BookBorrowedAJAXStatus status) {
        return BookBorrowedStatus.valueOf(status.name());
    }

    private BookBorrowedAJAXStatus convert(BookBorrowedStatus status) {
        return BookBorrowedAJAXStatus.valueOf(status.name());
    }
}
