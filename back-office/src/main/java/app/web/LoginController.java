package app.web;

import app.web.security.SecureUser;
import app.web.security.SecureUserHolder;
import core.framework.cache.Cache;
import core.framework.inject.Inject;
import core.framework.web.Request;
import core.framework.web.Response;
import core.framework.web.exception.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.UUID;

/**
 * @author murphy.zhang
 */
public class LoginController {
    private final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Inject
    Cache<SecureUser> cache;

    public Response login(Request request) {
        Map<String, String> params = request.formParams();
        String username = params.get("username");
        String password = params.get("password");
        if (!"admin".equals(username) || !"admin".equals(password)) {
            logger.warn("login failed, username={}, password={}", username, password);
            throw new NotFoundException("user is not found, username=" + username);
        }
        String token = UUID.randomUUID().toString();
        request.session().set(SecureUserHolder.KEY_TOKEN, token);
        SecureUser secureUser = new SecureUser();
        secureUser.username = username;
        cache.put(token, secureUser);
        logger.info("login success, username={}, password={}, token={}", username, password, token);
        return Response.text("login");
    }

    public Response logout(Request request) {
        request.session().get(SecureUserHolder.KEY_TOKEN).ifPresent(token -> {
            cache.evict(token);
        });
        request.session().invalidate();
        return Response.text("logout");
    }
}
