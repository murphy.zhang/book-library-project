package app.web.security;

import core.framework.crypto.Hash;

import java.security.SecureRandom;

/**
 * @author murphy.zhang
 */
public class Passwords {
    private static final String[] ELEMENT = {
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
        "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
    };

    private static final SecureRandom RANDOM = new SecureRandom();

    public static String randomPassword() {
        int len = (int) (Math.random() * 15 + 6);
        StringBuilder buffer = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            int next = RANDOM.nextInt(ELEMENT.length);
            buffer.append(ELEMENT[next]);
        }
        return buffer.toString();
    }

    public static String hash(String password) {
        return hash(password, "murphy-library", 1);
    }

    private static String hash(String password, String salt, int iteration) {
        String hashedPassword = password;
        for (int i = 0; i < iteration; i++) {
            hashedPassword = Hash.sha256Hex(salt + hashedPassword);
        }
        return hashedPassword;
    }
}
