package app.web;

import app.api.UserAJAXWebService;
import app.api.user.CreateUserAJAXRequest;
import app.api.user.CreateUserAJAXResponse;
import app.api.user.GetUserAJAXResponse;
import app.api.user.SearchUserAJAXRequest;
import app.api.user.SearchUserAJAXResponse;
import app.api.user.UpdateUserAJAXRequest;
import app.api.user.UpdateUserAJAXResponse;
import app.api.user.UserAJAXStatus;
import app.web.interceptor.LoginRequired;
import app.web.user.UserService;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

/**
 * @author murphy.zhang
 */
@LoginRequired
public class UserAJAXWebServiceImpl implements UserAJAXWebService {
    @Inject
    UserService userService;

    @Override
    public CreateUserAJAXResponse create(CreateUserAJAXRequest request) {
        ActionLogContext.put("username", request.username);
        return userService.create(request);
    }

    @Override
    public UpdateUserAJAXResponse update(Long id, UpdateUserAJAXRequest request) {
        ActionLogContext.put("userId", id);
        return userService.update(id, request);
    }

    @Override
    public GetUserAJAXResponse get(Long id) {
        ActionLogContext.put("userId", id);
        return userService.get(id);
    }

    @Override
    public SearchUserAJAXResponse search(SearchUserAJAXRequest request) {
        return userService.search(request);
    }

    @Override
    public void resetPassword(Long id) {
        ActionLogContext.put("userId", id);
        userService.resetPassword(id);
    }

    @Override
    public void setActive(Long id) {
        ActionLogContext.put("userId", id);
        userService.updateStatus(id, UserAJAXStatus.ACTIVE);
    }

    @Override
    public void setInactive(Long id) {
        ActionLogContext.put("userId", id);
        userService.updateStatus(id, UserAJAXStatus.INACTIVE);
    }
}
