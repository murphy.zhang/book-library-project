package app.web;

import app.api.BookTagAJAXWebService;
import app.api.book.CreateBookTagAJAXRequest;
import app.api.book.CreateBookTagAJAXResponse;
import app.api.book.SearchBookTagAJAXResponse;
import app.web.book.BookTagService;
import app.web.interceptor.LoginRequired;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

/**
 * @author murphy.zhang
 */
@LoginRequired
public class BookTagAJAXWebServiceImpl implements BookTagAJAXWebService {
    @Inject
    BookTagService bookTagService;

    @Override
    public SearchBookTagAJAXResponse search() {
        return bookTagService.search();
    }

    @Override
    public CreateBookTagAJAXResponse create(CreateBookTagAJAXRequest request) {
        ActionLogContext.put("tagName", request.name);
        return bookTagService.create(request);
    }
}
