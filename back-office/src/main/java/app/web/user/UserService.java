package app.web.user;

import app.api.BOUserWebService;
import app.api.user.BOCreateUserRequest;
import app.api.user.BOCreateUserResponse;
import app.api.user.BOGetUserResponse;
import app.api.user.BOSearchUserRequest;
import app.api.user.BOSearchUserResponse;
import app.api.user.BOUpdateUserPasswordRequest;
import app.api.user.BOUpdateUserRequest;
import app.api.user.BOUpdateUserResponse;
import app.api.user.BOUpdateUserStatusRequest;
import app.api.user.CreateUserAJAXRequest;
import app.api.user.CreateUserAJAXResponse;
import app.api.user.GetUserAJAXResponse;
import app.api.user.SearchUserAJAXRequest;
import app.api.user.SearchUserAJAXResponse;
import app.api.user.UpdateUserAJAXRequest;
import app.api.user.UpdateUserAJAXResponse;
import app.api.user.UserAJAXStatus;
import app.api.user.UserStatus;
import app.web.security.Passwords;
import app.web.security.SecureUserHolder;
import core.framework.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.Collectors;

/**
 * @author murphy.zhang
 */
public class UserService {
    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Inject
    BOUserWebService userWebService;
    @Inject
    SecureUserHolder userHolder;

    public CreateUserAJAXResponse create(CreateUserAJAXRequest request) {
        BOCreateUserRequest createUserRequest = new BOCreateUserRequest();
        createUserRequest.username = request.username;
        createUserRequest.password = Passwords.hash(request.password);
        createUserRequest.email = request.email;
        createUserRequest.description = request.description;
        createUserRequest.createdBy = userHolder.getUserName();
        BOCreateUserResponse createUserResponse = userWebService.create(createUserRequest);
        CreateUserAJAXResponse result = new CreateUserAJAXResponse();
        result.id = createUserResponse.id;
        result.success = createUserResponse.success;
        result.message = createUserResponse.message;
        return result;
    }

    public UpdateUserAJAXResponse update(Long id, UpdateUserAJAXRequest request) {
        BOUpdateUserRequest updateUserRequest = new BOUpdateUserRequest();
        updateUserRequest.username = request.username;
        updateUserRequest.description = request.description;
        updateUserRequest.email = request.email;
        updateUserRequest.status = convert(request.status);
        BOUpdateUserResponse updateUserResponse = userWebService.update(id, updateUserRequest);
        UpdateUserAJAXResponse result = new UpdateUserAJAXResponse();
        result.id = updateUserResponse.id;
        result.success = updateUserResponse.success;
        result.message = updateUserResponse.message;
        return result;
    }

    public GetUserAJAXResponse get(Long id) {
        BOGetUserResponse getUserResponse = userWebService.get(id);
        GetUserAJAXResponse result = new GetUserAJAXResponse();
        result.id = getUserResponse.id;
        result.username = getUserResponse.username;
        result.status = convert(getUserResponse.status);
        result.email = getUserResponse.email;
        result.description = getUserResponse.description;
        result.createdTime = getUserResponse.createdTime;
        return result;
    }

    public SearchUserAJAXResponse search(SearchUserAJAXRequest request) {
        BOSearchUserRequest searchUserRequest = new BOSearchUserRequest();
        searchUserRequest.limit = request.limit;
        searchUserRequest.skip = request.skip;
        searchUserRequest.name = request.name;
        if (request.status != null) {
            searchUserRequest.status = convert(request.status);
        }
        searchUserRequest.email = request.email;
        BOSearchUserResponse searchUserResponse = userWebService.search(searchUserRequest);
        SearchUserAJAXResponse result = new SearchUserAJAXResponse();
        result.total = searchUserResponse.total;
        if (result.total > 0) {
            result.users = searchUserResponse.users.stream().map(this::userView).collect(Collectors.toList());
        }
        return result;
    }

    private SearchUserAJAXResponse.User userView(BOSearchUserResponse.User user) {
        SearchUserAJAXResponse.User result = new SearchUserAJAXResponse.User();
        result.id = user.id;
        result.username = user.username;
        result.email = user.email;
        result.description = user.description;
        result.status = convert(user.status);
        result.createdTime = user.createdTime;
        return result;
    }

    public void resetPassword(Long id) {
        String password = Passwords.randomPassword();
        String encryptPassword = Passwords.hash(password);
        logger.info("reset password, userId={}, password={}", id, password);
        BOUpdateUserPasswordRequest request = new BOUpdateUserPasswordRequest();
        request.password = encryptPassword;
        request.updatedBy = userHolder.getUserName();
        userWebService.updatePassword(id, request);
        //TODO send email
        //BOGetUserResponse user = userWebService.get(id);
    }

    public void updateStatus(Long id, UserAJAXStatus status) {
        BOUpdateUserStatusRequest request = new BOUpdateUserStatusRequest();
        request.status = convert(status);
        request.updatedBy = userHolder.getUserName();
        userWebService.updateStatus(id, request);
    }

    private UserStatus convert(UserAJAXStatus status) {
        return UserStatus.valueOf(status.name());
    }

    private UserAJAXStatus convert(UserStatus status) {
        return UserAJAXStatus.valueOf(status.name());
    }
}
