package app.web;

import app.api.BookAuthorAJAXWebService;
import app.api.book.CreateBookAuthorAJAXRequest;
import app.api.book.CreateBookAuthorAJAXResponse;
import app.api.book.SearchBookAuthorAJAXRequest;
import app.api.book.SearchBookAuthorAJAXResponse;
import app.web.book.BookAuthorService;
import app.web.interceptor.LoginRequired;
import core.framework.inject.Inject;
import core.framework.log.ActionLogContext;

/**
 * @author murphy.zhang
 */
@LoginRequired
public class BookAuthorAJAXWebServiceImpl implements BookAuthorAJAXWebService {
    @Inject
    BookAuthorService bookAuthorService;

    @Override
    public SearchBookAuthorAJAXResponse search(SearchBookAuthorAJAXRequest request) {
        return bookAuthorService.search(request);
    }

    @Override
    public CreateBookAuthorAJAXResponse create(CreateBookAuthorAJAXRequest request) {
        ActionLogContext.put("authorName", request.name);
        return bookAuthorService.create(request);
    }
}
