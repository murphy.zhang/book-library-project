package app;

import core.framework.module.App;
import core.framework.module.SystemModule;

/**
 * @author murphy
 */
public class BackOfficeApp extends App {
    @Override
    protected void initialize() {
        load(new SystemModule("sys.properties"));
        http().httpPort(8083);
        http().httpsPort(8443);
        load(new WebModule());

        site().session().local();
    }
}
