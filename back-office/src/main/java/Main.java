import app.BackOfficeApp;

/**
 * @author murphy
 */
public class Main {
    public static void main(String[] args) {
        new BackOfficeApp().start();
    }
}
