package app.api;

import app.api.book.tag.BOCreateBookTagRequest;
import app.api.book.tag.BOCreateBookTagResponse;
import app.api.book.tag.BOSearchBookTagResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.Path;

/**
 * @author murphy.zhang
 */
public interface BOBookTagWebService {
    @GET
    @Path("/bo/book/tag")
    BOSearchBookTagResponse search();

    @POST
    @Path("/bo/book/tag")
    BOCreateBookTagResponse create(BOCreateBookTagRequest request);
}
