package app.api;

import app.api.book.author.BOCreateBookAuthorRequest;
import app.api.book.author.BOCreateBookAuthorResponse;
import app.api.book.author.BOGetBookAuthorResponse;
import app.api.book.author.BOSearchBookAuthorRequest;
import app.api.book.author.BOSearchBookAuthorResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author murphy.zhang
 */
public interface BOBookAuthorWebService {
    @GET
    @Path("/bo/book/author/:id")
    BOGetBookAuthorResponse get(@PathParam("id") Long id);

    @PUT
    @Path("/bo/book/author")
    BOSearchBookAuthorResponse search(BOSearchBookAuthorRequest request);

    @POST
    @Path("/bo/book/author")
    BOCreateBookAuthorResponse create(BOCreateBookAuthorRequest request);
}
