package app.api;

import app.api.book.BOCreateBookRequest;
import app.api.book.BOCreateBookResponse;
import app.api.book.BOGetBookResponse;
import app.api.book.stock.BOGetBookStockResponse;
import app.api.book.borrowed.BOSearchBookBorrowedRequest;
import app.api.book.borrowed.BOSearchBookBorrowedResponse;
import app.api.book.BOSearchBookRequest;
import app.api.book.BOSearchBookResponse;
import app.api.book.BOUpdateBookRequest;
import app.api.book.BOUpdateBookResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author murphy.zhang
 */
public interface BOBookWebService {
    @POST
    @Path("/bo/book")
    BOCreateBookResponse create(BOCreateBookRequest request);

    @PUT
    @Path("/bo/book/:id")
    BOUpdateBookResponse update(@PathParam("id") Long id, BOUpdateBookRequest request);

    @GET
    @Path("/bo/book/:id")
    BOGetBookResponse get(@PathParam("id") Long id);

    @GET
    @Path("/bo/book/:id/stock")
    BOGetBookStockResponse getStock(@PathParam("id") Long id);

    @PUT
    @Path("/bo/book")
    BOSearchBookResponse search(BOSearchBookRequest request);

    @GET
    @Path("/bo/book/borrowed/history")
    BOSearchBookBorrowedResponse searchBookBorrowed(BOSearchBookBorrowedRequest request);
}
