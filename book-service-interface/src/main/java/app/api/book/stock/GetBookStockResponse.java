package app.api.book.stock;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
public class GetBookStockResponse {
    @NotNull
    @Property(name = "book_id")
    public Long bookId;

    @NotNull
    @Property(name = "stock")
    public Integer stock;

}
