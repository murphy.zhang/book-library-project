package app.api.book.kafka;
//TODO rename package to app.book.api.kafka;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
//TODO
public class BookIncreaseStocksMessage {
    @NotNull
    @Property(name = "book_id")
    public Long bookId;
}
