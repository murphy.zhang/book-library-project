package app.api.book.kafka;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
//TODO move to notification service or delete form book service, probably just subscription stock increase
public class BookAvailableMessage {
    @NotNull
    @NotBlank
    @Property(name = "id")
    public String id;

    @NotNull
    @Property(name = "book_id")
    public Long bookId;

    @NotNull
    @NotBlank
    @Property(name = "book_name")
    public String bookName;

    @NotNull
    @Property(name = "user_id")
    public Long userId;
}
