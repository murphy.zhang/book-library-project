package app.api.book.kafka;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;

/**
 * @author murphy.zhang
 */
public class BookBorrowedWillExpireMessage {

    @NotNull
    @NotBlank
    @Property(name = "id")
    public String id;

    @NotNull
    @Property(name = "book_id")
    public Long bookId;

    @NotNull
    @Property(name = "user_id")
    public Long userId;

    @NotNull
    @Property(name = "expect_returned_time")
    public LocalDateTime expectReturnedTime;
}
