package app.api.book.borrowed;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author murphy
 */
public class SearchBookBorrowedResponse {
    @NotNull
    @Property(name = "total")
    public Long total;

    @Property(name = "book_borrowed_histories")
    public List<BookBorrowedHistory> bookBorrowedHistories;

    public static class BookBorrowedHistory {
        @NotNull
        @NotBlank
        @Property(name = "id")
        public String id;

        @NotNull
        @Property(name = "user_Id")
        public Long userId;

        @NotNull
        @Property(name = "book_Id")
        public Long bookId;

        @NotNull
        @Property(name = "status")
        public BookBorrowedStatus status;

        @NotNull
        @Property(name = "expect_returned_time")
        public LocalDateTime expectReturnedTime;

        @Property(name = "actual_returned_time")
        public LocalDateTime actualReturnedTime;

        @NotNull
        @Property(name = "created_time")
        public LocalDateTime createdTime;
    }
}
