package app.api.book.borrowed;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;

/**
 * @author murphy.zhang
 */
public class CreateBookBorrowResponse {
    @NotNull
    @Property(name = "id")
    public String id;

    @NotNull
    @Property(name = "created_time")
    public LocalDateTime createdTime;
}
