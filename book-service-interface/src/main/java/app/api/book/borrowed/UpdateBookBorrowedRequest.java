package app.api.book.borrowed;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
public class UpdateBookBorrowedRequest {
    @NotNull
    @Property(name = "user_id")
    public Long userId;
}
