package app.api.book.borrowed;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;

/**
 * @author murphy.zhang
 */
public class CreateBookBorrowedRequest {
    @NotNull
    @Property(name = "user_id")
    public Long userId;

    @NotNull
    @Property(name = "expect_returned_time")
    public LocalDateTime expectReturnedTime;
}
