package app.api.book.borrowed;

import core.framework.api.json.Property;

/**
 * @author murphy.zhang
 */
public enum BookBorrowedStatus {
    @Property(name = "BORROWED")
    BORROWED,
    @Property(name = "RETURNED")
    RETURNED
}
