package app.api.book.borrowed;

import core.framework.api.validate.NotNull;
import core.framework.api.web.service.QueryParam;

/**
 * @author murphy.zhang
 */
public class BOSearchBookBorrowedRequest {
    @NotNull
    @QueryParam(name = "skip")
    public Integer skip;

    @NotNull
    @QueryParam(name = "limit")
    public Integer limit;

    @NotNull
    @QueryParam(name = "book_id")
    public Long bookId;

    @QueryParam(name = "user_id")
    public Long userId;

    @QueryParam(name = "status")
    public BookBorrowedStatus status;
}
