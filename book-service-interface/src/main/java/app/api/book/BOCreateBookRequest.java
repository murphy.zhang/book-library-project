package app.api.book;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author murphy.zhang
 */
public class BOCreateBookRequest {
    @NotNull
    @NotBlank
    @Property(name = "name")
    public String name;

    @NotNull
    @Property(name = "category_id")
    public Long categoryId;

    @NotNull
    @Property(name = "author_ids")
    public List<Long> authorIds;

    @NotNull
    @Property(name = "tag_ids")
    public List<Long> tagIds;

    @NotNull
    @Property(name = "stock")
    public Integer stock;

    @Property(name = "description")
    public String description;

    @NotNull
    @Property(name = "published_time")
    public LocalDateTime publishedTime;

    @NotNull
    @NotBlank
    @Property(name = "created_by")
    public String createdBy;
}
