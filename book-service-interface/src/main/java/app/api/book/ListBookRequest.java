package app.api.book;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;
import core.framework.api.validate.Size;

import java.util.List;

/**
 * @author murphy.zhang
 */
public class ListBookRequest {
    @NotNull
    @Size(min = 1)
    @Property(name = "ids")
    public List<Long> ids;
}
