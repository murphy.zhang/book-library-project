package app.api.book.author;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
public class BOCreateBookAuthorRequest {
    @NotNull
    @NotBlank
    @Property(name = "name")
    public String name;

    @NotNull
    @NotBlank
    @Property(name = "nationality")
    public String nationality;

    @NotNull
    @NotBlank
    @Property(name = "dynasty")
    public String dynasty;

    @Property(name = "description")
    public String description;

    @NotNull
    @NotBlank
    @Property(name = "created_by")
    public String createdBy;
}
