package app.api.book.author;

import core.framework.api.json.Property;
import core.framework.api.validate.NotNull;

import java.util.List;

/**
 * @author murphy.zhang
 */
public class BOSearchBookAuthorRequest {
    @NotNull
    @Property(name = "skip")
    public Integer skip;

    @NotNull
    @Property(name = "limit")
    public Integer limit;

    @Property(name = "ids")
    public List<Long> ids;

    @Property(name = "name")
    public String name;

    @Property(name = "nationality")
    public String nationality;

    @Property(name = "dynasty")
    public String dynasty;
}
