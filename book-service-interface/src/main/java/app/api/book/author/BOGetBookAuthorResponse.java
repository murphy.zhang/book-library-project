package app.api.book.author;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;

public class BOGetBookAuthorResponse {
    @NotNull
    @Property(name = "id")
    public Long id;

    @NotNull
    @NotBlank
    @Property(name = "name")
    public String name;

    @NotNull
    @NotBlank
    @Property(name = "nationality")
    public String nationality;

    @NotNull
    @NotBlank
    @Property(name = "dynasty")
    public String dynasty;

    @Property(name = "description")
    public String description;

    @NotNull
    @Property(name = "created_time")
    public LocalDateTime createdTime;
}
