package app.api.book.category;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

/**
 * @author murphy.zhang
 */
public class BOCreateBookCategoryRequest {
    @NotNull
    @Property(name = "name")
    public String name;

    @NotNull
    @Property(name = "parent_id")
    public Long parentId;

    @NotNull
    @NotBlank
    @Property(name = "created_by")
    public String createdBy;
}
