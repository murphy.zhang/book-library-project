package app.api.book;

import core.framework.api.json.Property;
import core.framework.api.validate.NotBlank;
import core.framework.api.validate.NotNull;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author murphy.zhang
 */
public class BOUpdateBookRequest {
    @Property(name = "name")
    public String name;

    @Property(name = "category_id")
    public Long categoryId;

    @Property(name = "author_ids")
    public List<Long> authorIds;

    @Property(name = "tag_ids")
    public List<Long> tagIds;

    @Property(name = "stock")
    public Integer stock;

    @Property(name = "description")
    public String description;

    @Property(name = "published_time")
    public LocalDateTime publishedTime;

    @NotNull
    @NotBlank
    @Property(name = "updated_by")
    public String updatedBy;
}
