package app.api;

import app.api.book.category.BOCreateBookCategoryRequest;
import app.api.book.category.BOCreateBookCategoryResponse;
import app.api.book.category.BOGetBookCategoryResponse;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author murphy.zhang
 */
public interface BOBookCategoryWebService {
    @GET
    @Path("/bo/book/category/:id")
    BOGetBookCategoryResponse get(@PathParam("id") Long parentId);

    @POST
    @Path("/bo/book/category")
    BOCreateBookCategoryResponse create(BOCreateBookCategoryRequest request);
}
