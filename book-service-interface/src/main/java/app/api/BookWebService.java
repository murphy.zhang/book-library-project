package app.api;

import app.api.book.borrowed.CreateBookBorrowResponse;
import app.api.book.borrowed.CreateBookBorrowedRequest;
import app.api.book.ListBookRequest;
import app.api.book.ListBookResponse;
import app.api.book.GetBookResponse;
import app.api.book.stock.GetBookStockResponse;
import app.api.book.borrowed.SearchBookBorrowedRequest;
import app.api.book.borrowed.SearchBookBorrowedResponse;
import app.api.book.category.GetBookCategoryResponse;
import app.api.book.SearchBookRequest;
import app.api.book.SearchBookResponse;
import app.api.book.tag.SearchBookTagResponse;
import app.api.book.borrowed.UpdateBookBorrowResponse;
import app.api.book.borrowed.UpdateBookBorrowedRequest;
import core.framework.api.web.service.GET;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author murphy.zhang
 */
public interface BookWebService {
    @GET
    @Path("/book/:id")
    GetBookResponse get(@PathParam("id") Long id);

    @GET
    @Path("/book/:id/stock")
    GetBookStockResponse getStock(@PathParam("id") Long id);

    @PUT
    @Path("/book")
    SearchBookResponse search(SearchBookRequest request);

    @PUT
    @Path("/book/list")
    ListBookResponse list(ListBookRequest request);

    @GET
    @Path("/book/category/:id")
    GetBookCategoryResponse getCategory(@PathParam("id") Long parentId);

    @GET
    @Path("/book/tag")
    SearchBookTagResponse searchTag();

    @PUT
    @Path("/book/borrowed")
    SearchBookBorrowedResponse searchBookBorrowed(SearchBookBorrowedRequest request);

    @POST
    @Path("/book/:id/borrowed")
    CreateBookBorrowResponse borrowed(@PathParam("id") Long bookId, CreateBookBorrowedRequest request);

    @PUT
    @Path("/book/borrowed/history/:id")
    UpdateBookBorrowResponse returned(@PathParam("id") String id, UpdateBookBorrowedRequest request);
}
