package app.api;

import app.api.book.borrowed.CreateBookBorrowResponse;
import app.api.book.borrowed.CreateBookBorrowedRequest;
import app.api.book.borrowed.SearchBookBorrowedRequest;
import app.api.book.borrowed.SearchBookBorrowedResponse;
import app.api.book.borrowed.UpdateBookBorrowResponse;
import app.api.book.borrowed.UpdateBookBorrowedRequest;
import core.framework.api.web.service.POST;
import core.framework.api.web.service.PUT;
import core.framework.api.web.service.Path;
import core.framework.api.web.service.PathParam;

/**
 * @author murphy.zhang
 */
public interface BookBorrowedHistoryWebService {
    @PUT
    @Path("/user/:user-id/book/borrowed-history")
    SearchBookBorrowedResponse search(SearchBookBorrowedRequest request);

    @POST
    @Path("/user/:user-id/borrow-book")
    CreateBookBorrowResponse borrowBook(@PathParam("user-id") Long userId, CreateBookBorrowedRequest request);

    @PUT
    @Path("/book/borrowed-history/:id/return")
    UpdateBookBorrowResponse returnOfBook(@PathParam("id") String id, UpdateBookBorrowedRequest request);
}
