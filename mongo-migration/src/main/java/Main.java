import com.mongodb.client.model.Indexes;
import core.framework.mongo.MongoMigration;
import org.bson.Document;

/**
 * @author murphy
 */
public class Main {
    public static void main(String[] args) {
        var migration = new MongoMigration("sys.properties", "sys.mongo.adminURI");
        migration.migrate(mongo -> {
            mongo.runCommand(new Document().append("setParameter", 1).append("notablescan", 1));
        });

        migration = new MongoMigration("sys.properties");
        migration.migrate(mongo -> {
            mongo.createIndex("book_borrowed_histories", Indexes.ascending("book_id"));
            mongo.createIndex("book_borrowed_histories", Indexes.ascending("user_id"));
            mongo.createIndex("book_borrowed_histories", Indexes.ascending("expect_returned_time"));
            mongo.createIndex("user_subscriptions", Indexes.ascending("book_id"));
            mongo.createIndex("user_subscriptions", Indexes.ascending("user_id"));
            mongo.createIndex("user_notifications", Indexes.ascending("book_id"));
            mongo.createIndex("user_notifications", Indexes.ascending("user_id"));
        });
    }
}
