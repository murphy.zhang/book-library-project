package app.notification;

import app.api.book.kafka.BookAvailableMessage;
import app.api.book.kafka.BookBorrowedWillExpireMessage;
import app.notification.user.kafka.BookAvailableHandler;
import app.notification.user.kafka.BookBorrowedWillExpireHandler;
import core.framework.module.Module;

public class UserModule extends Module {
    @Override
    protected void initialize() {
        kafka().subscribe("book-available", BookAvailableMessage.class, bind(BookAvailableHandler.class));
        kafka().subscribe("book-borrowed-will-expire", BookBorrowedWillExpireMessage.class, bind(BookBorrowedWillExpireHandler.class));
    }
}
