package app.notification;

import app.api.BOBookWebService;
import app.api.UserNotificationWebService;
import app.api.UserSubscriptionWebService;
import app.api.UserWebService;
import core.framework.module.App;
import core.framework.module.SystemModule;

public class NotificationServiceApp extends App {
    @Override
    protected void initialize() {
        load(new SystemModule("sys.properties"));
        http().httpPort(8085);
        api().client(UserWebService.class, requiredProperty("sys.user.uri"));
        api().client(UserNotificationWebService.class, requiredProperty("sys.user.uri"));
        api().client(UserSubscriptionWebService.class, requiredProperty("sys.user.uri"));
        api().client(BOBookWebService.class, requiredProperty("sys.book.uri"));
        load(new BookModule());
        load(new UserModule());
    }
}
