package app.notification;

import app.api.book.kafka.BookAvailableMessage;
import app.api.book.kafka.BookIncreaseStocksMessage;
import app.notification.book.kafka.BookIncreaseStocksHandler;
import core.framework.module.Module;

public class BookModule extends Module {
    @Override
    protected void initialize() {
        kafka().publish("book-available", BookAvailableMessage.class);
        kafka().subscribe("book-increase-stocks", BookIncreaseStocksMessage.class, bind(BookIncreaseStocksHandler.class));
        kafka().poolSize(2);
    }
}
