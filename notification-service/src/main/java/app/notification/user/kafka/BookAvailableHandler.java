package app.notification.user.kafka;

import app.api.UserNotificationWebService;
import app.api.UserSubscriptionWebService;
import app.api.UserWebService;
import app.api.book.kafka.BookAvailableMessage;
import app.api.user.GetUserResponse;
import app.api.user.notification.CreateUserNotificationRequest;
import app.api.user.notification.UserNotificationType;
import core.framework.inject.Inject;
import core.framework.kafka.MessageHandler;
import core.framework.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author murphy.zhang
 */
public class BookAvailableHandler implements MessageHandler<BookAvailableMessage> {
    private final Logger logger = LoggerFactory.getLogger(BookAvailableHandler.class);

    @Inject
    UserWebService userWebService;
    @Inject
    UserSubscriptionWebService userSubscriptionWebService;
    @Inject
    UserNotificationWebService userNotificationWebService;

    @Override
    public void handle(String key, BookAvailableMessage value) throws Exception {
        logger.info("book available then notify user, userId={}, bookId={}", value.userId, value.bookId);
        GetUserResponse user = userWebService.get(value.userId);
        CreateUserNotificationRequest request = new CreateUserNotificationRequest();
        request.userId = value.userId;
        request.bookId = value.bookId;
        request.type = UserNotificationType.SUBSCRIPTION;
        request.content = Strings.format("dear {}, the books subscribed are available, book name is '{}'", user.username, value.bookName);
        userNotificationWebService.create(request);

        //TODO send email to user
        userSubscriptionWebService.finish(value.id);
    }
}
