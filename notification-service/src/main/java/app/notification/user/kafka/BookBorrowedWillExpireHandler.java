package app.notification.user.kafka;

import app.api.UserNotificationWebService;
import app.api.UserWebService;
import app.api.book.kafka.BookBorrowedWillExpireMessage;
import app.api.user.GetUserResponse;
import app.api.user.notification.CreateUserNotificationRequest;
import app.api.user.notification.UserNotificationType;
import core.framework.inject.Inject;
import core.framework.kafka.MessageHandler;
import core.framework.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author murphy.zhang
 */
public class BookBorrowedWillExpireHandler implements MessageHandler<BookBorrowedWillExpireMessage> {
    private final Logger logger = LoggerFactory.getLogger(BookBorrowedWillExpireHandler.class);

    @Inject
    UserWebService userWebService;
    @Inject
    UserNotificationWebService userNotificationWebService;

    @Override
    public void handle(String key, BookBorrowedWillExpireMessage value) throws Exception {
        logger.info("book borrowed will expire, bookId={}, userId={}, expiredTime={}", value.bookId, value.userId, value.expectReturnedTime);
        GetUserResponse user = userWebService.get(value.userId);
        CreateUserNotificationRequest request = new CreateUserNotificationRequest();
        request.userId = value.userId;
        request.bookId = value.bookId;
        request.type = UserNotificationType.EXPIRE;
        request.content = Strings.format("dear {}, the book that you borrowed will expire, expired time is {}", user.username, value.expectReturnedTime);
        userNotificationWebService.create(request);
        //TODO send email to user
    }
}
