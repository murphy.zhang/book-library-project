package app.notification.book.kafka;

import app.api.BOBookWebService;
import app.api.UserSubscriptionWebService;
import app.api.book.BOGetBookResponse;
import app.api.book.kafka.BookAvailableMessage;
import app.api.book.kafka.BookIncreaseStocksMessage;
import app.api.user.subscription.ListUserSubscriptionRequest;
import app.api.user.subscription.ListUserSubscriptionResponse;
import app.api.user.subscription.TotalUserSubscriptionResponse;
import app.api.user.subscription.UserSubscriptionStatus;
import core.framework.inject.Inject;
import core.framework.kafka.MessageHandler;
import core.framework.kafka.MessagePublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author murphy.zhang
 */
public class BookIncreaseStocksHandler implements MessageHandler<BookIncreaseStocksMessage> {
    private final Logger logger = LoggerFactory.getLogger(BookIncreaseStocksHandler.class);

    @Inject
    BOBookWebService boBookWebService;
    @Inject
    UserSubscriptionWebService userSubscriptionWebService;
    @Inject
    MessagePublisher<BookAvailableMessage> messagePublisher;

    @Override
    public void handle(String key, BookIncreaseStocksMessage value) throws Exception {
        ListUserSubscriptionRequest request = new ListUserSubscriptionRequest();
        request.skip = 0;
        request.limit = 1000;
        request.bookId = value.bookId;
        request.status = UserSubscriptionStatus.ON;
        TotalUserSubscriptionResponse response = userSubscriptionWebService.total(request);
        logger.info("book increase stocks, total {} user subscribe, bookId={}", response.total, value.bookId);
        if (response.total > 0) {
            BOGetBookResponse book = boBookWebService.get(value.bookId);
            long number = response.total / request.limit + (response.total % request.limit == 0 ? 0 : 1);
            for (int i = 0; i < number; i++) {
                request.skip = i * request.limit;
                //TODO don't push message and use async
                pushBookAvailableMessage(userSubscriptionWebService.list(request).userSubscriptions, book.name);
            }
        }
    }

    private void pushBookAvailableMessage(List<ListUserSubscriptionResponse.UserSubscription> subscribes, String bookName) {
        subscribes.forEach(s -> {
            BookAvailableMessage message = new BookAvailableMessage();
            message.id = s.id;
            message.bookId = s.bookId;
            message.bookName = bookName;
            message.userId = s.userId;
            messagePublisher.publish(message);
        });
    }
}
