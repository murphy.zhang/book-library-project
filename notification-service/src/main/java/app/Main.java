package app;

import app.notification.NotificationServiceApp;

/**
 * @author murphy.zhang
 */
public class Main {
    public static void main(String[] args) {
        new NotificationServiceApp().start();
    }
}
