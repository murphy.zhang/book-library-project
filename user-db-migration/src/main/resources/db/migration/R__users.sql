CREATE TABLE IF NOT EXISTS `users` (
  `id`                  INT(11)                         NOT NULL AUTO_INCREMENT,
  `username`            VARCHAR(50)                     NOT NULL,
  `password`            VARCHAR(100)                    NOT NULL,
  `email`               VARCHAR(50)                     NOT NULL,
  `status`              VARCHAR(20)                     NOT NULL,
  `description`         VARCHAR(255)                    DEFAULT NULL,
  `created_by`          VARCHAR (200)                   NOT NULL,
  `updated_by`          VARCHAR (200)                   NOT NULL,
  `created_time`        DATETIME                        NOT NULL,
  `updated_time`        DATETIME                        NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY uk_username (`username`)
)
ENGINE=InnoDB
AUTO_INCREMENT=1
DEFAULT CHARSET=utf8;